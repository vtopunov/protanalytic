#ifndef T_PG_H
#define T_PG_H

#include <cstdint>
#include <vector>
#include <memory>

#include <QtCore>
#include <QtEndian>

#include <libpq-fe.h>
#include <postgresql/server/catalog/pg_type.h>

inline void close(PGconn* conn) { PQfinish(conn); }
inline void close(PGresult* res) { PQclear(res); }

struct closer {
	template<class T>
        void operator () (const T& handle) { close(handle); }
};

template<class T>
using pg_handle = std::unique_ptr<T, closer> ;

template<class T> inline
pg_handle<T> make_pg_handle(T* handle) { return pg_handle<T>(handle, closer()); }

class SqlParameterList {
public:
	SqlParameterList() : params_(), formats_() {}

	SqlParameterList(const SqlParameterList& p) :
		params_(p.params_), 
		formats_(p.formats_)
	{}

	SqlParameterList(SqlParameterList&& p) :
		params_(std::move(p.params_)),
		formats_(std::move(p.formats_))
		{}

	SqlParameterList& operator = (const SqlParameterList& p) {
		params_ = p.params_;
		formats_ = p.formats_;
		return *this;
	}

	SqlParameterList& operator = (SqlParameterList&& p) {
		params_ = std::move(p.params_);
		formats_ = std::move(p.formats_);
		return *this;
	}

	SqlParameterList& operator += (const SqlParameterList& p) {
		params_.insert(params_.end(), p.params_.begin(), p.params_.end());
		formats_.insert(formats_.end(), p.formats_.begin(), p.formats_.end());
		return *this;
	}

	SqlParameterList& arg(const QByteArray& data) {
		if (!data.isEmpty()) {
			params_.push_back(data);
			formats_.push_back(1);
		}
		return *this;
	}

	SqlParameterList& arg(QByteArray&& data) {
		if (!data.isEmpty()) {
			params_.emplace_back(data);
			formats_.push_back(1);
		}
		return *this;
	}

	SqlParameterList& arg(const char* data) {
		if (data && *data) {
			params_.emplace_back(QByteArray(data));
			formats_.push_back(0);
		}
		return *this;
	}

	SqlParameterList& arg(const QString& data) {
		if (!data.isEmpty()) {
			params_.emplace_back(data.toLocal8Bit());
			formats_.push_back(0);
		}
		return *this;
	}

	SqlParameterList& arg(const std::string& data) {
		return SqlParameterList::arg(data.c_str());
	}

	template<size_t n>
	SqlParameterList& arg(const uint8_t(&data)[n]) {
		return SqlParameterList::arg(QByteArray::fromRawData(reinterpret_cast<const char*>(data), n));
	}

	template<class T>
	SqlParameterList& arg(const T& value) {
		return SqlParameterList::arg(std::to_string(value));
	}

	const std::vector<QByteArray>& params() const { return params_; }

	const std::vector<int>& formats() const { return formats_; }

	size_t size() const { return params().size(); }

private:
	std::vector<QByteArray> params_;
	std::vector<int> formats_;
};

inline SqlParameterList operator + (const SqlParameterList& a, const SqlParameterList& b) {
	return SqlParameterList(a) += b;
}

class Sql {
public:
	Sql() : command_(), params_() {}
	Sql(const char* cmd) : command_(cmd), params_() {}
	Sql(const std::string& cmd) : command_(QByteArray::fromRawData(cmd.data(), cmd.size())), params_() {}
	Sql(const QByteArray& cmd) : command_(cmd), params_() {}
	Sql(QByteArray&& cmd) : command_(cmd), params_() {}
	Sql(const QString& cmd) : command_(cmd.toLocal8Bit()), params_() {}
	Sql(const Sql& sql_) : command_(sql_.command_), params_(sql_.params_) {}
	Sql(Sql&& sql_) :
		command_(std::move(sql_.command_)),
		params_(std::move(sql_.params_)) {}

	Sql& operator = (const Sql& sql_) {
		command_ = sql_.command_;
		params_ = sql_.params_;
		return *this;
	}

	Sql& operator = (Sql&& sql_) {
		command_ = std::move(sql_.command_);
		params_ = std::move(sql_.params_);
		return *this;
	}

	Sql& operator += (const Sql& sql_) {
		command_ += sql_.command_;
		params_ += sql_.params_;
		return *this;
	}

	template<class T>
	Sql& arg(T&& data) {
		params_.arg(data);
		return *this;
	}

	template<class T>
	Sql& arg(const T& data) {
		params_.arg(data);
		return *this;
	}

	const char* command() const { return command_; }

	const SqlParameterList& params() const { return params_; }

	static const char param_prefix = '$';

	uint32_t parseParamsCount() const {
		return command_.count(param_prefix);
	}


	QByteArray debug() const {
		QByteArray debug_(command_);
		int i_param = 1;
		for (auto param : params_.params()) {
			if (!params_.formats().at(i_param-1)) {
				debug_.replace(param_prefix + QByteArray::number(i_param), param);
			}
			++i_param;
		}
		return debug_;
	}

private:
	QByteArray command_;
	SqlParameterList params_;
};

inline Sql operator + (const Sql& a, const Sql& b) {
	return Sql(a) += b;
}

template<class T, class Fn> inline
auto v_convert(const std::vector<T>& v, Fn fn) -> std::vector<decltype(fn(T()))> {
	std::vector<decltype(fn(T()))> res;
	res.reserve(v.size());
	for (auto& item : v) {
		res.emplace_back(fn(item));
	}
	return res;
}

template<class T> inline
T binary_to(const PGresult* res, uint32_t row, uint32_t column, const T& def_value = T()) {
	if (PQgetlength(res, row, column) == sizeof(T)) {
		return qFromBigEndian<T>(reinterpret_cast<const uchar*>(PQgetvalue(res, row, column)));
	}
	return def_value;
}

inline QByteArray binary_to_bytearray(const PGresult* res, uint32_t row, uint32_t column) {
	return QByteArray::fromRawData(PQgetvalue(res, row, column), PQgetlength(res, row, column));
}

inline QString binary_to_qstring(const PGresult* res, uint32_t row, uint32_t column) {
	return binary_to_bytearray(res, row, column);
}

inline int32_t string_to_int(const PGresult* res, uint32_t row, uint32_t column, int32_t def_value = 0) {
	bool ok;
	auto value = binary_to_bytearray(res, row, column).toLong(&ok);
	return (ok) ? value : def_value;
}

inline int32_t to_int(const PGresult* res, uint32_t row, uint32_t column, int32_t value = 0L) {
	if (!PQgetisnull(res, row, column)) {
		if (PQfformat(res, column)) {
			switch (PQftype(res, column)) {
			case INT2OID: {
				if (value >= INT16_MIN && value <= INT16_MAX) {
					value = binary_to<int16_t>(res, row, column, static_cast<int16_t>(value));
				}
			} break;
			case INT4OID: value = binary_to<int32_t>(res, row, column, value); break;
			case INT8OID: {
				const auto i64_value = binary_to<int64_t>(res, row, column, value);
				if (i64_value >= INT32_MIN && i64_value <= INT32_MAX) {
					value = i64_value;
				}
			} break;
			case TEXTOID:
			case VARCHAROID:
				value = string_to_int(res, row, column);
				break;
			}
		}
		else {
			value = string_to_int(res, row, column);
		}
	}

	return value;
}


inline QString to_qstring(const PGresult* res, uint32_t row, uint32_t column, const QString& defValue) {
	if (!PQgetisnull(res, row, column)) {
		if (PQfformat(res, column)) {
			switch (PQftype(res, column)) {
			case INT2OID: return QString::number(binary_to<int16_t>(res, row, column));
			case INT4OID: return QString::number(binary_to<int32_t>(res, row, column));
			case INT8OID: return QString::number(binary_to<int64_t>(res, row, column));
			}
		}

		return binary_to_qstring(res, row, column);
	}
	
	return defValue;
}

inline QString uri(const PGconn* conn_)  { 
	return (conn_) ? QString("%1:%2/%3").arg(PQhost(conn_)).arg(PQport(conn_)).arg(PQdb(conn_)) : "";
}

inline QString errorMessage(PGconn* conn_) {
	if (!conn_) {
		return "PgClient - invalid connection handle";
	}

	if (PQstatus(conn_) != CONNECTION_OK) {
		return QString("PGconn - ") + PQerrorMessage(conn_);
	}

	return QString();
}

class PgClient {
public:
	PgClient() :
		errorMessage_("PgClient - No Init"),
		conn_(make_pg_handle<PGconn>(nullptr)),
		res_(make_pg_handle<PGresult>(nullptr)),
		n_rows_(0UL),
		n_columns_(0UL) {}

	PgClient(const QString& conStr) :
		PgClient()
	{
		conn_ = make_pg_handle(PQconnectdb(conStr.toLocal8Bit()));
		errorMessage_ = ::errorMessage(conn_.get());
	}

	PgClient(PgClient&& rvalue) : 
		errorMessage_(std::move(rvalue.errorMessage_)),
		conn_(make_pg_handle(rvalue.conn_.release())),
		res_(make_pg_handle(rvalue.res_.release())),
		n_rows_(rvalue.n_rows_),
		n_columns_(rvalue.n_columns_)
	{}

	PgClient& operator = (PgClient&& rvalue) {
		errorMessage_ = std::move(rvalue.errorMessage_);
		conn_ = make_pg_handle(rvalue.conn_.release());
		res_ = make_pg_handle(rvalue.res_.release());
		n_rows_ = rvalue.n_rows_;
		n_columns_ = rvalue.n_columns_;
		return *this;
	}

	QString uri() const { return ::uri(conn_.get()); }
	 
	bool isOk() const { return errorMessage_.isEmpty(); }

	QString errorMessage() const { return errorMessage_; }

	PgClient& exec(const Sql& sql_) {
		if (isOk()) {
			const auto& params = sql_.params();
			auto n_params = params.size();

			if (n_params >= 0 && n_params <= INT_MAX) {
				const auto& vparams = params.params();
				const bool is_params = (n_params > 0);

				qDebug() << sql_.debug();

                if( sql_.parseParamsCount() != n_params ) {
                    qWarning() << "count prefix '" << sql_.param_prefix << "' != count params";
                }

				auto result = make_pg_handle(PQexecParams(
					conn_.get(), sql_.command(),
					static_cast<int>(n_params),
					nullptr,
					(is_params) ? v_convert(vparams, [](const QByteArray& data) { return data.constData(); }).data() : nullptr,
					(is_params) ? v_convert(vparams, [](const QByteArray& data) { return static_cast<int>(data.size()); }).data() : nullptr,
					params.formats().data(),
					1));

				auto is_fail_status = [](ExecStatusType status) {
					return (status != PGRES_COMMAND_OK) && (status != PGRES_TUPLES_OK);
				};

				if (!result) {
					errorMessage_ = "PgClient - invalid result handle";
				}
				else if (is_fail_status(PQresultStatus(result.get()))) {
					errorMessage_ = QString("PGresult - ") + PQresultErrorMessage(result.get());
				}
				else {
					const int n_rows { PQntuples(result.get()) };
					const int n_columns{ PQnfields(result.get()) };

					if (n_rows < 0) {
						errorMessage_ = "PgClient - PQntuples return value < 0";
					}
					else if (n_columns < 0) {
						errorMessage_ = "PgClient - PQnfields return value < 0";
					}
					else {
						res_ = std::move(result);
						n_rows_ = static_cast<uint32_t>(n_rows);
						n_columns_ = static_cast<uint32_t>(n_columns);
					}
				}
			}
			else {
				errorMessage_ = "PgClient - Too many parameters";
			}
		}

		return *this;
	}

	class RowIterator;

	class Row {
	public:
		class Column  {
		public:
			Column() :  row_(0UL), column_(0UL), result_(nullptr) {}

			Column(uint32_t row, uint32_t column, const PGresult* result) :
				row_(row),
				column_(column),
				result_(result){}

			QString toQString(const QString& defValue = QString()) const {
                                return (result_) ? to_qstring(result_, row_, column_, defValue) : defValue;
			}

			int32_t toInt(int32_t defValue = 0) const {
				return (result_) ? to_int(result_, row_, column_, defValue) : defValue;
			}

			QByteArray toByteArray() const {
				return (result_) ? binary_to_bytearray(result_, row_, column_) : QByteArray();
			}

		private:
			uint32_t row_;
			uint32_t column_;
			const PGresult* result_;
		};

		Row() : index_(0UL), size_(0UL), result_(nullptr) {}
		Row(uint32_t index, uint32_t size, const PGresult* result) : index_(index), size_(size), result_(result) {}

		uint32_t size() const { return size_; }

		Column at(uint32_t index) const {
			return (index < size()) ? Column(index_, index, result_) : Column();
		}

		Column column(uint32_t index) const {
			return at(index);
		}

		friend class Column;
		friend class RowIterator;

	private:
		uint32_t index_;
		uint32_t size_;
		const PGresult* result_;
	};

	class RowIterator : public std::iterator<std::random_access_iterator_tag, Row, int> {
	public:
		RowIterator() : row_() {}
		RowIterator(const Row& row) : row_(row) {}

		reference operator * () { return row_; }
		RowIterator& operator ++ () { ++row_.index_; return *this; }
		RowIterator& operator -- () { --row_.index_; return *this; }
		RowIterator operator ++ (int) { auto tmp = *this; ++*this; return tmp; }
		RowIterator operator -- (int) { auto tmp = *this; --*this; return tmp; }
		RowIterator& operator += (difference_type value) { row_.index_ += value; return *this; }
		RowIterator& operator -= (difference_type value) { row_.index_ -= value; return *this; }
		difference_type  operator - (const RowIterator& value) { return row_.index_ - value.row_.index_; }
		RowIterator operator + (difference_type value) const { auto tmp = *this; tmp += value; return tmp; }
		RowIterator operator - (difference_type value) const { auto tmp = *this; tmp -= value; return tmp; }
		reference operator [] (difference_type value) const { return *(*this + value); }
		bool operator == (const RowIterator& value)  const { return row_.index_ == value.row_.index_; }
		bool operator != (const RowIterator& value)  const { return row_.index_ != value.row_.index_; }
		bool operator > (const RowIterator& value)  const { return row_.index_ > value.row_.index_; }
		bool operator < (const RowIterator& value)  const { return row_.index_ < value.row_.index_; }
		bool operator >= (const RowIterator& value)  const { return row_.index_ >= value.row_.index_; }
		bool operator <= (const RowIterator& value)  const { return row_.index_ <= value.row_.index_; }

	private:
		Row row_;
	};

	uint32_t nRows() const { return n_rows_; }

	uint32_t nColumns() const { return n_columns_; }

	uint32_t size() const { return nRows(); }

	Row at(uint32_t index) {
		return (index < size()) ? Row(index, nColumns(), res_.get()) : Row();
	}

	Row row(uint32_t index) {
		return at(index);
	}

	bool empty() const { return size() == 0UL; }

	RowIterator begin() { return RowIterator(at(0UL)); }

	RowIterator end() { return RowIterator(Row(size(), 0UL, nullptr)); }

private:
	QString errorMessage_;
	pg_handle<PGconn> conn_;
	pg_handle<PGresult> res_;
	uint32_t n_rows_;
	uint32_t n_columns_;
};

#endif
