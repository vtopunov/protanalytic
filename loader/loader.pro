TEMPLATE = app
macx {
	CONFIG += c++11
	DEFINES += HAVE_PTHREAD
	QMAKE_CFLAGS += -gdwarf-2
	QMAKE_CXXFLAGS += -gdwarf-2
	QMAKE_INFO_PLIST= $${PWD}/Info.plist
	ICON = $${PWD}/icon.icns
	debug { DEFINES += _DEBUG }
}
DESTDIR = $$PWD/../bin/
QT += core
DEFINES += ELOG_H PG_PORT_H _SCL_SECURE_NO_WARNINGS
INCLUDEPATH += $$PWD/../converter $$PWD/../pgsql/include .
LIBS += -L$$PWD/../bin/ -L$$PWD/../pgsql/lib/
macx { LIBS += -lpq } else { LIBS += -llibpq }
SOURCES += $$PWD/$$PWD/../converter/google/protobuf/arena.cc $$PWD/$$PWD/../converter/google/protobuf/arenastring.cc $$PWD/$$PWD/../converter/google/protobuf/descriptor.cc $$PWD/$$PWD/../converter/google/protobuf/descriptor.pb.cc $$PWD/$$PWD/../converter/google/protobuf/descriptor_database.cc $$PWD/$$PWD/../converter/google/protobuf/dynamic_message.cc $$PWD/$$PWD/../converter/google/protobuf/extension_set.cc $$PWD/$$PWD/../converter/google/protobuf/extension_set_heavy.cc $$PWD/$$PWD/../converter/google/protobuf/generated_message_reflection.cc $$PWD/$$PWD/../converter/google/protobuf/generated_message_util.cc $$PWD/$$PWD/../converter/google/protobuf/io/coded_stream.cc $$PWD/$$PWD/../converter/google/protobuf/io/printer.cc $$PWD/$$PWD/../converter/google/protobuf/io/strtod.cc $$PWD/$$PWD/../converter/google/protobuf/io/tokenizer.cc $$PWD/$$PWD/../converter/google/protobuf/io/zero_copy_stream.cc $$PWD/$$PWD/../converter/google/protobuf/io/zero_copy_stream_impl.cc $$PWD/$$PWD/../converter/google/protobuf/io/zero_copy_stream_impl_lite.cc $$PWD/$$PWD/../converter/google/protobuf/map_field.cc $$PWD/$$PWD/../converter/google/protobuf/message.cc $$PWD/$$PWD/../converter/google/protobuf/message_lite.cc $$PWD/$$PWD/../converter/google/protobuf/reflection_ops.cc $$PWD/$$PWD/../converter/google/protobuf/repeated_field.cc $$PWD/$$PWD/../converter/google/protobuf/service.cc $$PWD/$$PWD/../converter/google/protobuf/stubs/atomicops_internals_x86_gcc.cc $$PWD/$$PWD/../converter/google/protobuf/stubs/atomicops_internals_x86_msvc.cc $$PWD/$$PWD/../converter/google/protobuf/stubs/common.cc $$PWD/$$PWD/../converter/google/protobuf/stubs/once.cc $$PWD/$$PWD/../converter/google/protobuf/stubs/stringprintf.cc $$PWD/$$PWD/../converter/google/protobuf/stubs/structurally_valid.cc $$PWD/$$PWD/../converter/google/protobuf/stubs/strutil.cc $$PWD/$$PWD/../converter/google/protobuf/stubs/substitute.cc $$PWD/$$PWD/../converter/google/protobuf/text_format.cc $$PWD/$$PWD/../converter/google/protobuf/unknown_field_set.cc $$PWD/$$PWD/../converter/google/protobuf/wire_format.cc $$PWD/$$PWD/../converter/google/protobuf/wire_format_lite.cc $$PWD/$$PWD/../converter/spectrum.pb.cc $$PWD/$$PWD/main.cpp
