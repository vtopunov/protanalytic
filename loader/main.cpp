#include <cstdio>
#include <cstdint>

#include <QtCore/QCoreApplication>

#ifdef _DEBUG

#pragma warning( push )
	#pragma warning( disable : 4100 )
	#pragma warning( disable : 4244 )
	#pragma warning( disable : 4512 )
	#pragma warning( disable : 4061 )

	#include <spectrum.pb.h>

#pragma warning( pop )

#endif

#include <t_pg.h>

bool uploadingImassFile(PgClient& client, const QString& experimentName, QByteArray& data) {
	client.exec("BEGIN");

	auto addExperimentCommand =
		Sql("INSERT INTO experiment (name, data) VALUES ($1, $2::bytea) RETURNING (id)")
		.arg(experimentName).arg(std::move(data));
	
	auto experement_id = client.exec(std::move(addExperimentCommand)).row(0).column(0).toInt();

	qDebug() << "\n RETURNING (id): " << experement_id << '\n';

#ifdef _DEBUG
	Experiment experiment;
	if (!experiment.ParseFromArray(data.constData(), data.size())) {
		qDebug() << "error: invalid format file\n";
		return false;
	}

	for (int i = 0; client.isOk() && i < experiment.spectrum_size(); ++i) {
		auto spectrum_name = experimentName + "/" + QString::number(i);
		auto spectrum = experiment.spectrum(i);
		auto size = spectrum.ByteSize();
		QByteArray data;
		data.resize(size);
		spectrum.SerializeToArray(data.data(), size);
		auto add_spectrum_cmd =
			Sql("INSERT INTO spectrum(experimentid, name, data) VALUES($1, $2, $3::bytea)")
			.arg(experement_id).arg(spectrum_name).arg(data);

		client.exec(std::move(add_spectrum_cmd));
	}
#endif

	client.exec("COMMIT");

	return client.isOk();
}

QString experimentName(const QFile& file) {
	QFileInfo fi(file);
	auto name = fi.baseName();
	if (!name.compare("DATA", Qt::CaseInsensitive)) {
		name = fi.absoluteDir().dirName();
	}
	return name;
}

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	auto imass_file = a.arguments().value(1);
	if (imass_file.isEmpty()) {
		qDebug() << "error: invalid first argument\n";
		return 1;
	}

	
	QFile file(imass_file);
	if (!file.open(QFile::ReadOnly)) {
		qDebug() << "error: can not open file\n";
		return 1;
	}

	auto data = file.readAll();
	if (data.isEmpty()) {
		qDebug() << "error: empty file\n";
		return 1;
	}

	PgClient client("hostaddr=127.0.0.1 port=5432 dbname=postgres user=postgres");
	if (!client.isOk()) {
		qDebug() << client.errorMessage();
		return 1;
	}


	if (!uploadingImassFile(client, experimentName(file), data)) {
		qDebug() << client.errorMessage();
		return 1;
	}

	qDebug() << "success: file" << imass_file << "was uploaded to database\n";

	return 0;
}
