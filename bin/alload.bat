cd OPST1000.D
"../convert" DATA.MS DATA.imass
"../load" DATA.imass
cd ..

cd OPSTD0.D
"../convert" DATA.MS DATA.imass
"../load" DATA.imass
cd ..

cd OPSTD150.D
"../convert" DATA.MS DATA.imass
"../load" DATA.imass
cd ..

cd OPSTD300.D
"../convert" DATA.MS DATA.imass
"../load" DATA.imass
cd ..

cd OPSTDQC.D
"../convert" DATA.MS DATA.imass
"../load" DATA.imass
cd ..

cd RAWSCAN_HCB.D
"../convert" DATA.MS DATA.imass
"../load" DATA.imass
cd ..
