#ifndef PROTANALYTIC_H
#define PROTANALYTIC_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>
//#include <QtSvg>
#include "ui_protanalytic.h"
#include <QSettings>
#include <ModelessWindow.h>
#include <t_pg.h>
#include <spectrum.pb.h>
#include <QMainWindow>
class ModelessWindow;
class ResumeEdit;
class Resume;
PgClient make_pg_client();
bool loadFile(const QString& fileName);


//class protanalytic : public QMainWindow
class protanalytic : public QWidget
{
	Q_OBJECT

public:
	protanalytic(QWidget *parent = 0);
	~protanalytic();

public slots:
	void databaseItemExpanded(QTreeWidgetItem *item);
	void databaseItemClicked(QTreeWidgetItem *item, int);
	void dbContextMenu(const QPoint& pos);
	void dbtblContextMenu(const QPoint& pos);
	void dbContextMenu(QTreeWidgetItem *item);
	void reload();

protected:
	virtual void showEvent(QShowEvent* ev) Q_DECL_OVERRIDE;

private:
	Ui::protanalyticClass ui;
	ResumeEdit * textedit_resume_;
	Resume* resume_;
	QSettings settings;
	std::vector<QTreeWidgetItem*> viewTree_;
	std::vector<double> x;
	std::vector<double> y;
	Spectrum data_;
};

#endif // PROTANALYTIC_H
