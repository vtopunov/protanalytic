<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>ModelessWindow</name>
    <message>
        <source>Propertiy #</source>
        <translation>Свойство №</translation>
    </message>
    <message>
        <source>-Substation</source>
        <translation>-Вещество</translation>
    </message>
    <message>
        <source> description</source>
        <translation> описание</translation>
    </message>
    <message>
        <source> Comment</source>
        <translation> Комментарий</translation>
    </message>
    <message>
        <source>Modeless Window</source>
        <translation>Немодальное окно</translation>
    </message>
    <message>
        <source>Modeless window for tests</source>
        <translation>Немодльное окно для тестов</translation>
    </message>
    <message>
        <source>Input here number from 0 to 999999999</source>
        <translation>Введите число от  0 до 999999999</translation>
    </message>
    <message>
        <source>Number Value</source>
        <translation>Числовое значение</translation>
    </message>
    <message>
        <source>It is number field with range from 0 to 999 999 999</source>
        <translation>Это числовое поле с диапазоном от 0 до 999 999 999</translation>
    </message>
    <message>
        <source>It is number field with button (range 100 - 10 000, step 5)</source>
        <translation>Это числовое поле с кнопками (диапазон 100 - 10 000, шаг 5)</translation>
    </message>
    <message>
        <source>Input here text</source>
        <translation>Веедите текст</translation>
    </message>
    <message>
        <source>Some Text</source>
        <translation>Немного текста</translation>
    </message>
    <message>
        <source>It is text field</source>
        <translation>Это текстовое поле</translation>
    </message>
    <message>
        <source>It is dropping list of greek letters</source>
        <translation>Выпадающий списко греческих букы</translation>
    </message>
    <message>
        <source>Edit text</source>
        <translation>Редакция текста</translation>
    </message>
    <message>
        <source>Button tool tip</source>
        <translation>Подсказка для кнопки</translation>
    </message>
    <message>
        <source>Button whatsThis</source>
        <translation>Кнопка чтоЭто</translation>
    </message>
    <message>
        <source>No number</source>
        <translation>Нет числа</translation>
    </message>
    <message>
        <source>line edit number value must contain less one digit</source>
        <translation>Поле числового значения должно содержать хотя бы одну цифру</translation>
    </message>
    <message>
        <source>Number edit not a number</source>
        <translation>В числовом поле присутвуют не цифры</translation>
    </message>
    <message>
        <source>Here you are. Combo box with greek alphabet</source>
        <translation>Вот. Выдпающий списко греческих букв</translation>
    </message>
</context>
<context>
    <name>NistDialog</name>
    <message>
        <source>NIST</source>
        <translation></translation>
    </message>
    <message>
        <source>Window provide work this data base NIST: search and identification of substations</source>
        <translation>Окно обеспечивает работу с базой данных NIST: поиск и идентификаци веществ</translation>
    </message>
    <message>
        <source>Push button to validate number Value</source>
        <translation>Кнопка проверки численного содержания</translation>
    </message>
    <message>
        <source>Push and you recieve warning message if number value input is wrong</source>
        <translation>Нажмите и появится предупрждающее сообщение, если числовое значение не корректно</translation>
    </message>
    <message>
        <source>Validate</source>
        <translation>Проверить</translation>
    </message>
    <message>
        <source>Combo box with greed alphabet</source>
        <translation>Выпадающий списко греческого алфавита</translation>
    </message>
    <message>
        <source>you can chose here any greeec letter</source>
        <translation></translation>
    </message>
    <message>
        <source>Edit Form to input text</source>
        <translation>Форма для ввода текста</translation>
    </message>
    <message>
        <source>You can put here any text including greec symbols (UTF16)</source>
        <translation>Сюда может быть введен любой текст, включая греческие  символы</translation>
    </message>
    <message>
        <source>Text Value</source>
        <translation>Тестовое значение</translation>
    </message>
    <message>
        <source>Form to edit number value</source>
        <translation>Форма для ввода числовых значений</translation>
    </message>
    <message>
        <source>This edit form allow identify digits in inputs</source>
        <translation>Форма позволяет определять цифры во вееденных данных</translation>
    </message>
    <message>
        <source>Number VAlue</source>
        <translation>Числовое значение</translation>
    </message>
    <message>
        <source>number edit form with ability increase and decrease value by mouse</source>
        <translation>форма с возможностью увеличиват и уменьшать число манипулятором мышь</translation>
    </message>
    <message>
        <source>You can add here numbe value from 10 to 10000 with step 5</source>
        <translation>Сюда может быть добавленочисло от 10 до 1000 с шагом 5</translation>
    </message>
    <message>
        <source>Tree</source>
        <translation>Дереов</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>Тип</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <source>Expertiment 1</source>
        <translation>Эксперимент 1</translation>
    </message>
    <message>
        <source>Getneral source</source>
        <translation>Общий источник</translation>
    </message>
    <message>
        <source>Ion Current</source>
        <translation>Ионный ток</translation>
    </message>
    <message>
        <source>This node groups all graphics of ion current</source>
        <translation>Узел объединяет графики ионного тока</translation>
    </message>
    <message>
        <source>graph type</source>
        <translation>тип графика</translation>
    </message>
    <message>
        <source>ion current for the selected mass range throughout the experiment</source>
        <translation>ионный ток для отобранного дипазона масс всего эксперимента</translation>
    </message>
    <message>
        <source>IC1</source>
        <translation>ИТ1</translation>
    </message>
    <message>
        <source>graph</source>
        <translation>график</translation>
    </message>
    <message>
        <source>IC2</source>
        <translation>ИТ2</translation>
    </message>
    <message>
        <source>IC3</source>
        <translation>ИТ3</translation>
    </message>
    <message>
        <source>Spectras</source>
        <translation>Спектры</translation>
    </message>
    <message>
        <source>All mass values on limited experiment length range</source>
        <translation>Полный массовый диапазон на указанном временном промежутку эксперимента</translation>
    </message>
    <message>
        <source>Spec 1</source>
        <translation type="unfinished">Спектр 1 </translation>
    </message>
    <message>
        <source>Spec2</source>
        <translation type="unfinished">Спектр 2 </translation>
    </message>
    <message>
        <source>Spec3</source>
        <translation type="unfinished">Спектр 3 </translation>
    </message>
    <message>
        <source>Expertiment 2</source>
        <translation>Эксперимент 2</translation>
    </message>
    <message>
        <source>General source</source>
        <translation>Общий источник</translation>
    </message>
    <message>
        <source>grpah type</source>
        <translation>тип графика</translation>
    </message>
    <message>
        <source>IC Total</source>
        <translation>Общий ионный ток</translation>
    </message>
    <message>
        <source>Accumalated Spec</source>
        <translation>Накопленный спектр</translation>
    </message>
    <message>
        <source>1</source>
        <translation></translation>
    </message>
    <message>
        <source>2</source>
        <translation></translation>
    </message>
    <message>
        <source>Substation</source>
        <translation>Вещество</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Описание</translation>
    </message>
    <message>
        <source>-Substation</source>
        <translation>-Вещество</translation>
    </message>
    <message>
        <source> description</source>
        <translation> описание</translation>
    </message>
    <message>
        <source> Comment</source>
        <translation> Комментарий</translation>
    </message>
    <message>
        <source>Propertiy #</source>
        <translation>Свойство №</translation>
    </message>
    <message>
        <source>No number</source>
        <translation>Не числовое значение</translation>
    </message>
    <message>
        <source>line edit number value must contain less one digit</source>
        <translation>Строка должна содержать хотя бы одну цифру</translation>
    </message>
    <message>
        <source>Number edit not a number</source>
        <translation>В числовом поле присутвсуют не цифры</translation>
    </message>
    <message>
        <source>Item Clicked</source>
        <translation>узел кликнут</translation>
    </message>
    <message>
        <source> item check state </source>
        <translation> галочка на узле </translation>
    </message>
    <message>
        <source>Checked</source>
        <translation type="unfinished"> установлена</translation>
    </message>
    <message>
        <source>Unchecked</source>
        <translation type="unfinished"> снята</translation>
    </message>
    <message>
        <source>Context menu </source>
        <translation>Контекстное меню</translation>
    </message>
    <message>
        <source>Action type </source>
        <translation>Тип действия</translation>
    </message>
    <message>
        <source>downloads</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Resolution</source>
        <translation>Разрешение</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>Низкое</translation>
    </message>
    <message>
        <source>High</source>
        <translation>Высокое</translation>
    </message>
    <message>
        <source>time</source>
        <translation>Время</translation>
    </message>
    <message>
        <source>Ions/Dwell</source>
        <translation>Ионы/Выдержка</translation>
    </message>
    <message>
        <source>[Sim Parameters]</source>
        <translation>[Параметры режима Sim]</translation>
    </message>
    <message>
        <source>INSTRUMENT CONTROL PARAMETERS:</source>
        <comment>0 and 1 to no translation</comment>
        <translation type="obsolete">1111:</translation>
    </message>
    <message numerus="yes">
        <source>%1INSTRUMENT CONTROL PARAMETERS:</source>
        <comment>0 and 1 to no translation</comment>
        <translation type="obsolete">
            <numerusform>%1ccccc%naaaaa:</numerusform>
            <numerusform>%1ccccc%nbbbbb:</numerusform>
            <numerusform>%1ccccc%n:</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>Header in first line</source>
        <comment>0-plural, 1-simple</comment>
        <translation type="obsolete">
            <numerusform>INSTRUMENT CONTROL PARAMETERS:</numerusform>
            <numerusform>ПАРАМЕТРЫ УПРАВЛЕНИЯ ПРИБОРОМ:</numerusform>
            <numerusform>INSTRUMENT CONTROL PARAMETERS:</numerusform>
        </translation>
    </message>
    <message>
        <source>INSTRUMENT CONTROL PARAMETERS:</source>
        <translation>ПАРАМЕТРЫ УПРАВЛЕНИЯ ПРИБОРОМ:</translation>
    </message>
</context>
<context>
    <name>Resume</name>
    <message>
        <source>Resume</source>
        <translation>Отчет</translation>
    </message>
    <message>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <source>Put here sticker</source>
        <translation>Прикрепить наклейку</translation>
    </message>
    <message>
        <source>Create form to store custom text</source>
        <translation>Создать форму с пользовательским текстом</translation>
    </message>
    <message>
        <source>Ctrl+H</source>
        <translation></translation>
    </message>
    <message>
        <source>Click here to add or replace image</source>
        <translation>Нажмите, чтобы добавить или заменить картинку</translation>
    </message>
    <message>
        <source>	Text to report adds by blocks with structure definite. It may contain block for youser picture. Block with header and paragraphs with 3 leve heirachy. Block of substation description (chromatography peaks pictures). Blocks can be repeared.</source>
        <translation>	Текст отчета добавляется блоками определенной структуры. Он может включать картинку пользователя, блок с заголовка и абзацев с тремя типами иерархии. Блок описание веществ (хроматографические пики). Блоки могу повторяться.</translation>
    </message>
    <message>
        <source>	User had ability to export report to PDF ODT or our own format RSM. Ability print report are...</source>
        <translation>	Пользователь может сохранять отчет в PDF, ODT или нашем форматеt RSM. Возможен вывод на печать...</translation>
    </message>
    <message>
        <source>	Navigation by press links work fine...</source>
        <translation>	Навигацие через нажатие линков работате хорошо...</translation>
    </message>
    <message>
        <source>	Customer may add own sticker this comments by pree right button of mouse in any document position</source>
        <translation>	Пользователь может добавлять наклейки с комментариями, нажав правую кнопку мышу в любом месте документа</translation>
    </message>
    <message>
        <source>1. About Report</source>
        <translation type="unfinished">1. Об отчетe</translation>
    </message>
    <message>
        <source>1. About Report (Find by press)</source>
        <translation>1. Об отчета (перейти нажатием)</translation>
    </message>
    <message>
        <source>This block structure not final. Borders, Colors, Fonts, colomns and row count are discussed. Its only demonstrate picrtrues and offers one option</source>
        <translation>Структура блок предварительна. Рамки, Цвета, Шрифты, а тажке количепство рядов и колонок обсуждается</translation>
    </message>
    <message>
        <source>About Chroma peaks pictures</source>
        <translation>О картинка с хроматографическими пиками</translation>
    </message>
    <message>
        <source>2. About Chroma peaks pictures (Find by press)</source>
        <translation>2. О картинка с хроматографическими пиками (перейти нажатием)</translation>
    </message>
    <message>
        <source>Remarks adding with pres right mouse button. They are minimizing them losts focus. They are moving then holded with left button. They are imporeted after paragraph placed under.</source>
        <translation>Пометки добавляются нажатием правой клавиши мыши. Они умешюются, когдатеряют фоку. Они передвигаются с зажатой левой клавишей мыши. Они импортируются в документы далее находящегося под ними параграфа.</translation>
    </message>
    <message>
        <source>About Stickers</source>
        <translation>О наклейках</translation>
    </message>
    <message>
        <source>3. About Stickers (Find by press)</source>
        <translation>3. О наклейках (Перейти нажатием)</translation>
    </message>
    <message>
        <source>Text in blocks has format. Tree predefined foramts are there. Needing and properties are discussed.</source>
        <translation>Текст в блоках форматоирован. Есть три формата. Необходимые настройки обсуждаются.</translation>
    </message>
    <message>
        <source>This block for example copiing format of first block.</source>
        <translation>Этот блок, для примера, копирует по формату первый.</translation>
    </message>
    <message>
        <source>Postscriptum</source>
        <translation>Постскриптум</translation>
    </message>
    <message>
        <source>5. Postscriptum</source>
        <translation>5. Постскриптум</translation>
    </message>
    <message>
        <source>Table example</source>
        <translation>Пример таблицы</translation>
    </message>
    <message>
        <source>4. Link to table example</source>
        <translation>4. Линк на пример таблицы</translation>
    </message>
    <message>
        <source>it is text too</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hor header 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hor header 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Row header 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cell value 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cell value 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Row header 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cell value 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cell value 4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Substation descripture</source>
        <translation>Описание вещества</translation>
    </message>
    <message>
        <source>5. Link to substaton descripture</source>
        <translation>5. Линк на описание вещества</translation>
    </message>
    <message>
        <source>Substation A</source>
        <translation>Вещество А</translation>
    </message>
    <message>
        <source>Substation B</source>
        <translation>Вещество B</translation>
    </message>
    <message>
        <source>Substation C</source>
        <translation>Вещество С</translation>
    </message>
    <message>
        <source>������ ����� �� ����� ����� ��� 
 �� ���� ���� �� ��� ����� � ����</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Print Document</source>
        <translation>Печать докмента</translation>
    </message>
    <message>
        <source>Open File...</source>
        <translation>Открыть файл...</translation>
    </message>
    <message>
        <source>Resume-Files (*.rsm);;HTML-Files (*.htm *.html);;All Files (*)</source>
        <translation>Файлы-очета (*.rsm);;HTML-файлы (*.htm *.html);;Все файлы (*)</translation>
    </message>
    <message>
        <source>Save as...</source>
        <translation>Сохранить как...</translation>
    </message>
    <message>
        <source>Resume-Files (*.rsm);;ODF files (*.odt);;HTML-Files (*.htm *.html);;All Files (*)</source>
        <translation>Фаылй отчета (*.rsm);;ODF файлы (*.odt);;HTML-файлы (*.htm *.html);;Все файлы (*)</translation>
    </message>
    <message>
        <source>%1[*] - %2</source>
        <translation></translation>
    </message>
    <message>
        <source>Resume Edit</source>
        <translation>Редактор очета</translation>
    </message>
    <message>
        <source>.rsm</source>
        <translation></translation>
    </message>
    <message>
        <source>Application</source>
        <translation>Приложение</translation>
    </message>
    <message>
        <source>The document has been modified.
Do you want to save your changes?</source>
        <translation>Документ изменены.
Вы хотите сохранить изменения?</translation>
    </message>
    <message>
        <source>Save As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose optons and save file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save changes to file with previously defined optoins</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Sticker</name>
    <message>
        <source>Sticker</source>
        <translation>Наклейка</translation>
    </message>
    <message>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
    <message>
        <source>Input your text here</source>
        <translation>Веедите текст</translation>
    </message>
</context>
<context>
    <name>TextBrowserImage</name>
    <message>
        <source>Open Image file...</source>
        <translation>Открыть файл изображения</translation>
    </message>
    <message>
        <source>JPEG (*.jpg *.jpeg);;PNG (*.png);;BMP (*.bmp)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>code</name>
    <message>
        <source>Please, wait...</source>
        <translation type="vanished">Пожалуйста, подождите...</translation>
    </message>
    <message>
        <source>Database</source>
        <translation type="vanished">База данных</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">Обновить</translation>
    </message>
    <message>
        <source>Experiments</source>
        <translation type="vanished">Эксперименты</translation>
    </message>
</context>
<context>
    <name>protanalyticClass</name>
    <message>
        <source>protanalytic</source>
        <translation>ПротоАналитик</translation>
    </message>
    <message>
        <source>Spectrum</source>
        <translation type="vanished">Спектр</translation>
    </message>
    <message>
        <source>Database</source>
        <translation>База данных</translation>
    </message>
    <message>
        <source>Formula</source>
        <translation>Формула</translation>
    </message>
    <message>
        <source>Experiment Data</source>
        <translation>Список спектров</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <source>Spectra</source>
        <translation>Спектры</translation>
    </message>
    <message>
        <source>message</source>
        <translation>сообщение</translation>
    </message>
</context>
</TS>
