#ifndef TABLEWITHKEYS_H
#define TABLEWITHKEYS_H

#include <QTableWidget>

class TableWithKeys : public QTableWidget
{
	Q_OBJECT

public:
	TableWithKeys(QWidget *parent = 0);
	~TableWithKeys();
protected:
	void keyPressEvent(QKeyEvent *event) ;
private:
	int m_iCurrentRow;// �� �� ���� ����������� ������� �������� ����������� ���������� ���.
	void rowSelectionChange( int currentRow, int previousRow);
private slots:
	
	void on_cellClicked ( int row, int column ) ;
	void on_cellDoubleClicked(int row, int column);
signals:
	void rowCheckState(int row);
	void rowMenuShow(int row);
};

#endif // TABLEWITHKEYS_H
