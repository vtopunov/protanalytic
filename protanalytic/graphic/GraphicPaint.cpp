#include"GraphicWidget.h"

inline void drawRotatedText(QPainter *painter, qreal degrees, const QRectF& r, const QString &text) {
    painter->save();
    painter->translate(r.x(), r.y());
    painter->rotate(degrees);
    painter->drawText(QRectF(0, 0, r.width(), r.height()), text );
    painter->restore();
}

void draw(QPainter& paint, QPointF* vpt, Coordinate x, Coordinate y, const GraphicLine& lin) {
	const double* xarray = lin.x;
	const double* yarray = lin.y;
	const int narray = lin.n;

	for (int j = 0; j < narray; ++j) {
		vpt[j].setX(x.to_window(xarray[j]));
		vpt[j].setY(y.to_window(yarray[j]));
	}

	paint.setPen(lin.linepen);
	paint.drawPolyline(vpt, narray);
}

bool GraphicWidget::paint(QPainter& paint) {
	if (!on_paint) return true;

    // 1. Calculate grid
	x.prepaint();
	y.prepaint();

    // 2. Calculate draw rect
	const QRectF rc = geometry();
	const QFontMetricsF xtm(x.font());
	const QFontMetricsF ytm(y.font());
	const QSizeF xsztxt = maxGraphicSizeText(x.text().cbegin(), x.text().cend(), xtm);
	const QSizeF ysztxt = maxGraphicSizeText(y.text().cbegin(), y.text().cend(), ytm);
	const limits winx(ysztxt.width() + ytm.width("-") + 5, rc.width() - 0.5 * xsztxt.width() );
	const limits winy(xsztxt.height(), rc.height() - ysztxt.height() - 2 * xsztxt.height());
	if (!winx.valid() || !winy.valid()) return false;

	x.setWindow(winx);
	y.setWindow(winy);

	paint.setPen(axisframepen);
	paint.setBrush(background);
	paint.drawRect(winx.min() - 1, winy.min() - 1, winx.length() + 2, winy.length() + 2);
	paint.setClipRect(winx.min(), winy.min(), winx.length(), winy.length());


    // 3. Drawing grids and labels

    // 3.1 ... by Y-axis
	paint.setFont(y.font());
	QRectF oldrc(0, 0, 1, 1);
	for (auto& lb : y.text()) {
		if (y.real().in(lb.first)) {
			const auto dlg = y.to_window(lb.first);
			paint.setPen(y.gridpen(lb.second));
			paint.setClipping(true);
			paint.drawLine(winx.min(), dlg, winx.max(), dlg);

			if (y.showlable_) {
				const auto cx = ytm.width(lb.second);
				QRectF rc(winx.min() - cx - 3, dlg - ysztxt.height() / 2, cx, ysztxt.height());
				paint.setPen(y.textpen_);
				paint.setClipping(false);
				drawRotatedText(&paint, y.textangle_, rc, lb.second);
				oldrc = rc;
			}
		}

		if (lb.first > y.real().max()) break;
	}

    // 3.2 .. by X-axis
	paint.setFont(x.font());
	oldrc.setRect(0, 0, 1, 1);
	for (auto& lb : x.text()) {
		if (x.real().in(lb.first)) {
			const auto dlg = x.to_window(lb.first);
			paint.setPen(x.gridpen(lb.second));
			paint.setClipping(true);
			paint.drawLine(dlg, winy.min() + 1, dlg, winy.max());
			const auto cx = xtm.width(lb.second);
			QRectF rc(dlg - cx / 2, winy.max() + xsztxt.height() / 3, cx + 3, xsztxt.height());
			if (fabsl(x.textangle_) > 0.1 || !rc.intersects(oldrc)) {
				paint.setPen(x.textpen_);
				paint.setClipping(false);
				drawRotatedText(&paint, x.textangle_, rc, lb.second);
				oldrc = rc;
			}
		}

		if (lb.first > x.real().max()) break;
	}

    // 4. Plot
	//paint.setRenderHint(QPainter::RenderHint::NonCosmeticDefaultPen, true );
	paint.setClipping(true);

	QPointF* vpt = vpoint_.data();
	for (const auto& lin : lines_) {
		draw(paint, vpt, x.coordinate_, y.coordinate_, lin);
	}

	return true;
}

void GraphicWidget::paintEvent(QPaintEvent*) {
    QPainter painter(this);
    paint(painter);
}
