#pragma once

#include <QWidget>

#include "GraphicAxis.h"

class GraphicWidget : public QWidget {
	Q_OBJECT
public:
    typedef std::list<GraphicLine> lines_t;

public:
    GraphicAxis<x_axis_type_tag> x;
    GraphicAxis<y_axis_type_tag> y;
	QPen axisframepen;
	QBrush background;
	bool on_paint;

private:
	lines_t lines_;
	mutable QPointF mpos_;
	mutable std::vector<QPointF> vpoint_;

public:
	GraphicWidget(QWidget* parent = 0) : QWidget(parent), 
        x(), y(), axisframepen(), background(),
        on_paint(),
        mpos_(), lines_(), vpoint_() { clear(); }

	void clear() {
		this->setMouseTracking(true);
		axisframepen = QColor(128,128,128); 
		background = QColor(255,255,255,255);
		on_paint = true;
        mpos_clear();
		x.clear();
		y.clear();
        lines_ = decltype(lines_)();
        vpoint_ = decltype(vpoint_)();
	}

    void addline(const GraphicLine& ln, const limits& exxlim = limits(), const limits& exylim = limits() ) {
        if( valid(ln) ) {
            x.addline(ln, exxlim);
            y.addline(ln, exylim);
            vpoint_.reserve(ln.n);
			lines_.push_back(ln);
		}
	}

	lines_t& lines() { return lines_; }

    bool zoom(double nZoom);
	bool paint(QPainter& paint);

protected:
	virtual void paintEvent(QPaintEvent*) Q_DECL_OVERRIDE;
	virtual void mouseDoubleClickEvent(QMouseEvent* ev) Q_DECL_OVERRIDE;
	virtual void wheelEvent(QWheelEvent* ev) Q_DECL_OVERRIDE;
	virtual void mouseMoveEvent(QMouseEvent* ev) Q_DECL_OVERRIDE;
	virtual void mouseReleaseEvent(QMouseEvent* ev) Q_DECL_OVERRIDE;

private:
    void mpos_clear() { mpos_ = decltype(mpos_)(); }
};

