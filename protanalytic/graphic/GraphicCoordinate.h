#pragma once 

#include <GraphicLimits.h>

inline bool scale(limits& lim, limits::real_t speed_scale, limits::real_t nScale) {
	const auto x0 = lim.mean();
	const auto dx = (lim.length() * powl(1 + speed_scale, nScale)) / 2;
	return lim.set(x0 - dx, x0 + dx);
}

struct Coordinate {
private:
	limits real_;
	limits window_;

public:
	limits::real_t to_real(limits::real_t x) const { return window_.to(real_, x); }

	limits::real_t to_window(limits::real_t x) const { return real_.to(window_, x); }

	void setReal(const limits& real) {
		real_ = real;
	}

	void setWindow(const limits& window) {
		window_ = window;
	}

	const limits& window() const {
		return window_;
	}

	const limits& real() const {
		return real_;
	}

	bool move(limits::real_t dx) {
		return real_.set(to_real(window_.min() + dx), to_real(window_.max() + dx));
	}

	bool scale(limits::real_t speed_scale, limits::real_t nScale) {
		return ::scale(real_, speed_scale, nScale);
	}
};
