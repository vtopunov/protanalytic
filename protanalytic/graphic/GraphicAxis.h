#pragma once

#include <map>

#include <QWidget>
#include <QtGui>

#include <GraphicLine.h>

#undef min
#undef max

typedef ::num_sequence<long double> grid_t;

class GraphicWidget;

struct x_axis_type_tag {};
struct y_axis_type_tag {};

inline const double* garray(const GraphicLine& ln, x_axis_type_tag) { return ln.x; }
inline const double* garray(const GraphicLine& ln, y_axis_type_tag) { return ln.y; }

inline limits unilimits(const limits& lim, x_axis_type_tag ) { return lim; }
inline limits unilimits(const limits& lim, y_axis_type_tag ) { return lim.inv(); }

inline grid_t grid(const limits& lim, const limits::real_t h) {
    return grid_t( ceill(lim.min()/h) * h, h, lim.max() );
}

inline limits::real_t h_round_grid(const limits& lim, limits::real_t round) {
    return powl(10., static_cast<int>( log10l( round * lim.length() ) ) );
}

inline grid_t round_grid(const limits& lim, limits::real_t round ) {
    const auto h = powl(10., static_cast<int>( log10l( round * lim.length() ) ) );
	return grid(lim, h);
}

inline grid_t grid(const limits& lim, limits::real_t minngrid, limits::real_t maxngrid) { // ����������� ���������� ����� 
    auto h = h_round_grid(lim, 0.1); // ��� �����
	if( lim.length() / h < minngrid ) h *= 0.1; // ��� ���� ����� ������, ������� ���� �����
	if( lim.length() / h > maxngrid ) h *= 10.; // ��� ���� ����� ������, c������ ����� �����
    return grid_t( ceill(lim.min()/h) * h, h, lim.max() );
}


template<class It>
inline QSizeF maxGraphicSizeText(It begin, It end, const QFontMetricsF& tm ) {
	double w = 0;
	double maxw = tm.width("000");
	for(; begin != end; ++begin ) {
		w = tm.width( begin->second );
		if( w > maxw ) maxw = w;
	}
	return QSize(maxw, tm.height());
}

typedef std::map<double, QString> labelsmap_t;
inline bool addlabel(labelsmap_t& map, double value, QString text, double eps = 1.0e-5) {
	if( map.empty() ) { map[value] = text; return true; }

    auto lb = map.lower_bound( value );
    auto it = lb;
	if( it != map.end() ) {
        if( std::fabs(it->first - value) < eps ) return false; // ������� ��� ����
	}
	if( it != map.begin() ) {
        if( std::fabs((--it)->first - value) < eps ) return false; // ������� ��� ����
	}

	map.insert(lb, labelsmap_t::value_type(value, text) );
	return true;
}

class GraphicWidget;

template<class axis_type>
struct GraphicAxis {
public:
	enum{ labeldiv = 100 };

public:
	QPen gridpen_;
	std::map<QString, QPen> gridpens_;
	QPen textpen_;
	QFont textfont_;
    double textangle_;
	bool autogrid_;
	bool showlable_;
	double maxngrid_;
	double minngrid_;
	QString labelpostfix_;
	QString labelprefix_;

private:
	labelsmap_t labels_;
	labelsmap_t text_;
	std::map<QString, QPen> gridpens__;

public:
    limits::real_t speed_scale_;
    limits::real_t speed_move_;

private:
	Coordinate coordinate_;
	limits save_real_;

public:
    GraphicAxis() : gridpen_(), gridpens_(), textpen_(), textfont_(),
		textangle_(), autogrid_(), showlable_(), maxngrid_(), minngrid_(), labelpostfix_(),
        labelprefix_(), labels_(), text_(), gridpens__(),
        speed_scale_(), speed_move_(), coordinate_(), save_real_() { clear(); }

	void clear() {
		gridpen_ = QColor(0,255,0);
		textpen_ = QColor(0,0,0);
		textfont_ = QFont();
        textangle_ = 0;
		autogrid_ = true;
		showlable_ = true;
		labelpostfix_ = decltype(labelpostfix_)();
		labelprefix_ = decltype(labelprefix_)();

        labels_ = decltype(labels_)();
		text_ = decltype(text_)();
		gridpens_ = decltype(gridpens_)();
		speed_scale_ = 0.1; 
		speed_move_ = 1.0;
		coordinate_ = decltype(coordinate_)();
		save_real_ = decltype(save_real_)();
		maxngrid_ = 15;
		minngrid_ = 5;
	}
	
    void setReal(limits::real_t min, limits::real_t max ) { setReal(limits(min, max) ); }

	void setReal(const limits& lim) {
		if( lim.valid() ) {
			coordinate_.setReal(lim);
			save_limits();
		}
	}

	const limits& real()const { return coordinate_.real(); }

	void setWindow(const limits& window__) { coordinate_.setWindow( unilimits(window__, axis_type()) ); }
    limits window() { return unilimits(coordinate_.window(), axis_type()); }

    limits::real_t to_real(limits::real_t x) { return coordinate_.to_real(x); }
	limits::real_t to_window(limits::real_t x) { return coordinate_.to_window(x); }

    static constexpr limits::real_t addlimmin(x_axis_type_tag) { return 0.01; }

    static constexpr limits::real_t addlimmin(y_axis_type_tag) { return 0.0; }

    template<class type>
	static constexpr limits::real_t addlimmax(type) { return 0.01; }

    void addline(const GraphicLine& ln, const limits& exlim = limits() ) {
        if( valid(ln) ) {
            auto g = garray(ln, axis_type());
            auto minmax = std::minmax_element(g, g+ln.n);
            limits glim(*minmax.first, *minmax.second);

            bool is_set = false;
            if( glim.min() < real().min() ) { is_set = true; } else { glim.min_ = real().min(); }
            if( glim.max() > real().max() ) { is_set = true; } else { glim.max_ = real().max(); }

            if( is_set ) {
                const auto addabsmax = addlimmax(axis_type()) * glim.length();
                const auto addabsmin = addlimmin(axis_type()) * glim.length();
                glim.max_ += addabsmax;
                glim.min_ -= addabsmin;

                if( exlim.valid() ) {
                    glim.max_ = std::max(glim.max_, exlim.max() + addabsmax );
                    glim.min_ = std::min(glim.min_, exlim.min() - addabsmin );
                }

                setReal(glim);
            }
        }
	}


    bool move(limits::real_t dx) {
		return coordinate_.move(speed_move_ * dx);
	}

    bool scale(limits::real_t nScale) { 
		return coordinate_.scale(speed_scale_, nScale);
	}

	void save_limits() { save_real_ = real(); }
	void restore_limits() { setReal( save_real_ ); }

	bool addlabel(QString value, QString text) {
        if( ( value.isEmpty() && text.isEmpty() ) || !real().valid() ) return false;
		if( text.isEmpty() )  { text = value; }
		if( value.isEmpty() ) { value = text; };
        bool ok = false;
        double real_value = value.toDouble(&ok);
        return ok && ::addlabel(labels_, real_value, text, real().length() / labeldiv );
	}

	const labelsmap_t& labels()const { return labels_; }
	const QFont& font()const { return textfont_; }

	void prepaint() { textinit(); }

	QPen gridpen(const QString& lb) {
		auto it = gridpens__.find(lb);
		return ( it == gridpens__.end() ) ? gridpen_ : it->second;	
	}

private:
	void textinit() {
		text_ = labels_;
		gridpens__ = gridpens_;
		if(autogrid_) {
			auto grid = ::grid(real(), minngrid_, maxngrid_);
            for(auto x : grid ) {
                ::addlabel(text_, x, to_string(x), real().length() / labeldiv );
			}
		}

		for(auto it = text_.begin(); it != text_.end(); ++it ) {
			it->second = labelprefix_ + it->second + labelpostfix_;
		}
	}

	const labelsmap_t& text()const { return text_; }

    static QString to_string(long double value) {
        auto format = ( fabs(value) < 1.0e7 ) ? 'f' : 'g';
        auto s = QString::number(value, format);
        if( format == 'f' && !s.isEmpty() ) {
            auto pos = s.end()-1;
            int nchop = 0;
            for(; (pos != s.begin()) && (*pos == '0') ; --pos, ++nchop );
            if( *pos == '.' ) ++nchop;
            s.chop(nchop);
        }
        return s;
    }

	friend class GraphicWidget;
};
