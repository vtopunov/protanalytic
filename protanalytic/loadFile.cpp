#include <protanalytic.h>
#include <converter.h>

bool loadFile(const QString& fileName) {
	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly)) {
		qWarning() << "error: can not open file" << fileName;
		return false;
	}

	auto data = file.readAll();
	if (data.isEmpty()) {
		qWarning() << "error: empty file" << fileName;
		return false;
	}

	auto result = Converter::serializeExperiment(std::move(data));
	if (result.isEmpty()) {
		qWarning() << "error: invalid serialize";
		return false;
	}

    auto client = make_pg_client();

    auto experimentName = QFileInfo(fileName).absoluteDir().dirName();

    client.exec("BEGIN");

    auto addExperimentCommand =
        Sql("INSERT INTO experiment (name, data) VALUES ($1, $2::bytea) RETURNING (id)")
        .arg(experimentName).arg(result);

    auto experement_id = client.exec(std::move(addExperimentCommand)).row(0).column(0).toInt();

    qWarning() << "\n RETURNING (id): " << experement_id << '\n';

#ifdef _DEBUG
    Experiment experiment;
    if (!experiment.ParseFromArray(result.constData(), result.size())) {
        qWarning() << "error: invalid format file\n";
        return false;
    }

    qDebug() << "spectrum count = " << experiment.spectrum_size();
    for (int i = 0; client.isOk() && i < experiment.spectrum_size(); ++i) {
        auto spectrum_name = experimentName + "/" + QString::number(i);
        auto spectrum = experiment.spectrum(i);
        auto size = spectrum.ByteSize();
        QByteArray data;
        data.resize(size);
        spectrum.SerializeToArray(data.data(), size);
        auto add_spectrum_cmd =
            Sql("INSERT INTO spectrum(experimentid, name, data) VALUES($1, $2, $3::bytea)")
            .arg(experement_id).arg(spectrum_name).arg(std::move(data));

        client.exec(std::move(add_spectrum_cmd));
    }
#endif

    client.exec("COMMIT");

    return client.isOk();
}
