#ifndef NISTDIALOG_H
#define NISTDIALOG_H

#include <QDialog>
#include "ui_NistDialog.h"
#include <QMenu>

class NistDialog : public QDialog
{
	Q_OBJECT

public:
	NistDialog(QWidget *parent = 0);
	~NistDialog();

private:
	QMenu menu_tree;
	Ui::NistDialog ui;
	private slots:
	void on_pushbutton();
	void on_NumberEditingFinished();
	void on_itemChanged(QTreeWidgetItem *item, int column);
	void on_contextMenu(QPoint point);
};

#endif // NISTDIALOG_H
