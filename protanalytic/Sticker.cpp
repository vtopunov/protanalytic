#include "Sticker.h"
#include <QMouseEvent>
#include <QPainter>
Sticker::Sticker(QWidget *parent)
	: QWidget(parent),
	grabbed_(false),
	int_status_()
{
	ui.setupUi(this);
}

Sticker::~Sticker()
{

}

void Sticker::setText(const QString & str)
{
	ui.textEdit->setPlainText(str);
}

QString Sticker::getText() const
{
	return ui.textEdit->toPlainText();
}

void Sticker::mouseMoveEvent(QMouseEvent *event)
{
	if (this->rect().contains(event->pos())) {
		// Mouse over Widget
		setCursor(Qt::ArrowCursor);
		//		m_bActive = false;
	}
	else {
		// Mouse out of Widget
		//		m_bActive = true;
	}
	//	setCursor(Qt::ArrowCursor);

	QSize sizeParent = parentWidget()->size();
	if (grabbed_)	{
		int iShiftX = event->x() - pointStartPosition.x();
		int iShiftY = event->y() - pointStartPosition.y();
		int x = pos().x() + iShiftX;
		int y = pos().y() + iShiftY;

		if (x < 0)
			x =0;
		if (x > static_cast<QWidget*>(parent())->width() - 32 - 2 * ui.verticalLayout->spacing())
			x = static_cast<QWidget*>(parent())->width() - 32 - 2 * ui.verticalLayout->spacing();
		if (y < 0)
			y = 0;
		if (y > static_cast<QWidget*>(parent())->height() - 32 - 2 * ui.verticalLayout->spacing())
			y = static_cast<QWidget*>(parent())->height() - 32 - 2 * ui.verticalLayout->spacing();

		move(x, y);
	}

}

void Sticker::mouseReleaseEvent(QMouseEvent *event)
{
	grabbed_ = false;
}

void Sticker::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		grabbed_ = true;
		pointStartPosition = event->pos();
	}
}

void Sticker::leaveEvent(QEvent * event)
{
	int_status_ = 0;
}

void Sticker::enterEvent(QEvent * event)
{
	setAutoFillBackground(true);
	int_status_ = 1;
//	ui.pushButton_Printed->setIcon(QIcon(":/textedit/pictures/comment_edit.png"));
	resize(QSize(300, 200));
	ui.label_Author_Date->show(); 
//	ui.pushButton_Printed->show();
	ui.textEdit->show();
	ui.textEdit->setFocus();
}

void Sticker::paintEvent(QPaintEvent *event)
{
	QPainter painter;
	painter.begin(this);
	if (int_status_)
		painter.drawPixmap(ui.verticalLayout->margin(), ui.verticalLayout->margin(), 32, 32, QPixmap(":/textedit/pictures/comment_edit.png"));
	else
		painter.drawPixmap(0, 0, 24, 24, QPixmap(":/textedit/pictures/comment.png"));
	painter.end();
	QWidget::paintEvent(event);
}

void Sticker::setMinimal()
{
	setAutoFillBackground(false);
	int_status_ = 0;
	ui.label_Author_Date->hide();
//	ui.pushButton_Printed->hide();
	ui.textEdit->hide();
	resize(QSize(24, 24));
	update();
}