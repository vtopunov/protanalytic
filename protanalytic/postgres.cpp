#ifdef WIN32
	#include <Windows.h>
#endif

#undef min
#undef max

#include <QtCore>
#include <t_pg.h>

bool postgresStart() {
	static bool once = false;
	if (once) {
		return false;
	}
	once = true;

	auto old = QDir::currentPath();

	if (!QDir::setCurrent("../pgsql")) {
        qWarning() << "can not open postgres directory";
		return false;
	}

	struct temp_cd {
		QString old;
		~temp_cd() { QDir::setCurrent(old); }
	} tmp = { old };
	(void)tmp;

	auto pgdata = QDir("data");
	if (!pgdata.exists()) {
        qWarning() << "postgres data directory don't exist";
		return false;
	}

	qunsetenv("PGDATA");
	qputenv("PGDATA", pgdata.absolutePath().toLocal8Bit());

	if (!QDir::setCurrent("./bin")) {
        qWarning() << "can not open	./pgsql/bin";
        return false;
	}

	bool success = false;

#ifdef WIN32 // QProcess::startDetached using facking CREATE_NEW_CONSOLE
	PROCESS_INFORMATION pinfo;
	STARTUPINFOA startupInfo;
	memset(&startupInfo, 0, sizeof(startupInfo));
	startupInfo.cb = sizeof(startupInfo);
	const DWORD CreationWindowFlags = (_DEBUG) ? CREATE_NEW_CONSOLE : CREATE_NO_WINDOW;
    success = CreateProcessA(0, "pg_ctl start -w",
		0, 0, FALSE, CREATE_UNICODE_ENVIRONMENT | CreationWindowFlags, 0, 0, &startupInfo, &pinfo);

	if (success) {
		CloseHandle(pinfo.hThread);
		CloseHandle(pinfo.hProcess);
	}
#else 
    success = QProcess::startDetached("./pg_ctl start -w");
    if( !success ) {
        qWarning() << "can not start detached pg_ctl";
    }
#endif

	return success;
}

PgClient make_pg_client() {
	enum{ n_reconnect = 5 };

	static struct {
		QAtomicInt counter_;
		struct Lock {
			~Lock() { counter_->deref(); }
			int ref;
			QAtomicInt* counter_;
		};

		Lock lock() {
			return{ counter_.ref(), &counter_ };
		}

	} onceEntery;
	auto lock = onceEntery.lock();
	if (lock.ref > 1) {
        return PgClient();
    }

#ifdef Q_OS_MAC
    const QString conString = "hostaddr=127.0.0.1 port=5432 dbname=postgres connect_timeout=1";
#else
	const QString conString = "hostaddr=127.0.0.1 port=5432 dbname=postgres user=postgres connect_timeout=1";
#endif
	PgClient client(conString);

	if (!client.isOk()) {
		if (postgresStart()) {
			enum { n_sleep = 5 };
			for (int i = 0; i < n_sleep; ++i) {
				QCoreApplication::processEvents();
				QThread::msleep(100);
			}
		}
	}

	if (!client.isOk()) {
		for (int i = 0; i < n_reconnect; ++i) {
			QCoreApplication::processEvents();
			client = PgClient(conString);
			if (client.isOk()) {
				break;
			}
			qDebug() << "postgres reconnect";
			QThread::msleep(100);
		}
	}


	return std::move(client);
}
