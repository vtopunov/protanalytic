#ifndef MODELESSWINDOW_H
#define MODELESSWINDOW_H

#include <QDialog>
class QGridLayout;
class QLineEdit;
class QPushButton;
class QComboBox;
class QSpinBox;
class TableWithKeys;
class QTableWidget;
class ModelessWindow : public QDialog
{
	Q_OBJECT

public:
	ModelessWindow(QWidget *parent);
	~ModelessWindow();
	void translateUI();
private:
	// GUI
	QLineEdit * lineedit_number_;
	QSpinBox * spinbox_spinbox_;
	QLineEdit * lineedit_text_;
	QComboBox * combobox_combobox_;
	QPushButton * pushbutton_pushbutton_;
	TableWithKeys * tablekeys_properties_;
	QTableWidget * tablewidget_NIST_;
	public slots :
		void on_show();
		void on_pushbutton();
		void on_NumberEditingFinished();
};

#endif // MODELESSWINDOW_H
