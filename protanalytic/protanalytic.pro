TEMPLATE = app
macx {
	CONFIG += c++11
	DEFINES += HAVE_PTHREAD
	QMAKE_CFLAGS += -gdwarf-2
	QMAKE_CXXFLAGS += -gdwarf-2
	QMAKE_INFO_PLIST= $${PWD}/Info.plist
	ICON = $${PWD}/icon.icns
	debug { DEFINES += _DEBUG }
}
DESTDIR = $$PWD/../bin/
QT += core gui printsupport svg widgets
DEFINES += ELOG_H PG_PORT_H _SCL_SECURE_NO_WARNINGS
INCLUDEPATH += $$PWD/../converter $$PWD/../loader $$PWD/../pgsql/include $$PWD/graphic .
LIBS += -L$$PWD/../bin/ -L$$PWD/../pgsql/lib/
macx { LIBS += -lpq } else { LIBS += -llibpq }
HEADERS += $$PWD/ModelessWindow.h $$PWD/NistDialog.h $$PWD/Resume.h $$PWD/ResumeEdit.h $$PWD/ResumeUser.h $$PWD/Sticker.h $$PWD/TableWithKeys.h $$PWD/graphic/GraphicWidget.h $$PWD/protanalytic.h $$PWD/qpgserver.h
SOURCES += $$PWD/../converter/converter.cpp $$PWD/../converter/google/protobuf/arena.cc $$PWD/../converter/google/protobuf/arenastring.cc $$PWD/../converter/google/protobuf/descriptor.cc $$PWD/../converter/google/protobuf/descriptor.pb.cc $$PWD/../converter/google/protobuf/descriptor_database.cc $$PWD/../converter/google/protobuf/dynamic_message.cc $$PWD/../converter/google/protobuf/extension_set.cc $$PWD/../converter/google/protobuf/extension_set_heavy.cc $$PWD/../converter/google/protobuf/generated_message_reflection.cc $$PWD/../converter/google/protobuf/generated_message_util.cc $$PWD/../converter/google/protobuf/io/coded_stream.cc $$PWD/../converter/google/protobuf/io/printer.cc $$PWD/../converter/google/protobuf/io/strtod.cc $$PWD/../converter/google/protobuf/io/tokenizer.cc $$PWD/../converter/google/protobuf/io/zero_copy_stream.cc $$PWD/../converter/google/protobuf/io/zero_copy_stream_impl.cc $$PWD/../converter/google/protobuf/io/zero_copy_stream_impl_lite.cc $$PWD/../converter/google/protobuf/map_field.cc $$PWD/../converter/google/protobuf/message.cc $$PWD/../converter/google/protobuf/message_lite.cc $$PWD/../converter/google/protobuf/reflection_ops.cc $$PWD/../converter/google/protobuf/repeated_field.cc $$PWD/../converter/google/protobuf/service.cc $$PWD/../converter/google/protobuf/stubs/atomicops_internals_x86_gcc.cc $$PWD/../converter/google/protobuf/stubs/atomicops_internals_x86_msvc.cc $$PWD/../converter/google/protobuf/stubs/common.cc $$PWD/../converter/google/protobuf/stubs/once.cc $$PWD/../converter/google/protobuf/stubs/stringprintf.cc $$PWD/../converter/google/protobuf/stubs/structurally_valid.cc $$PWD/../converter/google/protobuf/stubs/strutil.cc $$PWD/../converter/google/protobuf/stubs/substitute.cc $$PWD/../converter/google/protobuf/text_format.cc $$PWD/../converter/google/protobuf/unknown_field_set.cc $$PWD/../converter/google/protobuf/wire_format.cc $$PWD/../converter/google/protobuf/wire_format_lite.cc $$PWD/../converter/spectrum.pb.cc $$PWD/ModelessWindow.cpp $$PWD/NistDialog.cpp $$PWD/Resume.cpp $$PWD/ResumeEdit.cpp $$PWD/ResumeUser.cpp $$PWD/Sticker.cpp $$PWD/TableWithKeys.cpp $$PWD/TextBrowserImage.cpp $$PWD/TextEditImage.cpp $$PWD/graphic/GraphicMouseDoubleClickEvent.cpp $$PWD/graphic/GraphicMouseMove.cpp $$PWD/graphic/GraphicPaint.cpp $$PWD/graphic/GraphicWheelEvent.cpp $$PWD/graphic/GraphicZoom.cpp $$PWD/loadFile.cpp $$PWD/main.cpp $$PWD/postgres.cpp $$PWD/protanalytic.cpp
FORMS += $$PWD/NistDialog.ui $$PWD/Resume.ui $$PWD/ResumeUser.ui $$PWD/Sticker.ui $$PWD/protanalytic.ui
RESOURCES += $$PWD/protanalytic.qrc
