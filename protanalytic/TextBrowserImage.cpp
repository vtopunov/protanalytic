#include "TextBrowserImage.h"
#include <QMimeData>
#include <QFileInfo>
#include <QImageReader>
#include <QMap>
#include <QTextDocumentFragment>
#include <QTextBlock>
#include <QTemporaryDir>
#include <QDataStream>
#include <QKeyEvent>
#include <QDebug>
#include <QUrl>
#include "Sticker.h"
#include <QFileDialog>
#include <QPushButton>
#include <QScrollBar>
TextBrowserImage::TextBrowserImage(QWidget *parent) :
QTextBrowser(parent),
document_clone_(0),
image_count()
{
	connect(this, &QTextBrowser::anchorClicked, this, &TextBrowserImage::on_anchorClicked);
//	setOpenLinks(false);
}

QByteArray TextBrowserImage::docToArray(const QTextDocument* doc)
{
	QMap<QString, QImage> map_images;
	QString str_HTML = doc->toHtml();
	// �������� ���� HTML.

	int count = 0;
	int pos = 0;
	QRegExp rx("<img src=\".+/>");
	while ((pos = rx.indexIn(str_HTML, pos)) != -1)
	{
		// Making html to serialyze
		QString str_newName(QString("image_%1").arg(count));
		int indexLast = str_HTML.indexOf(QString("\""), pos+10);
		int indexFirst = str_HTML.indexOf(QString("\""), pos) + 1;
		QString str_oldName = str_HTML.mid(indexFirst, indexLast - indexFirst);
		str_HTML.replace(indexFirst, indexLast - indexFirst, str_newName);
//		str_HTML.replace(pos, rx.matchedLength(), strToReplace);
		pos += rx.matchedLength();
		count++;

		QVariant image_data = doc->resource(QTextDocument::ImageResource, QUrl(str_oldName));
		QImage picture = image_data.value<QImage>();
		map_images.insert(str_newName, picture);
	}
	QByteArray doc_data;
	QDataStream out(&doc_data, QIODevice::WriteOnly);
	out << str_HTML << map_images;
	return doc_data;
}

void TextBrowserImage::arrayToDoc(const QByteArray* data, QTextDocument* doc_new)
{
	QString html;
	QDataStream in(*data);
	QMap<QString, QImage> map_images;
	in >> html >> map_images;// Reading html and images with tags


	for (auto image : map_images)
	{
		QString key = map_images.key(image);
		QUrl url = makeTempFile(image);
		QString path = url.path();
		html.replace(key, url.path());
	}
	doc_new->setHtml(html);
}

QString TextBrowserImage::makeTempFile(const QImage&  img)
{
	static QString str_temp_dir;
	if (str_temp_dir.isEmpty()){
		static QTemporaryDir dir;
		if (dir.isValid())
			str_temp_dir = dir.path() + '/';
	}

	static int image_count = 0;
	QString str_file_path = str_temp_dir + QString("dropped_image_%1.png").arg(++image_count);
	// � ����������� � ��������� �������
	img.save(str_file_path, "PNG", 50);
	return str_file_path;
}


bool TextBrowserImage::isPositionReadOnly() const
{
	QTextCursor cursor_curr = textCursor();
	int state = cursor_curr.block().userState();
	if (state == 0)
		return true;
	return false;
}

//void TextBrowserImage::mousePressEvent(QMouseEvent * event)
//{
//	QTextCursor cursor_curr = textCursor();
//	QString str_text = cursor_curr.block().text();
//	if (cursor_curr.block().text().startsWith(tr("<a href = \"")))
//		str_text = "";
//	else
//	QTextBrowser::mousePressEvent(event);
//}

void TextBrowserImage::on_anchorClicked(const QUrl & link)
{
	auto cursor = textCursor();
	
	QTextCharFormat format_plain;
	QTextBlockFormat format_plain_block;
	/*QFont font_plain = format_plain.font();
	font_plain.setPointSize(12);
	format_plain.setFont(font_plain);
	format_plain_block.setTopMargin(10);
	format_plain_block.setBottomMargin(10);*/

	// int iRes;
	int link_val = cursor.block().userState();
	int link_status = link_val & 0xF;
	// leave cursor on link position.
	QTextCursor cursor_cut(cursor);
	QTextCursor cursor_format(cursor);
	cursor_cut.movePosition(QTextCursor::NextBlock);
	switch (link_status)
	{
	case static_cast<int>(Mark::link_text) :
	{
		break;
	}
	case static_cast<int>(Mark::link_image) :
	{
		QString fn = QFileDialog::getOpenFileName(this, tr("Open Image file..."),
			QString(), tr("JPEG (*.jpg *.jpeg);;"
			"PNG (*.png);;BMP (*.bmp)"));
		if (fn.isEmpty())
			return;
		QImage img(fn);
		QImage image = img.scaledToWidth(lineWrapColumnOrWidth(), Qt::SmoothTransformation);
		QString url = TextBrowserImage::makeTempFile(image);

		auto doc = document();
		doc->addResource(QTextDocument::ImageResource,
			url, QVariant(image));

		

		bool b_replaced = false;
		auto cursor = textCursor();
		QTextBlock block = cursor.block();
		for (int i = 0; i < 3; ++i){// Search only in first next blocks
			if (block.isValid()) {
				QTextBlock::iterator it;
				for (it = block.begin(); !(it.atEnd()); ++it) {
					QTextFragment currentFragment = it.fragment();
					QTextImageFormat imageFormat = currentFragment.charFormat().toImageFormat();
					if (imageFormat.isValid()){// We finde first picture
						imageFormat.setName(url);
						imageFormat.setWidth(image.width());
						imageFormat.setHeight(image.height());
						QTextCursor helper = cursor;

						
						helper.setPosition(currentFragment.position());
						helper.setPosition(currentFragment.position() + currentFragment.length(),
							QTextCursor::KeepAnchor);
						int int_Start_y = verticalScrollBar()->value() + cursorRect(helper).bottom();
						helper.setCharFormat(imageFormat);
						int int_End_y = verticalScrollBar()->value() + cursorRect(helper).bottom();
						correctStickers(int_End_y - int_Start_y);
						b_replaced = true;
						break;
					}
				}
			}
			block = block.next();
		}

		if (!b_replaced)
		{
				int int_Start_y = verticalScrollBar()->value() + cursorRect(cursor).top();
			cursor.insertBlock(format_plain_block, format_plain);
			cursor.block().setUserState(static_cast<int>(Mark::user_image));
			//	The image can be inserted into the document using the QTextCursor API :
			QTextImageFormat imageFormat;
			imageFormat.setWidth(image.width());
			imageFormat.setHeight(image.height());
			imageFormat.setName(url);
			cursor.insertImage(imageFormat);
			cursor.insertBlock();
			int int_End_y = verticalScrollBar()->value() + cursorRect(cursor).top();
			correctStickers(int_End_y - int_Start_y);
		}
		break;
	}
	case static_cast<int>(Mark::link_ref_text) :
	{
		int ref = 0xF0 & link_val;
		auto block = cursor.block();
		int iBlock_count = 0;
		while (block.isValid())
		{
			iBlock_count++;
			if (ref == block.userState())
			{
				QTextCursor c(block);
				verticalScrollBar()->setValue(
					cursorRect(c).top() + 2);
				return;
			}
			block = block.next();
		}
		break;
	}
	}
									
}

//void TextBrowserImage::focusInEvent(QFocusEvent * event)
//{
//	if (resume_user_ && resume_user_->isVisible())
//		resume_user_->accept();
//	return;
//}

void TextBrowserImage::on_Put_Sticker()
{
	auto sticker = new Sticker(viewport());
	QPoint pos(mapFromGlobal(QCursor::pos()));
	sticker->move(
		QPoint(pos - QPoint(20, 20))
		);
	sticker->show();
}

int TextBrowserImage::getStickersCount() const
{
	return viewport()->children().count();
}

const QObjectList& TextBrowserImage::lst_stickers() const
{
	return viewport()->children();
}

QByteArray TextBrowserImage::toArray() const
{
	QMap<QString, QImage> map_images;
	QString str_HTML = document()->toHtml();
	// �������� ���� HTML.

	int count = 0;
	int pos = 0;
	QRegExp rx("<img src=\".+/>");
	while ((pos = rx.indexIn(str_HTML, pos)) != -1)
	{
		// Making html to serialyze
		QString str_newName(QString("image_%1").arg(count));
		int indexLast = str_HTML.indexOf(QString("\""), pos + 10);
		int indexFirst = str_HTML.indexOf(QString("\""), pos) + 1;
		QString str_oldName = str_HTML.mid(indexFirst, indexLast - indexFirst);
		str_HTML.replace(indexFirst, indexLast - indexFirst, str_newName);
		//		str_HTML.replace(pos, rx.matchedLength(), strToReplace);
		pos += rx.matchedLength();
		count++;

		QVariant image_data = document()->resource(QTextDocument::ImageResource, QUrl(str_oldName));
		QImage picture = image_data.value<QImage>();
		map_images.insert(str_newName, picture);
	}
	QByteArray doc_data;
	QDataStream out(&doc_data, QIODevice::WriteOnly);
	out << str_HTML << map_images;
	// Serialize stickers
	// Count of stickers
	int iAppendToY = verticalScrollBar()->value();
	out << static_cast<quint32>(viewport()->children().count());
	for (auto obj_sticker : viewport()->children())
	{
		// if chile is sticker (by now it always)
		auto sticker = dynamic_cast<Sticker*>(obj_sticker);
		if (!sticker)
			continue;
		// corrected positon
		out << static_cast<quint32>(sticker->pos().x()) << static_cast<quint32>(sticker->pos().y() + iAppendToY);
		// text of sticker
		out << sticker->getText();
	}
	return doc_data;
}

void TextBrowserImage::fromArray(const QByteArray* data)
{
	for (auto obj_sticker : viewport()->children())
	{
		// if chile is sticker (by now it always)
		auto sticker = dynamic_cast<Sticker*>(obj_sticker);
		if (sticker)
			sticker->deleteLater();
	}

	QString html;
	QDataStream in(*data);
	QMap<QString, QImage> map_images;
	in >> html >> map_images;// Reading html and images with tags


	for (auto image : map_images)
	{
		QString key = map_images.key(image);
		QUrl url = makeTempFile(image);
		QString path = url.path();
		html.replace(key, url.path());
	}
	document()->setHtml(html);
	quint32 stickers_count;
	in >> stickers_count;
	quint32 x;
	quint32 y;
	for (int i = 0; i < stickers_count; ++i){
		auto * sticker = new Sticker(viewport());
		in >> x >> y;
		sticker->move(x, y);
		sticker->show();
		QString sticker_text;
		in >> sticker_text;
		sticker->setText(sticker_text);
	}
}

void TextBrowserImage::focusInEvent(QFocusEvent * event)
{
	for (auto widg : viewport()->children())
	{
		auto sticker = static_cast<Sticker*>(widg);
		if (sticker)
			sticker->setMinimal();
	}
}

void TextBrowserImage::correctStickers(int shift_y)
{
	for (auto obj : viewport()->children()){

		auto widg = static_cast<QWidget*>(obj);
		if (widg){
			int y_new = widg->pos().y() + shift_y;
			widg->move(widg->pos().x(), y_new);
		}
	}
}

QTextDocument* TextBrowserImage::document_with_stickers()
{
	if (document_clone_)
		delete document_clone_;
	document_clone_ = document()->clone(this);

	document_clone_->setTextWidth(document()->textWidth());

	QList<QTextCursor> lst_dest_curs;
//	int iAppendToY = verticalScrollBar()->value();
	int iAppendToY = 0;
	// Creating curosor by absolute position of stickers
	for (auto obj : viewport()->children())
	{
		auto sticker = static_cast<Sticker*>(obj);
		if (sticker)
		{
			QTextCursor cursor_source = cursorForPosition(QPoint(sticker->pos().x(), sticker->pos().y() + iAppendToY));
			QTextCursor cursor_dest(document_clone_);
			cursor_dest.setPosition(cursor_source.position());
			lst_dest_curs.append(cursor_dest);
		}
	}

	QTextBlockFormat block_format;
	QTextCharFormat char_format;
	QFont font = char_format.font();
	font.setItalic(true);
	font.setPixelSize(8);
	int iCount = 0;
	for (auto obj : viewport()->children())
	{
		auto sticker = static_cast<Sticker*>(obj);
		if (sticker)
		{
			QTextCursor cursor = lst_dest_curs[iCount];
			++iCount;
			cursor.movePosition(QTextCursor::NextBlock);
			cursor.insertBlock(block_format, char_format);
			cursor.insertText(sticker->getText());
			cursor.insertBlock(block_format, char_format);
		}
	}
	return document_clone_;
}





