#include "Resume.h"
#include <QTextBlock>
#include <QPrintPreviewDialog>
#include <QPrinter>
#include <QStyleFactory>
#include <QFileDialog>
#include <QFileInfo>
#include <QPrintDialog>
#include <QTextCodec>
#include <QTextDocumentWriter>
#include <QMessageBox>
Resume::Resume(QWidget *parent)
: QDialog(parent), ref_cout()
{
	ui.setupUi(this);
	fileNew();
	ui.textBrowser->addAction(ui.action_Put_sticker);
	connect(ui.action_Put_sticker, &QAction::triggered, ui.textBrowser, &TextBrowserImage::on_Put_Sticker);
	setStyle(QStyleFactory::create("fusion"));
}

Resume::~Resume()
{

}

void Resume::closeEvent(QCloseEvent *e)
{
	if (maybeSave())
		e->accept();
	else
		e->ignore();
}

void Resume::dataOnEdition(const QByteArray* data)
{
	ui.textBrowser->clear();
	auto doc = ui.textBrowser->document();
	doc->clear();
	TextBrowserImage::arrayToDoc(data, doc);
	ui.textBrowser->setDocument(doc);
}

QByteArray Resume::EditedToData()
{
	return TextBrowserImage::docToArray(ui.textBrowser->document());
}


void Resume::makeTest()
{
	add_editable_image(tr("Click here to add or replace image"));
	QStringList lstText;
	lstText << tr("\tText to report adds by blocks with structure definite. It may contain block for youser picture. Block with header and paragraphs with 3 leve heirachy. Block of substation description (chromatography peaks pictures). Blocks can be repeared.");
	lstText << tr("\tUser had ability to export report to PDF ODT or our own format RSM. Ability print report are...");
	lstText << tr("\tNavigation by press links work fine...");
	lstText << tr("\tCustomer may add own sticker this comments by pree right button of mouse in any document position");
	addSection(lstText, 0, tr("1. About Report"), tr("1. About Report (Find by press)"));
	lstText.clear();
	lstText << tr("\This block structure not final. Borders, Colors, Fonts, colomns and row count are discussed. Its only demonstrate picrtrues and offers one option");
	addSection(lstText, 1, tr("About Chroma peaks pictures"), tr("2. About Chroma peaks pictures (Find by press)"));
	lstText.clear();
	lstText << tr("\Remarks adding with pres right mouse button. They are minimizing them losts focus. They are moving then holded with left button. They are imporeted after paragraph placed under.");
	addSection(lstText, 2, tr("About Stickers"), tr("3. About Stickers (Find by press)"));
	
	lstText.clear();
	lstText << tr("\Text in blocks has format. Tree predefined foramts are there. Needing and properties are discussed.");
	lstText << tr("This block for example copiing format of first block.");
	addSection(lstText, 0, tr("Postscriptum"), tr("5. Postscriptum"));

	lstText.clear();
	addSection(lstText, 2, tr("Table example"), tr("4. Link to table example"));

//#ifdef Q_OS_MAC
//	QImage image1("../Resources/Demo1.png");
//	QImage image2("../Resources/Demo2.png");
//	QImage image3("../Resources/Demo3.png");
//#else
//	QImage image1("Demo1.png");
//	QImage image2("Demo2.png");
//	QImage image3("Demo3.png");
//#endif

	QStringList cells;
	cells << tr("it is text too");
	cells << tr("Hor header 1");
	cells << tr("Hor header 2");
	cells << tr("Row header 1");
	cells << tr("Cell value 1");
	cells << tr("Cell value 2");
	cells << tr("Row header 2");
	cells << tr("Cell value 3");
	cells << tr("Cell value 4");
	addTable(3, 3, cells);

	

	lstText.clear();
	addSection(lstText, 2, tr("Substation descripture"), tr("5. Link to substaton descripture"));
	cells.clear();



#ifdef Q_OS_MAC
	QImage im1("../Resources/DemoPeak1.png");
	QImage im2("../Resources//DemoPeak2.png");
	QImage im3("../Resources/DemoPeak3.png");
	QImage im4("../Resources/DemoPeak4.png");
	QImage im5("../Resources/DemoPeak5.png");
	QImage im6("../Resources/DemoPeak6.png");
	QImage im7("../Resources/DemoPeak7.png");
	QImage im8("../Resources/DemoPeak8.png");
	QImage im9("../Resources/DemoPeak9.png");
#else
	QImage im1("../protanalytic/pictures/DemoPeak1.png");
	QImage im2("../protanalytic/pictures/DemoPeak2.png");
	QImage im3("../protanalytic/pictures/DemoPeak3.png");
	QImage im4("../protanalytic/pictures/DemoPeak4.png");
	QImage im5("../protanalytic/pictures/DemoPeak5.png");
	QImage im6("../protanalytic/pictures/DemoPeak6.png");
	QImage im7("../protanalytic/pictures/DemoPeak7.png");
	QImage im8("../protanalytic/pictures/DemoPeak8.png");
	QImage im9("../protanalytic/pictures/DemoPeak9.png");
#endif
	

	Images images;
	images << im1 << im2 << im3;
	addSubstation(tr("Substation A")
		, images, cells);
	
	images.clear();
	images << im4 << im5 << im6;
	addSubstation(tr("Substation B")
		, images, cells);

	images.clear();
	images << im7 << im8 << im9;
	addSubstation(tr("Substation C")
		, images, cells);

}


void Resume::addSection(const QStringList& paragraphs, int heirarchy, const QString& title, const QString& ref)
{
	auto doc = ui.textBrowser->document();
	doc->setUndoRedoEnabled(false);

	auto cursor = ui.textBrowser->textCursor();
	//	cursor.insertBlock();
	// Text formats
	QTextCharFormat format_header;
	QTextCharFormat format_plain;

	QTextBlockFormat format_plain_block;
	QTextBlockFormat format_header_block;
	//	format_header_block.setPageBreakPolicy(QTextFormat::PageBreak_AlwaysBefore);
	//	format_plain_block.setPageBreakPolicy(QTextFormat::PageBreak_AlwaysAfter);
	QFont font_plain = format_plain.font();
	QFont font_header = format_header.font();

	font_plain.setPointSize(12);
	switch (heirarchy)	{
	case 1:	{
		font_header.setPointSize(14);
		font_header.setBold(true);
		break;
	}
	case 2:{
		font_header.setPointSize(12);
		font_header.setItalic(true);
		break;
	}
	default:{
		format_header_block.setAlignment(Qt::AlignCenter);
		font_header.setPointSize(16);
		font_header.setBold(true);
		break;
	}
	}
	format_header.setFont(font_header);
	format_plain.setFont(font_plain);


	format_header_block.setTopMargin(10);

	format_plain_block.setTopMargin(10);
	format_plain_block.setBottomMargin(10);

	if (!title.isEmpty()){
		cursor.insertBlock(format_header_block, format_header);
		if (ref.isEmpty())
			cursor.block().setUserState(0);
		else
		{// Add reference
			int val_block = static_cast<int>(Mark::ref_first_index) + (ref_cout << 4);
			cursor.block().setUserState(val_block);
			QTextCursor cursor_1(ui.textBrowser->document());
			cursor_1.movePosition(QTextCursor::Start);
			for (int i = 0; i < ref_cout; ++i)
			{
				cursor_1.movePosition(QTextCursor::NextBlock);
				cursor_1.movePosition(QTextCursor::NextBlock);
			}
			cursor_1.insertBlock();
			cursor_1.insertHtml("< a href = \"" + ref + "\">" + ref + "</a>");
			auto blockLink(cursor_1.block());
			int val_link = static_cast<int>(Mark::link_ref_text) + static_cast<int>(Mark::ref_first_index) + (ref_cout << 4);
			blockLink.setUserState(val_link);
			cursor_1.insertBlock();
			
			
			++ref_cout;
		}
		cursor.insertText(title);
	}

	for (auto parag : paragraphs){
		cursor.insertBlock(format_plain_block, format_plain);
		cursor.block().setUserState(0);
		cursor.insertText(parag);
		format_plain_block.setPageBreakPolicy(QTextFormat::PageBreak_Auto);
	}

	cursor.insertBlock();
	doc->setUndoRedoEnabled(true);
}

void Resume::addImage(const QImage& image, const QString& caption)
{
	auto doc = ui.textBrowser->document();
	doc->setUndoRedoEnabled(false);

	QString url = TextBrowserImage::makeTempFile(image);
	ui.textBrowser->document()->addResource(QTextDocument::ImageResource,
		url, QVariant(image));

	auto cursor = ui.textBrowser->textCursor();

	cursor.insertBlock();
	cursor.block().setUserState(0);
	//	The image can be inserted into the document using the QTextCursor API :

	QTextImageFormat imageFormat;
	imageFormat.setWidth(200);
	imageFormat.setHeight(200);
	imageFormat.setName(url);
	cursor.insertImage(imageFormat);

	if (!caption.isEmpty())
	{
		QTextBlockFormat format_caption_block;
		format_caption_block.setTopMargin(2);
		format_caption_block.setBottomMargin(2);

		QTextCharFormat format_caption;
		QFont font_caption = format_caption.font();
		font_caption.setPointSize(12);
		font_caption.setItalic(true);

		cursor.insertBlock(format_caption_block, format_caption);
		cursor.block().setUserState(0);
		cursor.insertText(caption);
	}
	cursor.insertBlock();
	doc->setUndoRedoEnabled(true);
}

void Resume::addTable(int r_count, int c_count, const QStringList& cell_texts, int modes)
{
	auto doc = ui.textBrowser->document();
	doc->setUndoRedoEnabled(false);

	auto cursor = ui.textBrowser->textCursor();
	cursor.insertBlock();

	QTextTableFormat table_format;
	QTextTable * table = cursor.insertTable(r_count, c_count, table_format);
	QTextTableCellFormat format_cell;

	QTextCharFormat format_char_hor_header;
	QTextCharFormat format_char_vert_header;
	QTextCharFormat format_char_value;
	QFont font = format_char_value.font();
	font.setPointSize(1.3* font.pointSize());
	switch (modes){
	case 1:{// highligh only hor header
		font.setBold(true);
		format_char_hor_header.setFont(font);
		break;
	}
	case 2:{// highligh only hor vert header
		font.setItalic(true);
		format_char_vert_header.setFont(font);
		break;
	}
	default:{
		font.setBold(true);
		format_char_hor_header.setFont(font);
		font.setBold(false);
		font.setItalic(true);
		format_char_vert_header.setFont(font);
	}
	}


	int index = 0;
	for (auto cell_text : cell_texts)
	{
		if (index < r_count){
			cursor.insertText(cell_text, format_char_hor_header);
		}
		else if (index % r_count){
			cursor.insertText(cell_text, format_char_value);
		}
		else{
			cursor.insertText(cell_text, format_char_vert_header);
		}
		QTextBlock block = cursor.block();
		block.setUserState(0);
		cursor.movePosition(QTextCursor::NextCell);
		++index;
	}

	//for (int c = 0; c < c_count; ++c){
	//		format_cell.setPadding(3);
	//		table->cellAt(r, c).setFormat(format_cell);
	//		cursor.movePosition(QTextCursor::NextCell);
	//	}

	cursor.insertBlock();
	doc->setUndoRedoEnabled(true);
}

void Resume::addSubstation(const QString& text, const Images& imgs, const QStringList & captions)
{
	auto doc = ui.textBrowser->document();
	doc->setUndoRedoEnabled(false);

	auto cursor = ui.textBrowser->textCursor();
	cursor.insertBlock();

	QTextTableFormat table_format;
	table_format.setHeaderRowCount(1);
	//  table_format.setWidth(100);
	//   table_format.setAlignment(Qt::AlignHCenter);
	table_format.setBorder(2);
	table_format.setBorderStyle(QTextFrameFormat::BorderStyle_Solid);
	table_format.setPadding(0);
	table_format.setCellPadding(1);
	table_format.setCellSpacing(0);
	QVector<QTextLength> lengths;
	int int_width_for_image = ui.textBrowser->lineWrapColumnOrWidth();
	//    lengths.append(QTextLength( QTextLength::FixedLength, int_width_for_image/4));
	//    lengths.append(QTextLength( QTextLength::FixedLength, int_width_for_image/2));
	//    lengths.append(QTextLength( QTextLength::FixedLength, int_width_for_image/2));
	//    lengths.append(QTextLength( QTextLength::FixedLength, int_width_for_image/2));
	lengths.append(QTextLength(QTextLength::PercentageLength, 15));
	lengths.append(QTextLength(QTextLength::PercentageLength, 27));
	lengths.append(QTextLength(QTextLength::PercentageLength, 27));
	lengths.append(QTextLength(QTextLength::PercentageLength, 27));
	table_format.setColumnWidthConstraints(lengths);
	QTextTableCellFormat format_cell;
	QTextCharFormat format_comments;
	QTextBlockFormat format_block_comments;
	format_block_comments.setTopMargin(3);
	format_block_comments.setBottomMargin(3);
	QTextCharFormat format_text;

	QTextImageFormat imageFormat;

	imageFormat.setWidth(int_width_for_image*0.20);
	imageFormat.setHeight(int_width_for_image*0.20);

	cursor.insertTable(1, 4, table_format);
	cursor.insertText(text, format_text);
	QTextBlock block = cursor.block();
	block.setUserState(0);

	int index = 0;
	for (auto image : imgs)
	{

		cursor.movePosition(QTextCursor::NextCell);
		QImage img = image.scaledToWidth(int_width_for_image*0.20, Qt::SmoothTransformation);
		QString url = TextBrowserImage::makeTempFile(img);
		ui.textBrowser->document()->addResource(QTextDocument::ImageResource,
			url, QVariant(img));
		imageFormat.setName(url);
		cursor.insertImage(imageFormat);
		block = cursor.block();
		block.setUserState(0);

		if (captions.count() > index){
			cursor.insertBlock(format_block_comments, format_comments);
			cursor.insertText(captions[index]);
			block = cursor.block();
			block.setUserState(0);
		}
		++index;
		//		cursor.movePosition(QTextCursor::NextCell);
	}
	doc->setUndoRedoEnabled(true);
}

void Resume::clean()
{
	ref_cout = 0;
	auto doc = ui.textBrowser->document();
	doc->clear();
}

void Resume::add_editable_clause(const QString& link)
{
	auto cursor = ui.textBrowser->textCursor();
	cursor.insertBlock();
 
	//cursor.insertHtml("< a href = \"" + link + "\">" + link + "</a>");
	cursor.insertHtml("< a href = \"" + tr("������ ����� �� ����� ����� ��� \n �� ���� ���� �� ��� ����� � ����") + "\">" + link + "</a>");

	auto blockLink(cursor.block());
	blockLink.setUserState(static_cast<int>(Mark::link_text));
}

void Resume::add_editable_image(const QString& link)
{
	auto cursor = ui.textBrowser->textCursor();
	cursor.insertBlock();

	cursor.insertHtml("< a href = \"" + link + "\">" + link + "</a>");
	auto blockLink(cursor.block());
	blockLink.setUserState(static_cast<int>(Mark::link_image));
}

void Resume::filePrintPreview()
{
	QPrinter printer(QPrinter::HighResolution);
	QPrintPreviewDialog preview(&printer, this);
	QObject::connect(&preview, &QPrintPreviewDialog::paintRequested, [this](QPrinter* p) {
	//	ui.textBrowser->print(p);
		ui.textBrowser->document_with_stickers()->print(p);
	});
	preview.exec();
}

void Resume::filePrintPdf()
{
	QString fileName = QFileDialog::getSaveFileName(this, "Export PDF",
		QString(), "*.pdf");
	if (!fileName.isEmpty()) {
		if (QFileInfo(fileName).suffix().isEmpty())
			fileName.append(".pdf");
		QPrinter printer(QPrinter::HighResolution);
		printer.setOutputFormat(QPrinter::PdfFormat);
		printer.setOutputFileName(fileName);
		ui.textBrowser->document_with_stickers()->print(&printer);
//		ui.textBrowser->document()->print(&printer);
	}
}

void Resume::filePrint()
{
	QPrinter printer(QPrinter::HighResolution);
	QPrintDialog *dlg = new QPrintDialog(&printer, this);
	if (ui.textBrowser->textCursor().hasSelection())
		dlg->addEnabledOption(QAbstractPrintDialog::PrintSelection);
	dlg->setWindowTitle(tr("Print Document"));
	if (dlg->exec() == QDialog::Accepted)
	//	ui.textBrowser->print(&printer);
	ui.textBrowser->document_with_stickers()->print(&printer);
	delete dlg;
}

void Resume::fileOpen()
{
	QString fn = QFileDialog::getOpenFileName(this, tr("Open File..."),
		QString(), tr("Resume-Files (*.rsm);;"
		"HTML-Files (*.htm *.html);;All Files (*)"));
	if (!fn.isEmpty())
		load(fn);
}


bool Resume::fileSave()
{
	if (fileName.isEmpty())
		return fileSaveAs();
	if (fileName.startsWith(QStringLiteral(":/")))
		return fileSaveAs();

	QString str = fileName;
	if (fileName.endsWith(".rsm"))//, Qt::CaseInsensitive)
	{
		return fileSaveResume();
	}
	QTextDocumentWriter writer(fileName);
	bool success = writer.write(ui.textBrowser->document());
	if (success)
		ui.textBrowser->document()->setModified(false);
	return success;
}

bool Resume::fileSaveResume()
{
	QFile file(fileName);
	if (file.open(QIODevice::WriteOnly)){
	//	QByteArray data = TextBrowserImage::docToArray(ui.textBrowser->document());
		QByteArray data = ui.textBrowser->toArray();
		file.write(data);
		file.close();
		return true;
	}
	return false;
}

bool Resume::fileSaveAs()
{
	QString fn = QFileDialog::getSaveFileName(this, tr("Save as..."), QString(),
		tr("Resume-Files (*.rsm);;"
		"ODF files (*.odt);;"
		"HTML-Files (*.htm *.html);;All Files (*)"));
	if (fn.isEmpty())
		return false;
	if (!(fn.endsWith(".odt", Qt::CaseInsensitive)
		|| fn.endsWith(".htm", Qt::CaseInsensitive)
		|| fn.endsWith(".html", Qt::CaseInsensitive)
		|| fn.endsWith("", Qt::CaseInsensitive))) {
		fn += ".rsm"; // default
	}
	setCurrentFileName(fn);
	return fileSave();
}
void Resume::fileNew()
{
	if (maybeSave()) {
		ui.textBrowser->clear();
		setCurrentFileName(QString());
	}
}
void Resume::setCurrentFileName(const QString &fileName)
{
	this->fileName = fileName.trimmed();
	ui.textBrowser->document()->setModified(false);

	QString shownName;
	if (fileName.isEmpty())
		shownName = "untitled.rsm";
	else
		shownName = QFileInfo(fileName).fileName();

	setWindowTitle(tr("%1[*] - %2").arg(shownName).arg(tr("Resume Edit")));
	setWindowModified(false);
}

bool Resume::load(const QString &f)
{
	if (!QFile::exists(f))
		return false;
	QFile file(f);
	if (!file.open(QFile::ReadOnly))
		return false;

	QByteArray data = file.readAll();
	if (f.endsWith(tr(".rsm"), Qt::CaseInsensitive)){
	//	auto doc = ui.textBrowser->document();
	//	TextBrowserImage::arrayToDoc(&data, doc);
	//	ui.textBrowser->setDocument(doc);
		ui.textBrowser->fromArray(&data);
	}
	else{
		QTextCodec *codec = Qt::codecForHtml(data);
		QString str = codec->toUnicode(data);
		if (Qt::mightBeRichText(str)) {
			ui.textBrowser->setHtml(str);
		}
		else {
			str = QString::fromLocal8Bit(data);
			ui.textBrowser->setPlainText(str);
		}
	}
	setCurrentFileName(f);
	return true;
}

bool Resume::maybeSave()
{
	if (!ui.textBrowser->document()->isModified())
		return true;

	QMessageBox::StandardButton ret;

	ret = QMessageBox::warning(this, tr("Application"),
		tr("The document has been modified.\n"
		"Do you want to save your changes?"),
		QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);

	if (ret == QMessageBox::Save)
		return fileSave();
	else if (ret == QMessageBox::Cancel)
		return false;
	return true;
}





