﻿#ifndef RESUME_H
#define RESUME_H

#include <QDialog>
#include "ui_Resume.h"
typedef QList<QImage> Images;
#include <QTextCursor>

class Resume : public QDialog
{
	Q_OBJECT

public:
	Resume(QWidget *parent = 0);
	~Resume();
	void closeEvent(QCloseEvent *e);
	void dataOnEdition(const QByteArray* data);
	QByteArray EditedToData();
	void clean();
	void makeTest();
	void addSubstation(const QString& text, const Images& imgs, const QStringList & captions);
	void addImage(const QImage& image, const QString& caption);
	void addTable(int r_count, int c_count, const QStringList& cell_texts, int modes = 3);
	void addSection(const QStringList& paragraphs, int heirarchy, const QString& title,  const QString& ref = QString());
	void add_editable_clause(const QString& link);
	void add_editable_image(const QString& link);
	bool fileSaveResume();
	void setCurrentFileName(const QString &fileName);
	bool load(const QString &f);
	void fileNew();
	bool maybeSave();

private:
	Ui::Resume ui;
	QTextCursor cursor_links;
	QString fileName;
	int ref_cout;
	public slots:
	void filePrintPreview();
	void filePrintPdf();
	void filePrint();
	void fileOpen();
	bool fileSave();
	bool fileSaveAs();
};

#endif // RESUME_H
