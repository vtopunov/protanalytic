#include "protanalytic.h"
#include <qapplication>
#include <QtWidgets/QApplication>


void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	QByteArray localMsg = msg.toLocal8Bit();

	fprintf(stdout, "%s %s\n", (const char*)QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss").toLocal8Bit(), localMsg.constData());
}

int main(int argc, char *argv[])
{
    //qInstallMessageHandler(myMessageOutput);
	
	QApplication a(argc, argv);
	QDir::setCurrent(QCoreApplication::applicationDirPath());

    qDebug() << "working directory " << QDir::currentPath();

	QTranslator ts;

	if (ts.load("ru")) {
		a.installTranslator(&ts);
	}

	protanalytic w;  
	w.show();
	return a.exec();
}
