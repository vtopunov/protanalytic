#ifndef TEXTEDITIMAGE_H
#define TEXTEDITIMAGE_H

#include <QTextEdit>
class QMimeData;
class QKeyEvent;
class QTextBlockFormat;
class QTextCharFormat;
class TextEditImage : public QTextEdit
{
public:
	TextEditImage(QWidget *parent = 0);
	bool canInsertFromMimeData(const QMimeData* source) const;
	void insertFromMimeData(const QMimeData* source);
	static QByteArray docToArray(const QTextDocument*);
	static void arrayToDoc(const QByteArray* data, QTextDocument* doc_new);// � QTextDocument ��� ������������ �����������
	static QString makeTempFile(const QImage& img);
	void keyPressEvent(QKeyEvent *event);//// 'del' & 'backspace' not erase not editable blocks
private:
	QString str_temp_dir;
	int image_count;
	void dropImage(const QUrl& url, const QImage& image);
	void dropTextFile(const QUrl& url);
	private slots:
	void on_cursorPositionChanged();
	inline bool isPositionReadOnly() const;
	
};

#endif // TEXTEDITIMAGE_H
