#include "protanalytic.h"
#include <functional>
#include <vector>
#include <converter.h>
#include "NistDialog.h"
#include "Resume.h"

inline QString _tr(const QString& text) {
	return QApplication::translate("code", text.toLocal8Bit());
}

const QString& pleaseWait() {
	static const QString msg = _tr("Please, wait..."); return msg;
}

int childCount(const QTreeWidgetItem* parent) { return parent->childCount(); }
int childCount(const QTreeWidget* parent) { return parent->topLevelItemCount(); }
QTreeWidgetItem* child(const QTreeWidgetItem* parent, int index) { return parent->child(index); }
QTreeWidgetItem* child(const QTreeWidget* parent, int index) { return parent->topLevelItem(index); }

template<class T>
std::vector<QTreeWidgetItem*> childs(T* parent) {
	auto count = (parent) ? childCount(parent) : 0;
	std::vector<QTreeWidgetItem*> result;
	if (count > 0) {
		result.reserve(count);
		for (int i = 0; i < count; ++i) {
			result.push_back(child(parent, i));
		}
	}
	return result;
}

template<class T>
QTreeWidgetItem* atOrAddTreeItem(T* parent, int index, const QString& text, const QVariant& data = QVariant()) {
	QTreeWidgetItem* childItem = nullptr;

	auto childCount = ::childCount(parent);

	if (childCount >= 0) {
		if (index >= childCount) {
			childItem = new QTreeWidgetItem(parent);
		}
		else {
			childItem = child(parent, index);
		}
	}

	if (childItem) {
		childItem->setText(0, text);

		if (data.isValid()) {
			childItem->setData(0, Qt::UserRole, data);
		}
	}
	return childItem;
}

template<class T>
T to(const QTreeWidgetItem* item, const T& defVal = T()) {
	if (item) {
		auto data = item->data(0, Qt::UserRole);
		if (data.type() == static_cast<QVariant::Type>( qMetaTypeId<T>()) ) {
			return data.value<T>();
		}
	}
	return defVal;
}

QPoint toPoint(const QSize& sz) {
	return QPoint(sz.width(), sz.height());
}

protanalytic::protanalytic(QWidget *parent)
: QWidget(parent),
	ui({}),
	settings("protoanalytic.protoanalytic.ini", QSettings::Format::IniFormat),
	viewTree_({}),
	x({}), y({}),
	data_(),
	textedit_resume_(),
	resume_()
{
#ifdef Q_OS_MAC
	this->setStyleSheet("* { font: 12pt \"Arial\"; }");
#else
	this->setStyleSheet("* { font: 8pt \"Arial\"; }");
#endif

	ui.setupUi(this);
	ui.retranslateUi(this);
	ui.msg->hide();
	
	QObject::connect(ui.dbtbl, &QTableWidget::currentCellChanged,
		[this](int row, int column, int oldrow, int oldcolumn) {
			if (row >= 0) {
				auto check = qobject_cast<QCheckBox*>(this->ui.dbtbl->cellWidget(row, 0));
				if (check) {
					check->click();
				}
			}
		}
	);

	auto NIST_dialog = new NistDialog(this);
	QObject::connect(ui.nist, &QPushButton::pressed, [NIST_dialog]() {
		NIST_dialog->show();
		NIST_dialog->raise();
		NIST_dialog->activateWindow();
	});

	QObject::connect(ui.open, &QPushButton::pressed, [this]() {
		bool isLoad = false;
		for (auto& fileName : QFileDialog::getOpenFileNames(this, QString(), QDir::currentPath(), "MS Files (*.MS)")) {
			if (loadFile(fileName)) {
				isLoad = true;
			}
		}

		if (isLoad) {
			this->reload();
		}
	});


	QObject::connect(ui.print, &QPushButton::pressed, [this]() {
		Resume resume(this);
		resume.move(this->geometry().center() - toPoint(resume.size()) / 2);
		resume.makeTest();
		resume.exec();
	});

	

	// for test 
	GraphicLine ln;
	int n = 100;
	x.reserve(n);
	y.reserve(n);
	for (int i = 0; i < n; ++i) {
		x.push_back(i);
		y.push_back(sin((7 * M_PI * i) / n));
	}
	ln.linepen.setStyle(Qt::PenStyle::NoPen);
	ln.x = x.data();
	ln.y = y.data();
	ln.n = n;
	ui.graphic->addline(ln);
}

QTreeWidgetItem* root(QTreeWidget* widget) {
	return atOrAddTreeItem(widget, 0, _tr("Experiments"));
}

void protanalytic::reload() {
	ui.database->clear();
	ui.dbtbl->clear();
	viewTree_.clear();
	databaseItemExpanded(::root(ui.database));
}

protanalytic::~protanalytic()
{
}

void protanalytic::showEvent(QShowEvent*) {
	setWindowIcon(QIcon(":/protanalytic.png"));
	reload();
}

std::vector<QTreeWidgetItem*> getViewTree(QTreeWidgetItem *item) {
	std::vector<QTreeWidgetItem*> tree;
	for (; item;) {
		auto next = item->parent();
		tree.emplace_back(item);
		item = next;
	}
	tree.shrink_to_fit();
	std::reverse(std::begin(tree), std::end(tree));
	return tree;
}


class ui_locker {
public:
	ui_locker(QWidget* widget) : widget_(widget){ widget->setEnabled(false); }
	~ui_locker() { widget_->setEnabled(true); }

private:
	QWidget* widget_;
};


void drawTable(QTableWidget* tbl, QTreeWidgetItem *from, protanalytic* this_) {
    qDebug() << "drawTable " << "item = " << (std::ptrdiff_t)from
             << " name =" <<  ( (from) ? from->text(0) : QString("") );

    auto childs = ::childs(from);
	auto size = childs.size();
	tbl->clear();

	if (size <= 0) {
		tbl->setColumnCount(0);
		tbl->setRowCount(0);
		tbl->hide();
	} else {
#ifdef Q_OS_MAC
        enum{ sectionSize = 20 };
#else
		enum{ sectionSize = 16 };
#endif
		auto vh = tbl->verticalHeader();
		vh->sectionResizeMode(QHeaderView::Fixed);
		vh->setDefaultSectionSize(sectionSize);

		tbl->setColumnCount(3);
		tbl->setRowCount(size);
		int i_row = 0;
		for (auto child : childs) {
            std::unique_ptr<QCheckBox> check { new QCheckBox() };
			check->setLayoutDirection(Qt::LayoutDirection::RightToLeft);
			QObject::connect(check.get(), &QCheckBox::clicked,
				[tbl, i_row, child, this_]() {
					const int nRow = tbl->rowCount();
					for (int i = 0; i < nRow; ++i) {
						const auto check = qobject_cast<QCheckBox*>(tbl->cellWidget(i, 0));
						if (check) {
							check->setChecked(i == i_row);
						}
					}

					tbl->selectRow(i_row);
					this_->databaseItemExpanded(child);
				}
			);
			tbl->setCellWidget(i_row, 0, check.release());
			tbl->setItem(i_row, 1, new QTableWidgetItem(child->data(0, Qt::UserRole).toString()));
			tbl->setItem(i_row, 2, new QTableWidgetItem(child->text(0)));
			++i_row;
		}
        tbl->resizeColumnsToContents();

#ifdef Q_OS_MAC
        enum { checkBoxColumnSize = sectionSize+3 };
#else
        enum { checkBoxColumnSize = sectionSize };
#endif

        tbl->horizontalHeader()->resizeSection(0, checkBoxColumnSize);
		tbl->show();
	}
}

void spectrumToPoints(std::vector<double>& x, std::vector<double>& y, const Spectrum& data) {
	if (!data.IsInitialized()) {
		qDebug() << "error: no spectrum data";
		return;
	}

	auto& peaks = data.peak();
	auto size = peaks.size();
	x.clear(); x.reserve(3 * size);
	y.clear(); y.reserve(3 * size);

	for (auto& peak : peaks) {
		const double x_value = peak.mass();
		const double y_value = peak.intensity();
		x.push_back(x_value); y.push_back(0);
		x.push_back(x_value); y.push_back(y_value);
		x.push_back(x_value); y.push_back(0);
	}
}

void viewSpectrum(GraphicWidget* graphic, const std::vector<double>& x, const std::vector<double>& y) {
	GraphicLine ln;
	ln.x = x.data();
	ln.y = y.data();
	ln.n = static_cast<int>(
		std::min(std::min(x.size(), y.size()), static_cast<size_t>(INT_MAX))
	);
	graphic->x.maxngrid_ = 55;
	graphic->addline(ln);
	graphic->repaint();
}

void protanalytic::databaseItemExpanded(QTreeWidgetItem *item) {
    qDebug() << "databaseItemExpanded item = " << (std::ptrdiff_t)item << " name = " << item->text(0);

    for(int i = 0; i < viewTree_.size(); ++i ) {
        qDebug() << "viewTree[" << i << "]=" << (std::ptrdiff_t)viewTree_[i];
    }


    if(!item) {
        qWarning() << "null item";
        return;
    }

    for(auto view : viewTree_ ) {
        if( view == item ) {
            qWarning() << "already view";
            return;
        }
    }

	auto viewTree = ::getViewTree(item);
    if (viewTree.empty()) {
        qWarning() << "empty view";
		return;
	}

	auto value = [](const std::vector<QTreeWidgetItem*>& view, int index) {
        QTreeWidgetItem* tableItem_ = 0;
		if (index < view.size()) {
			tableItem_ = view[index];
		}
		return tableItem_;
	};

	auto newValue = [&viewTree, value, this](int index) {
		auto newView = value(viewTree, index);
		auto oldView = value(this->viewTree_, index);
		return (newView != oldView) ? newView : nullptr;
	};

	enum{ nLevel = 3 };

	QTreeWidgetItem* selectsItem[nLevel];
	for (int i = 0; i < nLevel; ++i ) {
		selectsItem[i] = newValue(i);
	}

	if (std::find(std::begin(selectsItem), std::end(selectsItem), item) == std::end(selectsItem)) {
        qWarning() << "already view 2";
		return;
	}

	viewTree_ = viewTree;

	const QString sqls[] = {
		"SELECT id, name FROM experiment",
		"SELECT id, name FROM spectrum WHERE experimentid=$1",
		"SELECT data FROM spectrum WHERE id = $1"
	};

	auto updateTree = [this](PgClient& client, QTreeWidgetItem* item) {
		uint32_t i_success_row = 0;
		for (auto& row : client) {
			auto id = row.column(0).toInt();
			auto name = row.column(1).toQString();
			if (id > 0 && !name.isEmpty()) {
				atOrAddTreeItem(item, i_success_row, name, id);
				++i_success_row;
			}
		}
	};

    auto viewExperimentList = [this, updateTree](PgClient& client, QTreeWidgetItem* item) {
        qDebug() << "viewExperimentList item = " << (std::ptrdiff_t)item << " name = " << item->text(0);

        drawTable(this->ui.dbtbl, 0, 0);
		updateTree(client, item);

		auto childs = ::childs(item);
		if (!childs.empty()) {
            QTreeWidgetItem* lastExperiment = childs.back();
            qDebug() << "viewExperimentList call itemExpanded for last child item = "
                     << (std::ptrdiff_t)lastExperiment
                     << " name = " << lastExperiment->text(0);

            this->databaseItemExpanded(lastExperiment);
        }
	};

    auto viewSpectrumsList = [this, updateTree](PgClient& client, QTreeWidgetItem* item) {
        qDebug() << "viewSpectrumList item = " << (std::ptrdiff_t)item
                 << " name = " << item->text(0);

        drawTable(this->ui.dbtbl, 0, 0);
		updateTree(client, item);
		drawTable(this->ui.dbtbl, item, this);
        auto childs = ::childs(item);
        for (auto child : childs) {
            child->setHidden(true);
		}
		this->ui.graphic->clear();

        if (!childs.empty()) {
             QTreeWidgetItem* firstExperiment = childs.front();
             qDebug() << "viewSpectrumList call itemExpanded for first child item = " << (std::ptrdiff_t)firstExperiment <<
                         " name = " << firstExperiment->text(0);
             this->databaseItemExpanded(firstExperiment);
        }

        ui.database->setItemSelected(item, true);
        ui.database->scrollToItem(item);
	};

    auto viewGraphic = [this](PgClient& client, QTreeWidgetItem* item) {
        qDebug() << "viewGraphic " << (std::ptrdiff_t)item << " name = " << item->text(0);
		this->ui.graphic->clear();
		auto data = client.row(0).column(0).toByteArray();
		if (data_.ParseFromArray(data.constData(), data.size())) {
			spectrumToPoints(x, y, data_);
			viewSpectrum(this->ui.graphic, x, y);
			auto check = qobject_cast<QCheckBox*>(this->ui.dbtbl->cellWidget(item->parent()->indexOfChild(item), 0));
			if (check) {
				check->click();
			}
		}
	};

	const std::function<void (PgClient&, QTreeWidgetItem*)> methods[] = {
        viewExperimentList,
        viewSpectrumsList,
        viewGraphic
	};

	auto client = make_pg_client();

	for (int i = 0; i < nLevel; ++i) {
		auto item = selectsItem[i];
		if (item) {
			Sql sql{ sqls[i] };
			auto parseSqlParamCount = sql.parseParamsCount();
			if (parseSqlParamCount > 1) {
				qDebug() << "error: invalid sql params count";
				return;
			}

			if (parseSqlParamCount > 0) {
                auto id = to<int32_t>(item);
				if (id <= 0) {
					qDebug() << "error: invalid id QTreeWidgetItem ";
					return;
				}

				sql.arg(id);
			}

            qDebug() << client.errorMessage();
			client.exec(sql);

			if (!client.isOk()) {
				qDebug() << client.errorMessage();
				return;
			}
			
			methods[i](client, item);
		}
	}
}

void protanalytic::databaseItemClicked(QTreeWidgetItem *item, int column) {
	databaseItemExpanded(item);
}



void protanalytic::dbContextMenu(QTreeWidgetItem *item) {
	QMenu menu;

	QAction actions[] = {
		{ _tr("Refresh"), nullptr },
		{ _tr("Export"), nullptr },
		{ _tr("Export to XML"), nullptr },
		{ _tr("Resume"), nullptr }
	};

	for (auto& action : actions) {
		menu.addAction(&action);
	}

	auto refresh = [this](QTreeWidgetItem *item) {
		this->databaseItemClicked(item, 0);
	};

    auto exportToBinary = [this](QTreeWidgetItem *item) { 
		QFile file(QFileDialog::getSaveFileName(this, QString(), QString(), "Binary format (*.imass)"));
		if (file.open(QFile::WriteOnly)) {
			auto tree = getViewTree(item);
			if (tree.size() > 1) {
				auto id = to<int32_t>(tree[1]);
				if (id <= 0) {
					qWarning() << "invalid experiment id";
					return;
				}

				file.write( make_pg_client().exec(Sql("SELECT data FROM experiment WHERE id=$1").arg(id)).row(0).column(0).toByteArray() );
			}
		}
	};

	auto exportToXml = [this](QTreeWidgetItem *item) {
		QFile file(QFileDialog::getSaveFileName(this, QString(), QString(), "XML Document (*.xml)"));
		if (file.open(QFile::WriteOnly)) {
			QByteArray xml("<?xml version='1.0'?>\n");
			xml += ("<database><experiment name='" + item->text(0) + "'>\n");
			for (int i = 0; i < data_.peak_size(); ++i) {
				auto& peak = data_.peak(i);
				xml += ("<spectrum x='" + QByteArray::number(peak.mass()) + "' y='" + QByteArray::number(peak.intensity()) + "'/>\n");
			}
			xml += "</experiment></database>";
			file.write(xml);
		}
	};

	auto onResume = [this](QTreeWidgetItem *item) {
		ui.print->click();
	};

	std::function<void(QTreeWidgetItem*)> functors[] = {
		refresh,
        exportToBinary,
		exportToXml,
		onResume
	};

	auto action = menu.exec(QCursor::pos());

	if (action >= actions && action < std::end(actions)) {
		functors[action - actions](item);
	}
}

void protanalytic::dbContextMenu(const QPoint& pos) {
	dbContextMenu(ui.database->itemAt(pos));
}

void protanalytic::dbtblContextMenu(const QPoint& pos) {
	if (viewTree_.size() >= 2) {
		dbContextMenu(viewTree_[1]);
	}
}