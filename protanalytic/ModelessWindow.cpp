#include "ModelessWindow.h"
#include <QGridLayout>
#include <QLineEdit>
#include <QSpinBox>
#include <QPushButton>
#include <QComboBox>
#include <QHeaderView>
#include <QMessageBox>
#include "TableWithKeys.h"
ModelessWindow::ModelessWindow(QWidget *parent)
: QDialog(parent)
{
	setWindowIcon(QIcon(":/protanalytic/ExpNodeDB.png"));
	QGridLayout * layout_main = new QGridLayout(this);
	// �������� ����
	lineedit_number_ = new QLineEdit(this);
//	lineedit_number_->setInputMask("000000000");
	layout_main->addWidget(lineedit_number_, 0, 0);
	// �������� ���� � ��������.
	spinbox_spinbox_ = new QSpinBox(this);
	spinbox_spinbox_->setRange(100, 10000);
	spinbox_spinbox_->setSingleStep(5);
	layout_main->addWidget(spinbox_spinbox_, 0, 1);
	// ��������� ����
	lineedit_text_ = new QLineEdit(this);
	layout_main->addWidget(lineedit_text_, 1, 0);
	// 
	combobox_combobox_ = new QComboBox(this);
	for (int i = 0x3B1; i < 0x3CA; ++i)
	{
		combobox_combobox_->addItem(QChar(i));
	}
	layout_main->addWidget(combobox_combobox_, 1, 1);
	// ������
	pushbutton_pushbutton_ = new QPushButton(this);
	layout_main->addWidget(pushbutton_pushbutton_, 1, 2);

	tablekeys_properties_ = new TableWithKeys(this);
	layout_main->addWidget(tablekeys_properties_, 2, 1, 1, 2);
	tablekeys_properties_->setRowCount(5);
	tablekeys_properties_->setColumnCount(1);
	
	QHeaderView  * header = tablekeys_properties_->horizontalHeader();
	header->setSectionResizeMode(0, QHeaderView::Stretch);
	header->hide();
	for (int i = 0; i < 5; ++i)
	{
		QTableWidgetItem *newItem = new QTableWidgetItem();
		newItem->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsTristate);
		newItem->setCheckState(Qt::Unchecked);
		newItem->setText(tr("Propertiy #") +QString::number(i+1));
		tablekeys_properties_->setItem(i, 0, newItem);
	}

	tablewidget_NIST_ = new QTableWidget(this);
	layout_main->addWidget(tablewidget_NIST_, 2, 0);
	tablewidget_NIST_->setRowCount(2);
	tablewidget_NIST_->setColumnCount(3);
	for (int i = 0; i < 2; ++i)
	{
		QTableWidgetItem *newItem = new QTableWidgetItem();
		newItem->setFlags( Qt::ItemIsEnabled | Qt::ItemIsSelectable );
		QString strSubst(QChar(0x3B1 + i) + tr("-Substation"));
		newItem->setText(strSubst);
		tablewidget_NIST_->setItem(i, 0, newItem);

		newItem = new QTableWidgetItem();
		newItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		newItem->setText(strSubst + tr(" description"));
		tablewidget_NIST_->setItem(i, 1, newItem);

		newItem = new QTableWidgetItem();
		newItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		newItem->setText(strSubst + tr(" Comment"));
		tablewidget_NIST_->setItem(i, 2, newItem);
	}


	setTabOrder(lineedit_number_, spinbox_spinbox_);
	spinbox_spinbox_->setTabOrder(spinbox_spinbox_, lineedit_text_);
	setTabOrder(lineedit_text_, combobox_combobox_);
	setTabOrder(combobox_combobox_, pushbutton_pushbutton_);
	setTabOrder(pushbutton_pushbutton_, tablewidget_NIST_);
	setTabOrder(tablewidget_NIST_, tablekeys_properties_);

	connect(pushbutton_pushbutton_, &QPushButton::clicked, this, &ModelessWindow::on_pushbutton);
	connect(lineedit_number_, &QLineEdit::editingFinished, this, &ModelessWindow::on_NumberEditingFinished);
	translateUI();
}

ModelessWindow::~ModelessWindow()
{

}

void ModelessWindow::translateUI()
{
	setWindowTitle(tr("Modeless Window"));
	setWhatsThis(tr("Modeless window for tests"));

	lineedit_number_->setToolTip(tr("Input here number from 0 to 999999999"));
	lineedit_number_->setPlaceholderText(tr("Number Value"));
	lineedit_number_->setWhatsThis(tr("It is number field with range from 0 to 999 999 999"));

	spinbox_spinbox_->setToolTip(tr("Input here number from 0 to 999999999"));
	spinbox_spinbox_->setWhatsThis(tr("It is number field with button (range 100 - 10 000, step 5)"));

	lineedit_text_->setToolTip(tr("Input here text"));
	lineedit_text_->setPlaceholderText(tr("Some Text"));
	lineedit_text_->setWhatsThis(tr("It is text field"));

	combobox_combobox_->setToolTip(tr("Here you are. Combo box with greek alphabet"));
	combobox_combobox_->setWhatsThis(tr("It is dropping list of greek letters"));
	combobox_combobox_->setEditText(tr("Edit text"));

	pushbutton_pushbutton_->setText("Button");
	pushbutton_pushbutton_->setToolTip(tr("Button tool tip"));
	pushbutton_pushbutton_->setWhatsThis(tr("Button whatsThis"));
	
}

void ModelessWindow::on_show()
{
	show();
	raise();
	activateWindow();
}

void ModelessWindow::on_pushbutton()
{
	QString strText = lineedit_number_->text();
	if (strText.isEmpty())
		QMessageBox::warning(this, tr("No number"), tr("line edit number value must contain less one digit"));
}

void ModelessWindow::on_NumberEditingFinished()
{
	QString str_number = lineedit_number_->text();
	bool bResult = false;
	if (str_number.isEmpty())
		return;
	str_number.toInt(&bResult);
	if (!bResult)
	{
		QMessageBox::warning(this, tr("Number edit not a number"), "Please enter a valid value into line edit");
		lineedit_number_->setFocus();
	}
}