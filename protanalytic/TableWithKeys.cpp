#include "TableWithKeys.h"
#include <QKeyEvent>
#include <QHeaderView>
TableWithKeys::TableWithKeys(QWidget *parent)
	: QTableWidget(parent)
{
	setFocusPolicy(Qt::StrongFocus);// ����� ��������� ����� �� ������ ���� ��� �� ������� Tab.
//	setColumnCount (3);
	QHeaderView * hv = horizontalHeader   ();
	//hv->setSectionResizeMode( 0, QHeaderView::ResizeToContents);
	//hv->setSectionResizeMode( 1, QHeaderView::Stretch);
	//hv->setSectionResizeMode( 2, QHeaderView::ResizeToContents);
	connect (this, SIGNAL(cellClicked (int,int)), this, SLOT(on_cellClicked (int,int)));;
	bool C = connect (this, SIGNAL(cellDoubleClicked (int,int)), this, SLOT(on_cellDoubleClicked (int,int)));
//	horizontalHeader()->hide ();
	verticalHeader ()->setSectionsClickable(false);	
	verticalHeader()->setDefaultSectionSize ( verticalHeader()->minimumSectionSize () );
//	bool b = connect (hv, SIGNAL(sectionEntered(int)), this, SLOT(on_sectionEntered (int)));
//	hideColumn (3);// �������� ��� ��������.
	setSelectionBehavior (SelectionBehavior ::SelectRows);
	setSelectionMode (SelectionMode :: SingleSelection );
	m_iCurrentRow = -1;
//	selectionChanged ();
}

TableWithKeys::~TableWithKeys()
{

}

void TableWithKeys::rowSelectionChange ( int currentRow, int previousRow)
{
	/*
		QTableWidgetSelectionRange rangeOld(previousRow, 0,previousRow, 2);
		setRangeSelected (rangeOld, false);
		QTableWidgetSelectionRange rangeNew(currentRow, 0,currentRow, 2);
		setRangeSelected (rangeNew, true);
		*/
}

void TableWithKeys::keyPressEvent(QKeyEvent *event) 
{ 
	int iPreviousRow = m_iCurrentRow;
	int iRowCount = rowCount ();
	int iEnum = event->key();
	switch (event->key()) { 
	case Qt::Key_Down: 
		{
			if (m_iCurrentRow < iRowCount)
				++m_iCurrentRow ;
			selectRow (m_iCurrentRow);
		//	rowSelectionChange(m_iCurrentRow, iPreviousRow);
		}
		break; 
	case Qt::Key_Up: 
		{
		if (m_iCurrentRow < 0)
			m_iCurrentRow = iRowCount-1;
		if (m_iCurrentRow > 0)
			--m_iCurrentRow;
			selectRow (m_iCurrentRow);
		}
		break; 
	case Qt::Key_Enter :
	case Qt::Key_Return :
		if (m_iCurrentRow < iRowCount)
			on_cellDoubleClicked(m_iCurrentRow, 1);
	//	on_currentCellChanged(iRow, 0, 0, 0);
		break;
	case Qt::Key_Space :
		{
			QTableWidgetItem *itemCurr = item (m_iCurrentRow, 0);
			if (itemCurr)
			{
			//	QString str = itemCurr->text ();
			//	selectRow (m_iCurrentRow);
				Qt::CheckState state =  itemCurr ->checkState ();
				if (state)
					itemCurr->setCheckState (Qt::Unchecked );
				else
					itemCurr->setCheckState (Qt::Checked  );
				emit rowCheckState(m_iCurrentRow);
			}
		}
		break;
	default: 
		QTableWidget ::keyPressEvent(event); 
	} 
}

void TableWithKeys::on_cellClicked ( int row, int column ) 
{
	selectRow (row);
	if (column == 0)
	{
		QTableWidgetItem *itemCurr = item(row, 0);
		Qt::CheckState state = itemCurr->checkState();
		if (state)
			itemCurr->setCheckState(Qt::Checked);
		emit rowCheckState(row);
	}
 	m_iCurrentRow = row;
}

void TableWithKeys::on_cellDoubleClicked(int row, int column)
{	
	selectRow (row);
	emit rowMenuShow(row);
	m_iCurrentRow = row;
}
/*
void TableWithKeys::on_sectionEntered (int row)
{
	int iRow =row;
}
*/