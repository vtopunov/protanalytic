#define TEST_PURPOSE

#include "NistDialog.h"

#include <QMessageBox>

#ifdef TEST_PURPOSE
#include "converter.h"
#include <QFileDialog>
#include <QStyleFactory>
#include <QDebug>
#endif

NistDialog::NistDialog(QWidget *parent)
	: QDialog(parent),
	menu_tree(this)
{
	ui.setupUi(this);
	setStyle(QStyleFactory::create("fusion"));
	// Add greec alphabet in combobox
	for (int i = 0x3B1; i < 0x3CA; ++i)
		ui.comboBox->addItem(QChar(i));

	// Add records in Table
	for (int i = 0; i < 2; ++i)
	{
		QTableWidgetItem *newItem = new QTableWidgetItem();
		newItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		QString strSubst(QChar(0x3B1 + i) + tr("-Substation"));
		newItem->setText(strSubst);
		ui.tableWidget_Substation->setItem(i, 0, newItem);

		newItem = new QTableWidgetItem();
		newItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		newItem->setText(strSubst + tr(" description"));
		ui.tableWidget_Substation->setItem(i, 1, newItem);

		newItem = new QTableWidgetItem();
		newItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		newItem->setText(strSubst + tr(" Comment"));
		ui.tableWidget_Substation->setItem(i, 2, newItem);
	}
	// fill table widget properties
	ui.tableWithKeys->setRowCount(5);
	ui.tableWithKeys->setColumnCount(1);

	QHeaderView  * header = ui.tableWithKeys->horizontalHeader();
	header->setSectionResizeMode(0, QHeaderView::Stretch);
	header->hide();
	for (int i = 0; i < 5; ++i)
	{
		QTableWidgetItem *newItem = new QTableWidgetItem();
		newItem->setFlags(Qt::ItemIsUserCheckable | Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsTristate);
		newItem->setCheckState(Qt::Unchecked);
		newItem->setText(tr("Propertiy #") + QString::number(i + 1));
		ui.tableWithKeys->setItem(i, 0, newItem);
	}
	

}

NistDialog::~NistDialog()
{

}

void NistDialog::on_pushbutton()
{
#ifdef TEST_PURPOSE
	QString fn = QFileDialog::getOpenFileName(this, "Open file whith SIM method", tr("downloads"));
	if (fn.isEmpty())
		return;
	QFile file(fn);
	QByteArray protobuf;
	QByteArray data;

	if (fn.endsWith(tr(".txt"))){
		if (file.open(QIODevice::Text | QIODevice::ReadOnly)){
			QByteArray in = file.readAll();
			protobuf = Converter::parseMethodSIM(&in);
		}
	}
	else if (fn.endsWith(tr(".MS"))){
		if (file.open(QIODevice::ReadOnly)){
			QByteArray in = file.readAll();
			Converter::serializeSimExperiment(in, &data, &protobuf);
		}
	}

	qDebug() << "PROTOBUFFER SIM METHOD TEST\n";
	int group_count = Converter::getGroupCount(protobuf);
	qDebug() << "Groups count " << group_count;

	for (int i = 0; i < group_count; ++i){
		qDebug() << "GROUP " << i + 1 << "\nResoluion ";
		switch (Converter::getGroupResolution(protobuf, i))
		{
		case 0:
			qDebug() << "Low\n";
			break;
		case 1:
			qDebug() << "Medium\n";
			break;
		case 2:
			qDebug() << "High\n";
			break;
		default:
			break;
		}
		qDebug() << "Start Time " << Converter::getGroupStart(protobuf, i) << "\nIons-Dwell\nmass\t\tdwell\n";
		int ions_count = Converter::getGroupIonCount(protobuf, i);
		for (int j = 0; j < ions_count; ++j)
			qDebug() << Converter::getGroupIonMass(protobuf, i, j) << "\t\t" << Converter::getGroupIonDwell(protobuf, i, j) << "\n";

	}
#else
	QString strText = ui.lineEdit_numberValue->text();
	if (strText.isEmpty())
		QMessageBox::warning(this, tr("No number"), tr("line edit number value must contain less one digit"));
#endif
}

void NistDialog::on_NumberEditingFinished()
{
	QString str_number = ui.lineEdit_numberValue->text();
	bool bResult = false;
	if (str_number.isEmpty())
		return;
	str_number.toInt(&bResult);
	if (!bResult)
	{
		QMessageBox::warning(this, tr("Number edit not a number"), "Please enter a valid value into line edit");
		ui.lineEdit_numberValue->setFocus();
	}
}

void NistDialog::on_itemChanged(QTreeWidgetItem *item, int column)
{
	QMessageBox::warning(this,
		tr("Item Clicked"),
		item->text(0) +  tr(" item check state ") + QString(item->checkState(0)? tr("Checked"):tr("Unchecked"))) ;
}

void NistDialog::on_contextMenu(QPoint point)
{
	QTreeWidgetItem * currItem = ui.treeWidget->itemAt(point);
	if (!currItem)
		return;
	menu_tree.clear();
	menu_tree.setTitle(tr("Context menu ") + currItem->text(0));
	menu_tree.addAction(tr("Action type ") + currItem->text(1));
	menu_tree.popup(mapToGlobal(point + ui.treeWidget->pos()
															));
}