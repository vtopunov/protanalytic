#ifndef STICKER_H
#define STICKER_H

#include <QWidget>
#include "ui_Sticker.h"

class Sticker : public QWidget
{
	Q_OBJECT

public:
	Sticker(QWidget *parent);
	~Sticker();
	void mouseMoveEvent(QMouseEvent *event) final;
	void mouseReleaseEvent(QMouseEvent *event) final;
	void mousePressEvent(QMouseEvent *event) final;
	void setText(const QString & str);
	QString getText() const;
	void setMinimal();
private:
	
	int int_status_;
	void paintEvent(QPaintEvent *event);
	void leaveEvent(QEvent * event) final;
	void enterEvent(QEvent * event) final;
//	QPoint pointStartGlobalCursor;
	QPoint pointStartPosition;
	QPoint parent_global_;
	bool grabbed_;
	Ui::Sticker ui;
};

#endif // STICKER_H
