#ifndef TextBrowserImage_H
#define TextBrowserImage_H

#include <QTextBrowser>

class QMimeData;
class QKeyEvent;
class QTextBlockFormat;
class QTextCharFormat;
class ResumeUser;
class Sticker;
enum class Mark
{
	link_text = 0,
	link_image,
	link_ref_text,
	ref_text,
	user_text,
	user_image,
	ref_first_index = 0x20
};

class TextBrowserImage : public QTextBrowser
{
public:
	TextBrowserImage(QWidget *parent = 0);
//	bool canInsertFromMimeData(const QMimeData* source) const;
//	void insertFromMimeData(const QMimeData* source);
	static QByteArray docToArray(const QTextDocument*);
	static void arrayToDoc(const QByteArray* data, QTextDocument* doc_new);// � QTextDocument ��� ������������ �����������
	static QString makeTempFile(const QImage& img);
	const QObjectList& lst_stickers() const;
	QByteArray toArray() const;
	void fromArray(const QByteArray* data);
	void focusInEvent(QFocusEvent * event);
	QTextDocument* document_with_stickers();
private:
	QTextDocument * document_clone_;
	QString str_temp_dir;
	int image_count;
	inline int getStickersCount() const;
	public slots:
	void on_Put_Sticker();
	void correctStickers(int shift_y);
	private slots:
	inline bool isPositionReadOnly() const;
	void on_anchorClicked(const QUrl & link);
	public slots:
	
};

#endif // TextBrowserImage_H
