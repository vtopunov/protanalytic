#include "TextEditImage.h"
#include <QMimeData>
#include <QFileInfo>
#include <QImageReader>
#include <QMap>
#include <QTextBlock>
#include <QTemporaryDir>
#include <QDataStream>
#include <QKeyEvent>
#include <QDebug>
TextEditImage::TextEditImage(QWidget *parent) :QTextEdit(parent), image_count(0)
{
	connect(this, &QTextEdit::cursorPositionChanged,
		this, &TextEditImage::on_cursorPositionChanged);
}

bool TextEditImage::canInsertFromMimeData(const QMimeData* source) const
{
	return source->hasImage() || source->hasUrls() ||
		QTextEdit::canInsertFromMimeData(source);
}

void TextEditImage::insertFromMimeData(const QMimeData* source)
{

	if (isPositionReadOnly()){
		undo();
		return;
	}
	if (source->hasImage())
	{
		// Needing to copy in file is.
		QImage img = qvariant_cast<QImage>(source->imageData());
		QUrl url = makeTempFile(img);
		dropImage(url, img);
	}
	else if (source->hasUrls())
	{
		foreach(QUrl url, source->urls())
		{
			
			QFileInfo info(url.toLocalFile());
			if (QImageReader::supportedImageFormats().contains(info.suffix().toLower().toLatin1()))
				dropImage(url, QImage(info.filePath()));
			else
				dropTextFile(url);
		}
	}
	else
	{
		qDebug() << QString("BEFORE");
		qDebug ()<< source->html();
		QString str_without_userdata = source->html().remove("-qt-user-state:0;", Qt::CaseInsensitive);
		//QString str_without_userdata = source->html();
		qDebug() << "AFTER";
		qDebug() << str_without_userdata;
		QTextEdit::insertHtml(str_without_userdata);
//		QTextEdit::insertFromMimeData(source);
	}
}

void TextEditImage::dropImage(const QUrl& url, const QImage& image)
{
	if (!image.isNull())
	{
		document()->addResource(QTextDocument::ImageResource, url, image);
		textCursor().insertImage(url.toString());
	}
}

void TextEditImage::dropTextFile(const QUrl& url)
{
	QFile file(url.toLocalFile());
	if (file.open(QIODevice::ReadOnly | QIODevice::Text))
		textCursor().insertText(file.readAll());
}

QByteArray TextEditImage::docToArray(const QTextDocument* doc)
{
	QMap<QString, QImage> map_images;
	QString str_HTML = doc->toHtml();
	// �������� ���� HTML.

	int count = 0;
	int pos = 0;
	QRegExp rx("<img src=\".+/>");
	while ((pos = rx.indexIn(str_HTML, pos)) != -1)
	{
		// Making html to serialyze
		QString str_newName(QString("image_%1").arg(count));
		int indexLast = str_HTML.indexOf(QString("\""), pos+10);
		int indexFirst = str_HTML.indexOf(QString("\""), pos) + 1;
		QString str_oldName = str_HTML.mid(indexFirst, indexLast - indexFirst);
		str_HTML.replace(indexFirst, indexLast - indexFirst, str_newName);
//		str_HTML.replace(pos, rx.matchedLength(), strToReplace);
		pos += rx.matchedLength();
		count++;

		QVariant image_data = doc->resource(QTextDocument::ImageResource, QUrl(str_oldName));
		QImage picture = image_data.value<QImage>();
		map_images.insert(str_newName, picture);
	}
	QByteArray doc_data;
	QDataStream out(&doc_data, QIODevice::WriteOnly);
	out << str_HTML << map_images;
	return doc_data;
}

void TextEditImage::arrayToDoc(const QByteArray* data, QTextDocument* doc_new)
{
	QString html;
	QDataStream in(*data);
	QMap<QString, QImage> map_images;
	in >> html >> map_images;// Reading html and images with tags


	for (auto image : map_images)
	{
		QString key = map_images.key(image);
		QUrl url = makeTempFile(image);
		QString path = url.path();
		html.replace(key, url.path());
	}
	doc_new->setHtml(html);
}

QString TextEditImage::makeTempFile(const QImage&  img)
{
	static QString str_temp_dir;
	if (str_temp_dir.isEmpty()){
		static QTemporaryDir dir;
		if (dir.isValid())
			str_temp_dir = dir.path() + '/';
	}

	static int image_count = 0;
	QString str_file_path = str_temp_dir + QString("dropped_image_%1.png").arg(++image_count);
	// � ����������� � ��������� �������
	img.save(str_file_path, "PNG", 50);
	return str_file_path;
}

void TextEditImage::keyPressEvent(QKeyEvent *event)
{
	switch (event->key()){
	case Qt::Key_Delete:{
		if (isReadOnly())
			return;
		QTextCursor curs = textCursor();
		int iPosition = curs.positionInBlock();
		QTextBlock paragr_curr = curs.block();
		int iLength = paragr_curr.length();
		if (iPosition < iLength-1)	{
			if (paragr_curr.userState() == 0)
				return;
		}
		else{
			curs.movePosition(QTextCursor::NextBlock);
			QTextBlock paragr_next = curs.block();
			if (paragr_next.isValid() && paragr_next.userState() == 0)
				return;
		}
	}
	case Qt::Key_Backspace:{
		if (isReadOnly())
			return;
		QTextCursor curs = textCursor();
		int iPosition = curs.positionInBlock();
		if (iPosition > 0){
			QTextBlock paragr_curr = curs.block();
			if (paragr_curr.userState() == 0)
				return;
		}
		else{
			curs.movePosition(QTextCursor::PreviousBlock);
			QTextBlock paragr_next = curs.block();
			if (paragr_next.isValid() && paragr_next.userState() == 0)
				return;
		}
	}
	}
	QTextEdit::keyPressEvent(event);
}

void TextEditImage::on_cursorPositionChanged()
{
	// Prisyatch 17.03.15
	QTextDocument *textDocument = document();
	QTextCursor cursor_curr = textCursor();
	// Special processing for selected fragments. To prevent modification then fragment contain uneditable piece.
	bool bReadOnly = false;
	if (cursor_curr.hasSelection())
	{
		int ind_start = cursor_curr.selectionStart();
		int ind_end = cursor_curr.selectionEnd();

		QTextCursor cursor_fragm = QTextCursor(textDocument);
		int ind_position = ind_start;
		cursor_fragm.setPosition(ind_start);
		QTextBlock paragraph = cursor_fragm.block();
		while (ind_position < ind_end)
		{
			if (paragraph.isValid() && paragraph.userState() == 0){
				bReadOnly = true;
				break;
			}
			if (cursor_fragm.movePosition(QTextCursor::NextBlock)){
				paragraph = cursor_fragm.block();
				ind_position = cursor_fragm.position();
			}
		}
		setReadOnly(bReadOnly);
		return;
	}

	// Frame current cursor set is.
	int state = cursor_curr.block().userState();
	if (state == -1)
		setReadOnly(false);
	else if (state == 0)
		setReadOnly(true);
}

bool TextEditImage::isPositionReadOnly() const
{
	QTextCursor cursor_curr = textCursor();
	int state = cursor_curr.block().userState();
	if (state == 0)
		return true;
	return false;
}

