TEMPLATE = app
macx {
	CONFIG += c++11
	DEFINES += HAVE_PTHREAD
	QMAKE_CFLAGS += -gdwarf-2
	QMAKE_CXXFLAGS += -gdwarf-2
	QMAKE_INFO_PLIST= $${PWD}/Info.plist
	ICON = $${PWD}/icon.icns
	debug { DEFINES += _DEBUG }
}
DESTDIR = $$PWD/..$(Platform)/$(Configuration)/
QT += core
DEFINES += CONVERTER_LIB _SCL_SECURE_NO_WARNINGS
INCLUDEPATH += $$PWD/../google/protobuf .
SOURCES += $$PWD/$$PWD/converter.cpp $$PWD/$$PWD/google/protobuf/arena.cc $$PWD/$$PWD/google/protobuf/arenastring.cc $$PWD/$$PWD/google/protobuf/descriptor.cc $$PWD/$$PWD/google/protobuf/descriptor.pb.cc $$PWD/$$PWD/google/protobuf/descriptor_database.cc $$PWD/$$PWD/google/protobuf/dynamic_message.cc $$PWD/$$PWD/google/protobuf/extension_set.cc $$PWD/$$PWD/google/protobuf/extension_set_heavy.cc $$PWD/$$PWD/google/protobuf/generated_message_reflection.cc $$PWD/$$PWD/google/protobuf/generated_message_util.cc $$PWD/$$PWD/google/protobuf/io/coded_stream.cc $$PWD/$$PWD/google/protobuf/io/printer.cc $$PWD/$$PWD/google/protobuf/io/strtod.cc $$PWD/$$PWD/google/protobuf/io/tokenizer.cc $$PWD/$$PWD/google/protobuf/io/zero_copy_stream.cc $$PWD/$$PWD/google/protobuf/io/zero_copy_stream_impl.cc $$PWD/$$PWD/google/protobuf/io/zero_copy_stream_impl_lite.cc $$PWD/$$PWD/google/protobuf/map_field.cc $$PWD/$$PWD/google/protobuf/message.cc $$PWD/$$PWD/google/protobuf/message_lite.cc $$PWD/$$PWD/google/protobuf/reflection_ops.cc $$PWD/$$PWD/google/protobuf/repeated_field.cc $$PWD/$$PWD/google/protobuf/service.cc $$PWD/$$PWD/google/protobuf/stubs/atomicops_internals_x86_gcc.cc $$PWD/$$PWD/google/protobuf/stubs/atomicops_internals_x86_msvc.cc $$PWD/$$PWD/google/protobuf/stubs/common.cc $$PWD/$$PWD/google/protobuf/stubs/once.cc $$PWD/$$PWD/google/protobuf/stubs/stringprintf.cc $$PWD/$$PWD/google/protobuf/stubs/structurally_valid.cc $$PWD/$$PWD/google/protobuf/stubs/strutil.cc $$PWD/$$PWD/google/protobuf/stubs/substitute.cc $$PWD/$$PWD/google/protobuf/text_format.cc $$PWD/$$PWD/google/protobuf/unknown_field_set.cc $$PWD/$$PWD/google/protobuf/wire_format.cc $$PWD/$$PWD/google/protobuf/wire_format_lite.cc $$PWD/$$PWD/spectrum.pb.cc
