#ifndef CONVERTER_GLOBAL_H
#define CONVERTER_GLOBAL_H

#include <QtCore/qglobal.h>
#include <QString>


typedef QString(*PrototyeloadExperiment)(const QString&);
typedef QString(*PrototypegetName)();
typedef int(*PrototypegetSpectraCount)();
typedef QByteArray(*PrototypegetSpecData)();

typedef void(*PrototypedecodeSpectrum)(const QByteArray&);
typedef int(*PrototypegetTimeMsec)();
typedef int(*PrototypegetPeakcount)();
typedef int(*PrototypegetPeakIntens)(int);
typedef int(*PrototypegetPeakMasss)(int);

#ifdef CONVERTER_LIB
# define CONVERTER_EXPORT Q_DECL_EXPORT
#else
# define CONVERTER_EXPORT Q_DECL_IMPORT
#endif



//QString CONVERTER_EXPORT loadExperiment(const QString& strPath);
//QString  CONVERTER_EXPORT getName();
//QByteArray CONVERTER_EXPORT getSpecData();
//void CONVERTER_EXPORT decodeSpectrum(const QByteArray& data);
//
//int CONVERTER_EXPORT getSpectraCount();
//int CONVERTER_EXPORT getTimeMsec();
//int CONVERTER_EXPORT getPeakcount();
//int CONVERTER_EXPORT getPeakIntens(int iSerialNumber);
//int CONVERTER_EXPORT getPeakMasss(int iSerialNumber);

#endif // CONVERTER_GLOBAL_H


