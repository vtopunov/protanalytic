#include "converter.h"
#include "spectrum.pb.h"
#include "sim_method.pb.h"
#include <QDataStream>
#include <qDebug>
#include <QVector>
#include <algorithm>
#include <QTextStream>
#include <QTextCodec>
#include <QByteArray>
#include <QObject>
#include <QApplication>


const int min_group_len = 8;
Converter::Converter()
{
}

Converter::~Converter()
{
}
 
inline QString tr_dynamic(const QString& text, bool bEnglish = true) {
	return QApplication::translate("QObject", text.toLocal8Bit());
}

QByteArray Converter::serializeExperiment(const QByteArray& dataIn)
{
	QByteArray dataOut;
	QDataStream stream(dataIn);
	stream.setByteOrder(QDataStream::BigEndian);

	quint32 int4;
	stream >> int4;
	if ((int4 >> 16) != 0x0132)
		return dataOut;

	Experiment exp;// Now we can parse Experiment

	stream.device()->seek(264);
	quint32 spec_address;
	stream >> spec_address;
	spec_address = spec_address * 2 - 2;

	stream.device()->seek(spec_address);
	quint16 spec_block_size;
	stream >> spec_block_size;// �������� �� ������ ������ ������ �����, �� ��������� ��� ������.
	spec_block_size *= 2;

	while (spec_block_size)// ������� ��������� �����, ����� �� ������ ������ ������������ ����� �������.
	{
		serializeSpectrum(&stream, exp.add_spectrum());
		// ��������� �� ��������� ����
		spec_address += spec_block_size;
		stream.device()->seek(spec_address);
		// �������� ������ ���������� �����.
		stream >> spec_block_size;
		spec_block_size *= 2;
	}

	int size = exp.ByteSize();
	int iCount = exp.spectrum_size();
	dataOut.resize(size);
	exp.SerializeToArray(dataOut.data(), size);

	return dataOut;
}

void Converter::serializeSpectrum(QDataStream* stream, Spectrum * spec)
{
	// All addresses are relative.
	quint32 time;
	(*stream) >> time;
	spec->set_time_stamp(time);
	stream->skipRawData(6);// throu next spec size and 2 flags (Need for read precalculated data)
	quint16  peak_count;
	(*stream) >> peak_count;//throu highest peak
	stream->skipRawData(4);

	struct Peak {
		quint16 mass;
		quint16 intensity;
	};

	std::vector<Peak> vpeaks;
	vpeaks.reserve(peak_count);

	for (quint16 i = 0; i < peak_count; ++i) {
		Peak peak = { 0, 0 };
		(*stream) >> peak.mass;
		(*stream) >> peak.intensity;
		vpeaks.emplace_back(std::move(peak));
	}

	// Where no twins and values sorted descnend
	// sort to ascend and delete twins;
	std::sort(std::begin(vpeaks), std::end(vpeaks), [](const Peak& a, const Peak& b) { return a.mass < b.mass; });
	auto last = std::unique(std::begin(vpeaks), std::end(vpeaks), [](const Peak& a, const Peak& b) { return a.mass == b.mass; });
	vpeaks.erase(last, std::end(vpeaks));

	//qDebug() << "Erase\n";
	//for (auto peak_curr : vpeaks)
	//	qDebug() << peak_curr.mass / 20.0 << "\t\t" << peak_curr.intensity << "\n";

	peak_count = vpeaks.size();
	auto peaks = spec->mutable_peak();
	peaks->Reserve(peak_count);

	for (auto& value : vpeaks) {
		auto peak = peaks->Add();
		peak->set_mass(static_cast<float>(value.mass) / 20);
		peak->set_intensity(value.intensity);
	}
}

// NO TESTS
int Converter::serializeSimExperiment(const QByteArray& dataIn, QByteArray * dataOut, QByteArray * methodOut)
{
	// If you do not need sim method prefer simple function serializeExperiment
	if (!dataOut || !methodOut)
		return 2;

	QDataStream stream(dataIn);
	stream.setByteOrder(QDataStream::BigEndian);

	quint32 int4;
	stream >> int4;
	if ((int4 >> 16) != 0x0132)
		return -1;

	Experiment exp;// Now we can add empty experiment and sim method


	stream.device()->seek(264);
	quint32 spec_address;
	stream >> spec_address;
	spec_address = spec_address * 2 -2;

	stream.device()->seek(spec_address);
	quint16 spec_block_size;
	stream >> spec_block_size;// Maybe size 4 bytes, but answered is out where...
	spec_block_size *= 2;

	
	while (spec_block_size)// End of spectras we identify by absent <next spec block size>.
	{
		serializeSpectrum(&stream, exp.add_spectrum());
		// Go to next block
		spec_address += spec_block_size;
		stream.device()->seek(spec_address);
		// Size of next block
		stream >> spec_block_size;
		spec_block_size *= 2;
	}

	int size = exp.ByteSize();
	dataOut->resize(size);
	exp.SerializeToArray(dataOut->data(), size);
	 
	return extractMethodSIM(&exp, methodOut);
}

int Converter::extractMethodSIM(Experiment * expIN, QByteArray * methodOUT)
{
	MethodSIM meth;
	meth.set_reconstructed(true);
	// First Spectrum masses to integers;
	auto SpecCurr = expIN->spectrum(0);
	int PeaksInSpec = SpecCurr.peak_size();
	auto Peaks = SpecCurr.peak();
	QVector<unsigned short> vec20m(PeaksInSpec);
	for (int j = 0; j < PeaksInSpec; ++j){
		vec20m[j] = 20 * Peaks.Get(j).mass();
	}

	int group_time = SpecCurr.time_stamp();
	int SpecsInGoup = 1;
	int GoupCount = 1;
	int TotalSpecCount = expIN->spectrum_size();
	for (int i = 1; i < TotalSpecCount; ++i){// All Specs

		SpecCurr = expIN->spectrum(i);
		

		bool bNewGoup = false;// Is it new group
		// �ompare previous spec to curr:
		if (vec20m.size() != SpecCurr.peak_size())// Different peak count -> New Group ?
			if (SpecsInGoup < min_group_len)// too small group...
				return 1;//.return error
			else 
				bNewGoup = true; //...add New Group
		else{
			for (int j = 0; j < SpecCurr.peak_size(); ++j){// Is all peaks same?

				if (vec20m[j] !=  20 * Peaks.Get(j).mass()){// Different peaks founded
					if (SpecsInGoup < min_group_len)// too small group...
						return 1;//.return error
					else{
						bNewGoup = true; //...add New Group
						break;
					}
				}

			}
		}
		// ADD NEW GROUP if Spec Different
		if (bNewGoup){
			auto group = meth.add_group();
			if (GoupCount == 1)
				group->set_start(0);
			else
				group->set_start(group_time/60000);
			group->set_resolution(ResolutionType::LOW);
			// Avereged dwell sets for all ions in group
			int time_from_one_mass_to_another = 0.0468 * 2 * (Peaks.Get(PeaksInSpec - 1).mass() - Peaks.Get(0).mass()) + Peaks.size() * 0.010;
			int dwell = (((SpecCurr.time_stamp() - group_time) / SpecsInGoup) - time_from_one_mass_to_another) / PeaksInSpec;
			for (int j = 0; j < PeaksInSpec; ++j){
				auto ion = group->add_ion();
				ion->set_mass(Peaks.Get(j).mass());
				ion->set_dwell(dwell);
//				qDebug() << "mass\t" << ion->mass() << "\tdwell\t" << ion->dwell() << "\n";
			}
			group_time = SpecCurr.time_stamp();
			PeaksInSpec = SpecCurr.peak_size();
			Peaks = SpecCurr.peak();
			++GoupCount;
			SpecsInGoup = 1;
		}
		else 
			++SpecsInGoup;

		// copiing new array;
		vec20m.resize(PeaksInSpec);
		for (int j = 0; j < PeaksInSpec; ++j)
			vec20m[j] = 20 * Peaks.Get(j).mass();
	}

	// Adding last group
	auto group_last = meth.add_group();
	if (GoupCount == 1)
		group_last->set_start(0);
	else
		group_last->set_start(group_time/60000);
	group_last->set_resolution(ResolutionType::LOW);
	// Same dwell sets for all ions in group

	int time_from_one_mass_to_another = 0.0468 * 2 * (Peaks.Get(PeaksInSpec - 1).mass() - Peaks.Get(0).mass()) + Peaks.size() * 0.010;
//	int deltM = Peaks.Get(PeaksInSpec - 1).mass() - Peaks.Get(0).mass();
	int dwell = (((SpecCurr.time_stamp() - group_time) / (SpecsInGoup-1)) - time_from_one_mass_to_another) / PeaksInSpec;
//	int time1 = (SpecCurr.time_stamp() - group_time) / (SpecsInGoup - 1);
	for (int j = 0; j < PeaksInSpec; ++j){
		auto ion = group_last->add_ion();
		ion->set_mass(Peaks.Get(j).mass());
		ion->set_dwell(dwell);
	}

	int size = meth.ByteSize();
	methodOUT->resize(size);
	meth.SerializeToArray(methodOUT->data(), size);
	return 0;
}


QByteArray Converter::parseMethodSIM(QByteArray* str)
{
	QByteArray result;
	QString line;
	QTextCodec* codec = QTextCodec::codecForName("UTF-8");
	QString strAll = codec->toUnicode(*str);

	int iTranslation = -1;

	if (strAll.contains(QString("INSTRUMENT CONTROL PARAMETERS:")))
		iTranslation = 0;
	else {
		codec = QTextCodec::codecForName("Windows-1251");
		QString strAll = codec->toUnicode(*str);
		QString header = QObject::tr("INSTRUMENT CONTROL PARAMETERS:");
		if (strAll.contains(header))
			iTranslation = 1;
	}

	if (iTranslation == -1)
		return QByteArray();

	QTextStream stream(str, QIODevice::ReadOnly);
	QString str_sim_parameters = (iTranslation ? QObject::tr("[Sim Parameters]") : QString("[Sim Parameters]"));
	QString str_resolution = (iTranslation ? QObject::tr("Resolution") : QString("Resolution"));
	QString str_low = (iTranslation ? QObject::tr("Low") : QString("Low"));
	QString str_high = (iTranslation ? QObject::tr("High") : QString("High"));
	QString str_time = (iTranslation ? QObject::tr("time") : QString("time"));
	QString str_ionsdwell = (iTranslation ? QObject::tr("Ions/Dwell") : QString("Ions/Dwell"));


	// Search SIM parameters block.
	bool bExistsMethodSIM = false;
	while (!stream.atEnd()){
		line = stream.readLine();
		if (line.contains(str_sim_parameters)){
			bExistsMethodSIM = true;
			break;
		}
	}
	if (!bExistsMethodSIM)
		return QByteArray();

	MethodSIM meth;
	meth.set_reconstructed(false);
	Group* group_current = 0;

	// New group in SIM method started
	while (!stream.atEnd())// throu all text
	{
		while (!stream.atEnd()){// search resolution
			line = stream.readLine();
			if (line.contains(str_resolution)){
				if (line.contains(str_low)){
					group_current = meth.add_group();
					group_current->set_resolution(ResolutionType::LOW);
					
				}
				else if (line.contains(str_high)){
					group_current = meth.add_group();
					group_current->set_resolution(ResolutionType::HIGH);
				}
				else
					continue;// no appropriate value for string resolution
			}
			else continue;// no string resolution
			// new group were detected by line of resolutoin
			group_current->set_start(0);
			while (!stream.atEnd()){// search ions-dwells and possible gtoup start time
				line = stream.readLine();
				// After resolution and before ionsdwell lines start time may be is.
				if (line.contains(str_ionsdwell)){//on next line(s) we will wait theese pairs

					line = stream.readLine();
					int iLeft = line.indexOf("(", 0);
					int iRight = line.indexOf(")", iLeft);
					while (iLeft > 0 && iRight > 0){// // value pairs folllow in brackets.
						while (iLeft > 0 && iRight > 0)// // value pairs folllow in brackets.
						{
							QString pair = line.mid(iLeft + 1, iRight - iLeft - 1);
							QStringList lstPair = pair.split(',');
							if (lstPair.length() != 2)
								return QByteArray();
							bool bOk;
							float fMass = lstPair[0].toFloat(&bOk);
							if (!bOk)
								return QByteArray();
							int dwell = lstPair[1].toInt(&bOk);
							if (!bOk)
								return QByteArray();

							auto ion = group_current->add_ion();
							ion->set_mass(fMass);
							ion->set_dwell(dwell);
							iLeft = line.indexOf("(", iRight);
							iRight = line.indexOf(")", iLeft);
						}// end of one line of values ions and their dwells
						if (!stream.atEnd())
							line = stream.readLine();
						
						iLeft = line.indexOf("(", 0);
						iRight = line.indexOf(")", iLeft);
					}// end of lines with vlues ion-dwell. exit out to cycle of search new group
					if (line.isEmpty())
						break;// Return to search of resolution - to search new group
				}
				else if (line.contains(str_time, Qt::CaseInsensitive)){// Befor ions/dwell header we are seaching for start time (1 group it may absent)
					QString str = line.section(":", 1, 1);
					group_current->set_start(str.toFloat());
				}
				
			}

		}// end of block we founded Ion-dwell header
	}// Text is ended.
	int size = meth.ByteSize();
	result.resize(size);
	meth.SerializeToArray(result.data(), size);
	return result;
}

int Converter::getSpecsCount(const QByteArray& data)
{
	Experiment exp;
	exp.ParseFromArray(data.data(), data.size());
	return exp.spectrum_size();
}

float Converter::getSpecTime(const QByteArray& data, int spec_index)
{
	Experiment exp;
	exp.ParseFromArray(data.data(), data.size());
	int time_msec = exp.spectrum(spec_index).time_stamp();
	return static_cast<float>(time_msec) / 6e4;
}

int Converter::getPeaksCount(const QByteArray& data, int spec_index)
{
	Experiment exp;
	exp.ParseFromArray(data.data(), data.size());
	return exp.spectrum(spec_index).peak_size();
}

float Converter::getPeakMass(const QByteArray& data, int spec_index, int peak_index)
{
	Experiment exp;
	exp.ParseFromArray(data.data(), data.size());
	return exp.spectrum(spec_index).peak(peak_index).mass();
}

int Converter::getPeakIntens(const QByteArray& data, int spec_index, int peak_index)
{
	Experiment exp;
	exp.ParseFromArray(data.data(), data.size());
	return exp.spectrum(spec_index).peak(peak_index).intensity();
}

// test meth extraction
int  Converter::getGroupCount(const QByteArray& data)
{
	MethodSIM meth;
	meth.ParseFromArray(data.data(), data.size());
	return meth.group_size();
}

int  Converter::getGroupResolution(const QByteArray& data, int indx_group)
{
	MethodSIM meth;
	meth.ParseFromArray(data.data(), data.size());
	if (indx_group >= meth.group_size())
		return -1;
	return meth.group(indx_group).resolution();
}
float  Converter::getGroupStart(const QByteArray& data, int indx_group)
{
	MethodSIM meth;
	meth.ParseFromArray(data.data(), data.size());
	if (indx_group >= meth.group_size())
		return -1;
	return meth.group(indx_group).start();
}

int  Converter::getGroupIonCount(const QByteArray& data, int indx_group)
{
	MethodSIM meth;
	meth.ParseFromArray(data.data(), data.size());
	if (indx_group >= meth.group_size())
		return -1;
	return meth.group(indx_group).ion_size();
}

float Converter::getGroupIonMass(const QByteArray& data, int indx_group, int indx_ion)
{
	MethodSIM meth;
	meth.ParseFromArray(data.data(), data.size());
	if (indx_group >= meth.group_size())
		return -1;
	if (indx_ion >= meth.group(indx_group).ion_size())
		return -1;
	return meth.group(indx_group).ion(indx_ion).mass();
}

int  Converter::getGroupIonDwell(const QByteArray& data, int indx_group, int indx_ion){
	MethodSIM meth;
	meth.ParseFromArray(data.data(), data.size());
	if (indx_group >= meth.group_size())
		return -1;
	if (indx_ion >= meth.group(indx_group).ion_size())
		return -1;
	return meth.group(indx_group).ion(indx_ion).dwell();
}
