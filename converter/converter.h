#ifndef CONVERTER_H
#define CONVERTER_H

#include "converter_global.h"
#include <QString>
#include <memory>
#include <QCoreApplication>
class Spectrum;
class Experiment;
class QDataStream;
class CONVERTER_EXPORT Converter
{
	Q_DECLARE_TR_FUNCTIONS(Converter)

public:
	Converter();
	~Converter();
	static QByteArray serializeExperiment(const QByteArray& dataIn);// Spectra data from file data to protobuff array;
	static int serializeSimExperiment(const QByteArray& dataIn, QByteArray * dataOut, QByteArray * methodOut);// only for SIM files. Like serializeExperiment but trying to extracts SIM method
	static QByteArray parseMethodSIM(QByteArray* str);// Extracts exact SIM method from text of "Acqmeth.txt" to protobuff array.
	// For tests.
	// spectra
	static int getSpecsCount(const QByteArray& data);
	static float getSpecTime(const QByteArray& data, int spec_index);
	static int getPeaksCount(const QByteArray& data, int spec_index);
	static float getPeakMass(const QByteArray& data, int spec_index, int peak_index);
	static int getPeakIntens(const QByteArray& data, int spec_index, int peak_index);
	// method
	static int  getGroupCount(const QByteArray& data);
	static int  getGroupResolution(const QByteArray& data, int indx_group);
	static float  getGroupStart(const QByteArray& data, int indx_group);
	static int  getGroupIonCount(const QByteArray& data, int indx_group);
	static float  getGroupIonMass(const QByteArray& data, int indx_group, int indx_ion);
	static int  getGroupIonDwell(const QByteArray& data, int indx_group, int indx_ion);


private:
	static void serializeSpectrum(QDataStream* stream, Spectrum * spec);
	static int extractMethodSIM(Experiment * expIN, QByteArray * methodOUT);
};

#endif // CONVERTER_H
