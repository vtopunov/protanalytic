// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: spectrum.proto

#ifndef PROTOBUF_spectrum_2eproto__INCLUDED
#define PROTOBUF_spectrum_2eproto__INCLUDED

#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_spectrum_2eproto();
void protobuf_AssignDesc_spectrum_2eproto();
void protobuf_ShutdownFile_spectrum_2eproto();

class Spectrum;
class Spectrum_Peak;
class Experiment;
class Database;
class Database_Experiment;

// ===================================================================

class Spectrum_Peak : public ::google::protobuf::Message {
 public:
  Spectrum_Peak();
  virtual ~Spectrum_Peak();

  Spectrum_Peak(const Spectrum_Peak& from);

  inline Spectrum_Peak& operator=(const Spectrum_Peak& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const Spectrum_Peak& default_instance();

  void Swap(Spectrum_Peak* other);

  // implements Message ----------------------------------------------

  inline Spectrum_Peak* New() const { return New(NULL); }

  Spectrum_Peak* New(::google::protobuf::Arena* arena) const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const Spectrum_Peak& from);
  void MergeFrom(const Spectrum_Peak& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(Spectrum_Peak* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _internal_metadata_.arena();
  }
  inline void* MaybeArenaPtr() const {
    return _internal_metadata_.raw_arena_ptr();
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // required float mass = 1;
  inline bool has_mass() const;
  inline void clear_mass();
  static const int kMassFieldNumber = 1;
  inline float mass() const;
  inline void set_mass(float value);

  // required uint32 intensity = 2;
  inline bool has_intensity() const;
  inline void clear_intensity();
  static const int kIntensityFieldNumber = 2;
  inline ::google::protobuf::uint32 intensity() const;
  inline void set_intensity(::google::protobuf::uint32 value);

  // @@protoc_insertion_point(class_scope:Spectrum.Peak)
 private:
  inline void set_has_mass();
  inline void clear_has_mass();
  inline void set_has_intensity();
  inline void clear_has_intensity();

  // helper for ByteSize()
  int RequiredFieldsByteSizeFallback() const;

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  float mass_;
  ::google::protobuf::uint32 intensity_;
  friend void  protobuf_AddDesc_spectrum_2eproto();
  friend void protobuf_AssignDesc_spectrum_2eproto();
  friend void protobuf_ShutdownFile_spectrum_2eproto();

  void InitAsDefaultInstance();
  static Spectrum_Peak* default_instance_;
};
// -------------------------------------------------------------------

class Spectrum : public ::google::protobuf::Message {
 public:
  Spectrum();
  virtual ~Spectrum();

  Spectrum(const Spectrum& from);

  inline Spectrum& operator=(const Spectrum& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const Spectrum& default_instance();

  void Swap(Spectrum* other);

  // implements Message ----------------------------------------------

  inline Spectrum* New() const { return New(NULL); }

  Spectrum* New(::google::protobuf::Arena* arena) const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const Spectrum& from);
  void MergeFrom(const Spectrum& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(Spectrum* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _internal_metadata_.arena();
  }
  inline void* MaybeArenaPtr() const {
    return _internal_metadata_.raw_arena_ptr();
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  typedef Spectrum_Peak Peak;

  // accessors -------------------------------------------------------

  // required uint32 time_stamp = 1;
  inline bool has_time_stamp() const;
  inline void clear_time_stamp();
  static const int kTimeStampFieldNumber = 1;
  inline ::google::protobuf::uint32 time_stamp() const;
  inline void set_time_stamp(::google::protobuf::uint32 value);

  // repeated .Spectrum.Peak peak = 2;
  inline int peak_size() const;
  inline void clear_peak();
  static const int kPeakFieldNumber = 2;
  inline const ::Spectrum_Peak& peak(int index) const;
  inline ::Spectrum_Peak* mutable_peak(int index);
  inline ::Spectrum_Peak* add_peak();
  inline const ::google::protobuf::RepeatedPtrField< ::Spectrum_Peak >&
      peak() const;
  inline ::google::protobuf::RepeatedPtrField< ::Spectrum_Peak >*
      mutable_peak();

  // @@protoc_insertion_point(class_scope:Spectrum)
 private:
  inline void set_has_time_stamp();
  inline void clear_has_time_stamp();

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::RepeatedPtrField< ::Spectrum_Peak > peak_;
  ::google::protobuf::uint32 time_stamp_;
  friend void  protobuf_AddDesc_spectrum_2eproto();
  friend void protobuf_AssignDesc_spectrum_2eproto();
  friend void protobuf_ShutdownFile_spectrum_2eproto();

  void InitAsDefaultInstance();
  static Spectrum* default_instance_;
};
// -------------------------------------------------------------------

class Experiment : public ::google::protobuf::Message {
 public:
  Experiment();
  virtual ~Experiment();

  Experiment(const Experiment& from);

  inline Experiment& operator=(const Experiment& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const Experiment& default_instance();

  void Swap(Experiment* other);

  // implements Message ----------------------------------------------

  inline Experiment* New() const { return New(NULL); }

  Experiment* New(::google::protobuf::Arena* arena) const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const Experiment& from);
  void MergeFrom(const Experiment& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(Experiment* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _internal_metadata_.arena();
  }
  inline void* MaybeArenaPtr() const {
    return _internal_metadata_.raw_arena_ptr();
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // repeated .Spectrum spectrum = 1;
  inline int spectrum_size() const;
  inline void clear_spectrum();
  static const int kSpectrumFieldNumber = 1;
  inline const ::Spectrum& spectrum(int index) const;
  inline ::Spectrum* mutable_spectrum(int index);
  inline ::Spectrum* add_spectrum();
  inline const ::google::protobuf::RepeatedPtrField< ::Spectrum >&
      spectrum() const;
  inline ::google::protobuf::RepeatedPtrField< ::Spectrum >*
      mutable_spectrum();

  // @@protoc_insertion_point(class_scope:Experiment)
 private:

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::RepeatedPtrField< ::Spectrum > spectrum_;
  friend void  protobuf_AddDesc_spectrum_2eproto();
  friend void protobuf_AssignDesc_spectrum_2eproto();
  friend void protobuf_ShutdownFile_spectrum_2eproto();

  void InitAsDefaultInstance();
  static Experiment* default_instance_;
};
// -------------------------------------------------------------------

class Database_Experiment : public ::google::protobuf::Message {
 public:
  Database_Experiment();
  virtual ~Database_Experiment();

  Database_Experiment(const Database_Experiment& from);

  inline Database_Experiment& operator=(const Database_Experiment& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const Database_Experiment& default_instance();

  void Swap(Database_Experiment* other);

  // implements Message ----------------------------------------------

  inline Database_Experiment* New() const { return New(NULL); }

  Database_Experiment* New(::google::protobuf::Arena* arena) const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const Database_Experiment& from);
  void MergeFrom(const Database_Experiment& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(Database_Experiment* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _internal_metadata_.arena();
  }
  inline void* MaybeArenaPtr() const {
    return _internal_metadata_.raw_arena_ptr();
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // required string name = 1;
  inline bool has_name() const;
  inline void clear_name();
  static const int kNameFieldNumber = 1;
  inline const ::std::string& name() const;
  inline void set_name(const ::std::string& value);
  inline void set_name(const char* value);
  inline void set_name(const char* value, size_t size);
  inline ::std::string* mutable_name();
  inline ::std::string* release_name();
  inline void set_allocated_name(::std::string* name);

  // repeated bytes experiment = 2;
  inline int experiment_size() const;
  inline void clear_experiment();
  static const int kExperimentFieldNumber = 2;
  inline const ::std::string& experiment(int index) const;
  inline ::std::string* mutable_experiment(int index);
  inline void set_experiment(int index, const ::std::string& value);
  inline void set_experiment(int index, const char* value);
  inline void set_experiment(int index, const void* value, size_t size);
  inline ::std::string* add_experiment();
  inline void add_experiment(const ::std::string& value);
  inline void add_experiment(const char* value);
  inline void add_experiment(const void* value, size_t size);
  inline const ::google::protobuf::RepeatedPtrField< ::std::string>& experiment() const;
  inline ::google::protobuf::RepeatedPtrField< ::std::string>* mutable_experiment();

  // @@protoc_insertion_point(class_scope:Database.Experiment)
 private:
  inline void set_has_name();
  inline void clear_has_name();

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::internal::ArenaStringPtr name_;
  ::google::protobuf::RepeatedPtrField< ::std::string> experiment_;
  friend void  protobuf_AddDesc_spectrum_2eproto();
  friend void protobuf_AssignDesc_spectrum_2eproto();
  friend void protobuf_ShutdownFile_spectrum_2eproto();

  void InitAsDefaultInstance();
  static Database_Experiment* default_instance_;
};
// -------------------------------------------------------------------

class Database : public ::google::protobuf::Message {
 public:
  Database();
  virtual ~Database();

  Database(const Database& from);

  inline Database& operator=(const Database& from) {
    CopyFrom(from);
    return *this;
  }

  inline const ::google::protobuf::UnknownFieldSet& unknown_fields() const {
    return _internal_metadata_.unknown_fields();
  }

  inline ::google::protobuf::UnknownFieldSet* mutable_unknown_fields() {
    return _internal_metadata_.mutable_unknown_fields();
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const Database& default_instance();

  void Swap(Database* other);

  // implements Message ----------------------------------------------

  inline Database* New() const { return New(NULL); }

  Database* New(::google::protobuf::Arena* arena) const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const Database& from);
  void MergeFrom(const Database& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(Database* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _internal_metadata_.arena();
  }
  inline void* MaybeArenaPtr() const {
    return _internal_metadata_.raw_arena_ptr();
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  typedef Database_Experiment Experiment;

  // accessors -------------------------------------------------------

  // repeated .Database.Experiment experiment = 1;
  inline int experiment_size() const;
  inline void clear_experiment();
  static const int kExperimentFieldNumber = 1;
  inline const ::Database_Experiment& experiment(int index) const;
  inline ::Database_Experiment* mutable_experiment(int index);
  inline ::Database_Experiment* add_experiment();
  inline const ::google::protobuf::RepeatedPtrField< ::Database_Experiment >&
      experiment() const;
  inline ::google::protobuf::RepeatedPtrField< ::Database_Experiment >*
      mutable_experiment();

  // @@protoc_insertion_point(class_scope:Database)
 private:

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  ::google::protobuf::uint32 _has_bits_[1];
  mutable int _cached_size_;
  ::google::protobuf::RepeatedPtrField< ::Database_Experiment > experiment_;
  friend void  protobuf_AddDesc_spectrum_2eproto();
  friend void protobuf_AssignDesc_spectrum_2eproto();
  friend void protobuf_ShutdownFile_spectrum_2eproto();

  void InitAsDefaultInstance();
  static Database* default_instance_;
};
// ===================================================================


// ===================================================================

// Spectrum_Peak

// required float mass = 1;
inline bool Spectrum_Peak::has_mass() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void Spectrum_Peak::set_has_mass() {
  _has_bits_[0] |= 0x00000001u;
}
inline void Spectrum_Peak::clear_has_mass() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void Spectrum_Peak::clear_mass() {
  mass_ = 0;
  clear_has_mass();
}
inline float Spectrum_Peak::mass() const {
  // @@protoc_insertion_point(field_get:Spectrum.Peak.mass)
  return mass_;
}
inline void Spectrum_Peak::set_mass(float value) {
  set_has_mass();
  mass_ = value;
  // @@protoc_insertion_point(field_set:Spectrum.Peak.mass)
}

// required uint32 intensity = 2;
inline bool Spectrum_Peak::has_intensity() const {
  return (_has_bits_[0] & 0x00000002u) != 0;
}
inline void Spectrum_Peak::set_has_intensity() {
  _has_bits_[0] |= 0x00000002u;
}
inline void Spectrum_Peak::clear_has_intensity() {
  _has_bits_[0] &= ~0x00000002u;
}
inline void Spectrum_Peak::clear_intensity() {
  intensity_ = 0u;
  clear_has_intensity();
}
inline ::google::protobuf::uint32 Spectrum_Peak::intensity() const {
  // @@protoc_insertion_point(field_get:Spectrum.Peak.intensity)
  return intensity_;
}
inline void Spectrum_Peak::set_intensity(::google::protobuf::uint32 value) {
  set_has_intensity();
  intensity_ = value;
  // @@protoc_insertion_point(field_set:Spectrum.Peak.intensity)
}

// -------------------------------------------------------------------

// Spectrum

// required uint32 time_stamp = 1;
inline bool Spectrum::has_time_stamp() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void Spectrum::set_has_time_stamp() {
  _has_bits_[0] |= 0x00000001u;
}
inline void Spectrum::clear_has_time_stamp() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void Spectrum::clear_time_stamp() {
  time_stamp_ = 0u;
  clear_has_time_stamp();
}
inline ::google::protobuf::uint32 Spectrum::time_stamp() const {
  // @@protoc_insertion_point(field_get:Spectrum.time_stamp)
  return time_stamp_;
}
inline void Spectrum::set_time_stamp(::google::protobuf::uint32 value) {
  set_has_time_stamp();
  time_stamp_ = value;
  // @@protoc_insertion_point(field_set:Spectrum.time_stamp)
}

// repeated .Spectrum.Peak peak = 2;
inline int Spectrum::peak_size() const {
  return peak_.size();
}
inline void Spectrum::clear_peak() {
  peak_.Clear();
}
inline const ::Spectrum_Peak& Spectrum::peak(int index) const {
  // @@protoc_insertion_point(field_get:Spectrum.peak)
  return peak_.Get(index);
}
inline ::Spectrum_Peak* Spectrum::mutable_peak(int index) {
  // @@protoc_insertion_point(field_mutable:Spectrum.peak)
  return peak_.Mutable(index);
}
inline ::Spectrum_Peak* Spectrum::add_peak() {
  // @@protoc_insertion_point(field_add:Spectrum.peak)
  return peak_.Add();
}
inline const ::google::protobuf::RepeatedPtrField< ::Spectrum_Peak >&
Spectrum::peak() const {
  // @@protoc_insertion_point(field_list:Spectrum.peak)
  return peak_;
}
inline ::google::protobuf::RepeatedPtrField< ::Spectrum_Peak >*
Spectrum::mutable_peak() {
  // @@protoc_insertion_point(field_mutable_list:Spectrum.peak)
  return &peak_;
}

// -------------------------------------------------------------------

// Experiment

// repeated .Spectrum spectrum = 1;
inline int Experiment::spectrum_size() const {
  return spectrum_.size();
}
inline void Experiment::clear_spectrum() {
  spectrum_.Clear();
}
inline const ::Spectrum& Experiment::spectrum(int index) const {
  // @@protoc_insertion_point(field_get:Experiment.spectrum)
  return spectrum_.Get(index);
}
inline ::Spectrum* Experiment::mutable_spectrum(int index) {
  // @@protoc_insertion_point(field_mutable:Experiment.spectrum)
  return spectrum_.Mutable(index);
}
inline ::Spectrum* Experiment::add_spectrum() {
  // @@protoc_insertion_point(field_add:Experiment.spectrum)
  return spectrum_.Add();
}
inline const ::google::protobuf::RepeatedPtrField< ::Spectrum >&
Experiment::spectrum() const {
  // @@protoc_insertion_point(field_list:Experiment.spectrum)
  return spectrum_;
}
inline ::google::protobuf::RepeatedPtrField< ::Spectrum >*
Experiment::mutable_spectrum() {
  // @@protoc_insertion_point(field_mutable_list:Experiment.spectrum)
  return &spectrum_;
}

// -------------------------------------------------------------------

// Database_Experiment

// required string name = 1;
inline bool Database_Experiment::has_name() const {
  return (_has_bits_[0] & 0x00000001u) != 0;
}
inline void Database_Experiment::set_has_name() {
  _has_bits_[0] |= 0x00000001u;
}
inline void Database_Experiment::clear_has_name() {
  _has_bits_[0] &= ~0x00000001u;
}
inline void Database_Experiment::clear_name() {
  name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
  clear_has_name();
}
inline const ::std::string& Database_Experiment::name() const {
  // @@protoc_insertion_point(field_get:Database.Experiment.name)
  return name_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void Database_Experiment::set_name(const ::std::string& value) {
  set_has_name();
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:Database.Experiment.name)
}
inline void Database_Experiment::set_name(const char* value) {
  set_has_name();
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:Database.Experiment.name)
}
inline void Database_Experiment::set_name(const char* value, size_t size) {
  set_has_name();
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:Database.Experiment.name)
}
inline ::std::string* Database_Experiment::mutable_name() {
  set_has_name();
  // @@protoc_insertion_point(field_mutable:Database.Experiment.name)
  return name_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* Database_Experiment::release_name() {
  clear_has_name();
  return name_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void Database_Experiment::set_allocated_name(::std::string* name) {
  if (name != NULL) {
    set_has_name();
  } else {
    clear_has_name();
  }
  name_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), name);
  // @@protoc_insertion_point(field_set_allocated:Database.Experiment.name)
}

// repeated bytes experiment = 2;
inline int Database_Experiment::experiment_size() const {
  return experiment_.size();
}
inline void Database_Experiment::clear_experiment() {
  experiment_.Clear();
}
inline const ::std::string& Database_Experiment::experiment(int index) const {
  // @@protoc_insertion_point(field_get:Database.Experiment.experiment)
  return experiment_.Get(index);
}
inline ::std::string* Database_Experiment::mutable_experiment(int index) {
  // @@protoc_insertion_point(field_mutable:Database.Experiment.experiment)
  return experiment_.Mutable(index);
}
inline void Database_Experiment::set_experiment(int index, const ::std::string& value) {
  // @@protoc_insertion_point(field_set:Database.Experiment.experiment)
  experiment_.Mutable(index)->assign(value);
}
inline void Database_Experiment::set_experiment(int index, const char* value) {
  experiment_.Mutable(index)->assign(value);
  // @@protoc_insertion_point(field_set_char:Database.Experiment.experiment)
}
inline void Database_Experiment::set_experiment(int index, const void* value, size_t size) {
  experiment_.Mutable(index)->assign(
    reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_set_pointer:Database.Experiment.experiment)
}
inline ::std::string* Database_Experiment::add_experiment() {
  return experiment_.Add();
}
inline void Database_Experiment::add_experiment(const ::std::string& value) {
  experiment_.Add()->assign(value);
  // @@protoc_insertion_point(field_add:Database.Experiment.experiment)
}
inline void Database_Experiment::add_experiment(const char* value) {
  experiment_.Add()->assign(value);
  // @@protoc_insertion_point(field_add_char:Database.Experiment.experiment)
}
inline void Database_Experiment::add_experiment(const void* value, size_t size) {
  experiment_.Add()->assign(reinterpret_cast<const char*>(value), size);
  // @@protoc_insertion_point(field_add_pointer:Database.Experiment.experiment)
}
inline const ::google::protobuf::RepeatedPtrField< ::std::string>&
Database_Experiment::experiment() const {
  // @@protoc_insertion_point(field_list:Database.Experiment.experiment)
  return experiment_;
}
inline ::google::protobuf::RepeatedPtrField< ::std::string>*
Database_Experiment::mutable_experiment() {
  // @@protoc_insertion_point(field_mutable_list:Database.Experiment.experiment)
  return &experiment_;
}

// -------------------------------------------------------------------

// Database

// repeated .Database.Experiment experiment = 1;
inline int Database::experiment_size() const {
  return experiment_.size();
}
inline void Database::clear_experiment() {
  experiment_.Clear();
}
inline const ::Database_Experiment& Database::experiment(int index) const {
  // @@protoc_insertion_point(field_get:Database.experiment)
  return experiment_.Get(index);
}
inline ::Database_Experiment* Database::mutable_experiment(int index) {
  // @@protoc_insertion_point(field_mutable:Database.experiment)
  return experiment_.Mutable(index);
}
inline ::Database_Experiment* Database::add_experiment() {
  // @@protoc_insertion_point(field_add:Database.experiment)
  return experiment_.Add();
}
inline const ::google::protobuf::RepeatedPtrField< ::Database_Experiment >&
Database::experiment() const {
  // @@protoc_insertion_point(field_list:Database.experiment)
  return experiment_;
}
inline ::google::protobuf::RepeatedPtrField< ::Database_Experiment >*
Database::mutable_experiment() {
  // @@protoc_insertion_point(field_mutable_list:Database.experiment)
  return &experiment_;
}


// @@protoc_insertion_point(namespace_scope)

#ifndef SWIG
namespace google {
namespace protobuf {


}  // namespace protobuf
}  // namespace google
#endif  // SWIG

// @@protoc_insertion_point(global_scope)

#endif  // PROTOBUF_spectrum_2eproto__INCLUDED
