
#include <QtCore/QCoreApplication>
#include <converter/converter.h>
#include <QtCore>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	auto in_file = a.arguments().value(1);
	if (in_file.isEmpty()) {
		qDebug() << "error: invalid first argument";
		return 1;
	}

	QFile file(in_file);
	if (!file.open(QIODevice::ReadOnly)) {
		qDebug() << "error: can not open file" << in_file;
		return 1;
	}

	auto data = file.readAll();
	if (data.isEmpty()) {
		qDebug() << "error: empty file" << in_file;
		return 1;
	}

	auto result = Converter::serializeExperiment(std::move(data));
	if (result.isEmpty()) {
		qDebug() << "error: invalid serialize";
		return 1;
	}

	auto out_file = a.arguments().value(2);
	if (out_file.isEmpty()) {
		qDebug() << "error: invalid second argument\n";
		return 1;
	}

	QFile out(out_file);
	if (!out.open(QFile::WriteOnly)) {
		qDebug() << "error: can not open out file\n";
		return 1;
	}

	out.write(std::move(result));

	qDebug() << "success";

	return 0;
}
