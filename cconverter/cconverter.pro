TEMPLATE = app
macx {
	CONFIG += c++11
	DEFINES += HAVE_PTHREAD
	QMAKE_CFLAGS += -gdwarf-2
	QMAKE_CXXFLAGS += -gdwarf-2
	QMAKE_INFO_PLIST= $${PWD}/Info.plist
	ICON = $${PWD}/icon.icns
	debug { DEFINES += _DEBUG }
}
DESTDIR = $$PWD/../bin
QT += core
INCLUDEPATH += $$PWD/.. .
LIBS += -L$$PWD/../bin/
LIBS += lconverter
SOURCES += $$PWD/$$PWD/main.cpp
