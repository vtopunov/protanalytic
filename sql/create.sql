﻿CREATE TABLE experiment
(
    id serial primary key,
    name varchar(128),
    data bytea
);

CREATE TABLE spectrum
(
  id serial primary key,
  experimentid serial NOT NULL references experiment(id),
  name varchar(128),
  data bytea
);