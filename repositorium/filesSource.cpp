#include "FilesSource.h"


#include <QFile>
#include <QFileDialog>
#include <QLabel>
#include <QMessageBox>
#include <QSpinBox>
#include <QString>
#include <QLineEdit>
#include <QTextStream>
#include <QHBoxLayout> 
#include <QVBoxLayout> 
#include <QPushButton>
#include <QMessageBox>
#include <QDir>
#include <QDirIterator>
#include <QFile>
#include <QDebug>


FilesSource::FilesSource(const QString & title, QWidget * parent)
: QGroupBox(title, parent), b_search_packages(false)
{
//	setCheckable(true);
	// �������� ����������.
	auto * layoutMain = new QVBoxLayout(this);
	setLayout(layoutMain);

	label_comment_ = new QLabel(this);
	
	layoutMain->addWidget(label_comment_);

	// ���������� ������������ ������ � ���������� ������������� �������
	auto * layoutBrowse = new QHBoxLayout(this);
	layoutMain->addLayout(layoutBrowse);
	// ������ ��������.

	lineedit_path_ = new QLineEdit(this);
	
	layoutBrowse->addWidget(lineedit_path_);
	pushbutton_ = new QPushButton("Directory Browse", this);
	pushbutton_->setFixedWidth(100);
	layoutBrowse->addWidget(pushbutton_);

	pushbutton_copy_ = new QPushButton("Copy", this);
	pushbutton_copy_->setFixedWidth(100);
	layoutBrowse->addWidget(pushbutton_copy_);

	label_results_ = new QLabel("No results");
	
	layoutMain->addWidget(label_results_);
	// ������ �� ��������
	QFont widgetFont = font();
	widgetFont.setBold(true);
	widgetFont.setPixelSize(18);
	setFont(widgetFont);
	widgetFont.setBold(false);
	label_results_->setFont(widgetFont);
	label_comment_->setFont(widgetFont);
	lineedit_path_->setFont(widgetFont);
	widgetFont.setPixelSize(10);
	pushbutton_->setFont(widgetFont);
	
	connect(pushbutton_, &QPushButton::pressed, this, &FilesSource::on_browse);
	connect(lineedit_path_, &QLineEdit::textChanged, this, &FilesSource::on_textEdited);
	connect(pushbutton_copy_, &QPushButton::pressed, this, &FilesSource::on_copy_folder);
	translateUI();
}

FilesSource::~FilesSource()
{

}

void FilesSource::translateUI()
{
	//m_pushButtonMZML->setText(tr("Open mzML"));
}

void FilesSource::setComment(const QString & text)
{
	label_comment_->setText(text);
}


void FilesSource::on_browse()
{
	str_path_ = QFileDialog::getExistingDirectory(this, tr("Chose Directory for ").append(title()), "/home/");
	if (str_path_.isEmpty())
		return;
	lineedit_path_->setText(str_path_);
}

void FilesSource::on_textEdited(const QString &)
{
	QPalette palette;
	QDir dir(lineedit_path_->text());
	if (dir.exists())
		palette.setColor(QPalette::Text, Qt::black);
	else
		palette.setColor(QPalette::Text, Qt::darkRed);
	lineedit_path_->setPalette(palette);
	if (b_search_packages)
	{
		QStringList dirs;
		checkOnPackages(&dirs);
		if (!dirs.isEmpty())
			emit packageDirs(dirs);
	}
}

void FilesSource::setFolder(const QString& folder)
{
	if (folder.isEmpty())
		lineedit_path_->setText(QDir::currentPath() + "/../ProtoInstaller/");
	else
		lineedit_path_->setText(folder);
}

QString FilesSource::getFoder() const
{
	return lineedit_path_->text();
}

void FilesSource::checkOnPackages(QStringList* packagedirs)
{
	QDir dir(lineedit_path_->text().trimmed());
	if (dir.exists() )
	{// ��������� �� ���������� ����� � ������ ���������.
		QStringList subdirs = dir.entryList(QDir::AllDirs | QDir::NoDotAndDotDot);
		int iResult = 0;
		int iPackageIdx = -1;
		if (subdirs.contains("config") )
			iResult += 1;
		if (subdirs.contains("packages"))
		{
			iPackageIdx = subdirs.indexOf("packages");
			iResult += 2;
		}
		label_comment_->show();
		switch (iResult)
		{
		case 0:
		{
			label_results_->setText("'config' and 'packages' folder absent.");
			break;
		}
		case 1:
		{
			label_results_->setText("No 'packages' folder.");
			break;
		}
		case 2:
		{
			label_results_->setText("No 'config' folder.");
			break;
		}
		case 3:
		{
			label_comment_->hide();
			QDir dir_packages(dir.absolutePath().append("/packages"));
			QStringList lst_packages = dir_packages.entryList(QDir::AllDirs | QDir::NoDotAndDotDot);
			QString str("Founded Installer  structure with following packages:");
			for (auto strPackage : lst_packages)// ������ ���� �����, ������������ � ����� (packages) 
			{
				str += "\n";
				str += strPackage;
				// ���� � ���������� ������ ���� ������������� "data" - ��������� �� � ������� �������, ������� ����� ���������
				QString str_package_with_data(QString("%1/%2/data").arg(dir_packages.absolutePath(), strPackage));
				QDir dirEx;
			//	dir.exists();
				if (dirEx.exists(str_package_with_data))
				{
					packagedirs->append(str_package_with_data);
				}
			}
			label_results_->setText(str);
			break;
		}
		}

	}
	else
	{
		label_results_->setText("No such directory");
	}
}

bool FilesSource::�opyFolder(QString strFrom, QString strTo)//������� ����������� ����� � �� ����������.
{
	QDir dir;
	//strFrom - ��� �����, ������  ����������
	//strTo - ��� �����, ����  ����������

	//��������, ��� ����� ������������� ������, ���� ��� - �������
	strTo = QDir::cleanPath(strTo).append("/");
	strFrom = QDir::cleanPath(strFrom).append("/");


	//������� ("��������") ������� �����
//	QDirIterator itr(strFrom, QDirIterator::Subdirectories);
	QDirIterator itr(strFrom);
	dir.mkdir(strTo);

	//������� ������ � ����� � ������� ����������
	while (itr.hasNext())
	{
		qDebug() << itr.path();
		QString strFilePath = itr.next();
		QString strFileTitle = itr.fileName();

		//�������� "." � ".."
		if ((itr.fileName() != ".") && (itr.fileName() != "..") && (itr.fileName() != ""))
		{
			if ((strFileTitle == ".") || (strFileTitle == "..") || (strFileTitle.isEmpty()))
				continue;
			if (itr.fileInfo().isDir()){ //����������� ���������� � ��������� ����� (strTo + strFileTitle)
				�opyFolder(strFilePath, strTo + strFileTitle);
			}
			else{
				
				QFile::copy(strFilePath, strTo + strFileTitle);
			}
		}
	}

	return true;
}

bool FilesSource::remove_folder(const QString &path)
{
	bool result = true;
	QFileInfo info(path);
	if (info.isDir())
	{
		QDir dir(path);
		foreach(const QString &entry, dir.entryList(QDir::AllDirs | QDir::Files | QDir::Hidden | QDir::NoDotAndDotDot)) 
		{
			result &= remove_folder(dir.absoluteFilePath(entry));
		}
		if (!info.dir().rmdir(info.fileName()))
			return false;
	}
	else 
		result = QFile::remove(path);
	return result;
}

bool FilesSource::clean_folder(const QString &path)
{
	bool result = true;
	QDir dir(path);
	foreach(const QString & entry, dir.entryList(QDir::AllDirs | QDir::Files | QDir::Hidden | QDir::NoDotAndDotDot))
		result &= remove_folder(dir.absoluteFilePath(entry));
	return result;
}

void FilesSource::on_copy_folder()
{
	if (b_search_packages)
		return;
	QString dest = title();
	clean_folder(dest);
	�opyFolder(lineedit_path_->text(), dest);
	if (title().endsWith("/Analytic/bin/", Qt::CaseInsensitive))
	{
		copy_QT(dest);
		copy_PGSQL(dest);
	}
}

void FilesSource::copy_QT(const QString &dest)
{
	QString str_dest(dest);
	if (!str_dest.endsWith("/"))
		str_dest += QString("/");

	QStringList lst_DLL;
#ifdef Q_OS_WIN
	lst_DLL << "Qt5Cored.dll"
		//<< "Qt5Core.dll"
		<< "Qt5Guid.dll"
		//<< "Qt5Gui.dll"
		<< "Qt5Widgetsd.dll"
		//<< "Qt5Widgets.dll"
		<< "icudt53.dll" << "icuin53.dll" << "icuuc53.dll";
#else
	lst_DLL << "Qt5Cored.so"
		//<< "Qt5Core.dll"
		<< "Qt5Guid.so"
		//<< "Qt5Gui.dll"
		<< "Qt5Widgetsd.so"
		//<< "Qt5Widgets.dll"
		<< "icudt53.so" << "icuin53.so" << "icuuc53.so";
#endif
	QString path_DLL = QDir::cleanPath(qgetenv("qtdir")).append("/bin/");
	for (auto str_dll : lst_DLL)
	{
		QFile::copy(path_DLL + str_dll, str_dest + str_dll);
	}

	// Copying IMAGEFORMATS
	QString path_imagefs = QDir::cleanPath(qgetenv("qtdir")).append("/plugins/imageformats/");
	QDir dir(path_imagefs);
	if (!dir.exists())
		return;
	//str_dest.append("imageformats/");
	QDir dir_dest;
	dir_dest.mkdir(str_dest + "imageformats/");
	QFileInfoList lst_entries = dir.entryInfoList(QDir::NoDotAndDotDot | QDir::Files);
	for (auto entry : lst_entries)
	{
		QString name = entry.fileName();
#ifdef Q_OS_WIN
		if (name.endsWith("d.dll"))
#else
		if (name.endsWith("d.so"))
#endif
			QFile::copy(entry.absoluteFilePath(), str_dest + "imageformats/" + name);
	}

	// Copying PLATFORMS
	path_imagefs = QDir::cleanPath(qgetenv("qtdir")).append("/plugins/platforms/");
	QDir dir2(path_imagefs);
	if (!dir2.exists())
		return;
	QDir dir_dest2;
	dir_dest2.mkdir(str_dest + "platforms/");
	lst_entries = dir2.entryInfoList(QDir::NoDotAndDotDot | QDir::Files);
	for (auto entry : lst_entries)
	{
		QString name = entry.fileName();
#ifdef Q_OS_WIN
		if (name.endsWith("d.dll"))
#else
		if (name.endsWith("d.so"))
#endif
			QFile::copy(entry.absoluteFilePath(), str_dest + "platforms/" + name);
	}

}

void FilesSource::copy_PGSQL(const QString &dest)
{
	QDir dir(dest);
	dir.cdUp();
	dir.cdUp();
	dir.cdUp();
	dir.cdUp();
	dir.cdUp();
	dir.cdUp();
	//dir.setPath(dir.absolutePath() + "/pgsql/bin");
	QString path_from = dir.absolutePath() + "/pgsql/bin/";
	dir.setPath(path_from);
	if (dir.exists()){
		QStringList lstDLL;
#ifdef Q_OS_WIN
		lstDLL<<"libpq.dll";
		lstDLL<<"ssleay32.dll";
		lstDLL << "intl.dll";
		lstDLL << "libeay32.dll";
#else
		lstDLL << "libpq.so";
		lstDLL << "ssleay32.so";
		lstDLL << "intl.so";
		lstDLL << "libeay32.so";
#endif
		for (auto entry : lstDLL)
			QFile::copy(path_from + entry, dest + entry);
	}


}