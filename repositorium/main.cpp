//#include "repositorium.h"
//#include <QtWidgets/QApplication>
//
//int main(int argc, char *argv[])
//{
//	QApplication a(argc, argv);
//	Repositorium w;
//	w.show();
//	return a.exec();
//}

#include <QtWidgets>
#include "repositorium.h"

//! [0]
int main(int argc, char *argv[])
//! [0] //! [1]
{
	QApplication app(argc, argv);

	//! [1] //! [3]
	app.setWindowIcon(QIcon("blendIcon.ico"));
	Repositorium mainWindow;
	mainWindow.show();
	return app.exec();
}