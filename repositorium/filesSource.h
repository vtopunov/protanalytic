#ifndef FilesSource_H
#define FilesSource_H

#include <QWidget>
#include <QGroupBox>

class QSpinBox;
class QTextEdit;
class QLabel;
class QLineEdit;
class QPushButton;
class FilesSource : public QGroupBox
{
	Q_OBJECT

public:
	FilesSource(const QString & title, QWidget * parent = 0);
	~FilesSource();
	void translateUI();
	void setComment(const QString & text);
	void setReuslts(const QString & text);
	void setFolder(const QString& folder);
	QString getFoder() const;
	void checkOnPackages(QStringList* packagedirsOut);
	void set_search_packages(bool bSearch = true){ b_search_packages = bSearch; }
	bool �opyFolder(QString strFrom, QString strTo);
	static bool remove_folder(const QString &path);
	static bool clean_folder(const QString &path);
private:
	bool b_search_packages;
	QString str_path_;
	QPushButton * pushbutton_;// ��������� - ���������� ������ ������ ��� ���� ������.
	QPushButton * pushbutton_copy_;
	QLabel * label_comment_;
	QLabel * label_results_;
	QLineEdit * lineedit_path_;
	QSpinBox * spinbox_shift_;

	void copy_QT(const QString &dest);
	void copy_PGSQL(const QString &dest);
	private slots:
	void on_browse();
	void on_textEdited(const QString & text);
	public slots:
	void on_copy_folder();
signals:
	void packageDirs(QStringList dirs);
};

#endif // FilesSource_H
