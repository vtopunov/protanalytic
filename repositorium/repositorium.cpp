#include "Repositorium.h"

#include <QDebug>

#include <QAction>
#include <QDir>
#include <QFileDialog>
#include <QLabel>
#include <QMenuBar>
#include <QMessageBox>
#include <QTextEdit>
#include <QPushButton>
#include <QTextStream>
#include <QDataStream>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QProcess>
#include <QSpinBox>
#include <QLineEdit>
#include "filesSource.h"

const char* INSTALLER_NAME = "InterlabProtoInstaller.exe";
Repositorium::Repositorium() :fs_destination(0)
{
	setWindowState(Qt::WindowMaximized);
	setWindowTitle("Repositorium");

	auto widgetCentral = new QWidget(this);
	setCentralWidget(widgetCentral);
	layout_main_ = new QVBoxLayout(widgetCentral);

	fs_destination = createDestinationFolder();
	layout_main_->addWidget(fs_destination);
//	layoutHor->addWidget(m_pushbuttonNext);

	createMenuBar();
	translateUI();
	fs_destination->setFolder(QString());

	//openfile("C:/Users/Prisyatch/Downloads/Data/OPST1000.D/DATA.MS");
//	openfile("C:/Users/Prisyatch/Downloads/Files_SIM_SCAN/Files_SIM_SCAN/HCB_SIM.D/DATA.MS");
}

void Repositorium::createMenuBar()
{
//	m_menuFile = new QMenu(this);
//	menuBar()->addMenu(m_menuFile);
	actionOpenDirs_ = new QAction(this);
	menuBar()->addAction(actionOpenDirs_);
	connect(actionOpenDirs_, &QAction::triggered, this, &Repositorium::on_openDirs);

	action_makeInstaller_ = new QAction(this);
	menuBar()->addAction(action_makeInstaller_);
	connect(action_makeInstaller_, &QAction::triggered, this, &Repositorium::on_makeInstaller);

	action_make_all_ = new QAction(this);
	menuBar()->addAction(action_make_all_);
	connect(action_make_all_, &QAction::triggered, this, &Repositorium::on_make_all);
	action_make_run_ = new QAction(this);
	menuBar()->addAction(action_make_run_);
	connect(action_make_run_, &QAction::triggered, this, &Repositorium::on_make_run);

}

void Repositorium::translateUI()
{
	//m_menuFile->setTitle(tr("&File"));
	actionOpenDirs_->setText("Open-packges");
	action_makeInstaller_->setText("Assemble-Installer");
	action_make_all_->setText(tr("Copy-Files+Asseble-installer"));
	action_make_run_->setText(tr("Copy+Asseble+Run"));
}

FilesSource * Repositorium::createDestinationFolder()
{
	if (fs_destination)
		delete fs_destination;
	fs_destination = new FilesSource("Destination Folder", this);
	connect(fs_destination, &FilesSource::packageDirs, this, &Repositorium::on_package_data_folders);
	fs_destination->setComment(
		"Please specify the folder containing the structure required for assembly installer.\nIt is folder contains two subfolders: 'packages' & 'config'."
		);
	fs_destination->set_search_packages();

	return fs_destination;
}

FilesSource * Repositorium::createSourceFolder(const QString& destination)
{
	char * ANALYTIC = "/Analytic/";
	QString path;
	QDir dir;
	if (destination.trimmed().endsWith("/packages/Proto.pgsql/data", Qt::CaseInsensitive))	{
		path = destination + ANALYTIC;//" pgsql / ";
		dir.mkdir(path);
		path.append("pgsql/");
		dir.mkdir(path);
	}
	else if (destination.trimmed().endsWith("/packages/Proto.analytic/data", Qt::CaseInsensitive)){
		path = destination + ANALYTIC;
		dir.mkdir(path);
		path.append("bin/");
		dir.mkdir(path);
	}
	
	auto fs = new FilesSource(path, this);
	fs->setComment(
		"Open folder"
		);
	
	if (path.endsWith("/pgsql/", Qt::CaseInsensitive))
	{
		fs->setFolder(dir_main.absolutePath().append("/pgsql/"));
	}
	else if (path.endsWith("/bin/", Qt::CaseInsensitive)){
		fs->setFolder(dir_main.absolutePath().append("/bin/"));
	}
	else 
		fs->setFolder(dir_main.absolutePath());
	return fs;
}

void Repositorium::on_package_data_folders(QStringList dirs)
{
	dir_main.setPath(fs_destination->getFoder());
	dir_main.cdUp();
	while (!layout_main_->isEmpty()) 
		layout_main_->takeAt(0)->widget()->deleteLater();

	for (auto str_dir : dirs)
	{
		auto * fs = createSourceFolder(str_dir);
		layout_main_->addWidget(fs);
//		fs->on_copy_folder();
	}
//	action_makeInstaller_->trigger();
	if (fs_destination)
		fs_destination->deleteLater();
}

void Repositorium::on_openDirs()
{
	while (!layout_main_->isEmpty())
		layout_main_->takeAt(0)->widget()->deleteLater();
	fs_destination = 0;
	layout_main_->addWidget(createDestinationFolder());
	fs_destination->setFolder(QString());
}

void Repositorium::on_makeInstaller()
{
	QDir dir(QDir::currentPath());
	dir.cdUp();
	QString path_current = dir.absolutePath() + "/ProtoInstaller";
		if (QDir::setCurrent(path_current)) {
			QProcess::execute(QString("binarycreator.exe -c config/config.xml -p packages ") + INSTALLER_NAME);
	//		qDebug() << 
		/*QProcess proc;
		proc.start(QString("binarycreator.exe -c config/config.xml -p packages ") + INSTALLER_NAME);
		if (proc.waitForFinished()) {
			layout_main_->addWidget(new QLabel(proc.readAll()));
		}*/
	}
}

void Repositorium::on_make_all()
{
	for (int i = 0; i < 2; ++i)
	{
		auto widg = layout_main_->itemAt(i)->widget();
		auto fs = dynamic_cast<FilesSource*>(widg);
		if (!fs)
			continue;
		fs->on_copy_folder();
	}
	on_makeInstaller();
}

void Repositorium::on_make_run()
{
	on_make_all();
	QString path_installer = QDir::currentPath() + "/" +INSTALLER_NAME;
//	QProcess proc;
//	QProcess::startDetached(path_installer);
	QProcess::execute(path_installer);
}


