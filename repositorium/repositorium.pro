TEMPLATE = app
macx {
	CONFIG += c++11
	DEFINES += HAVE_PTHREAD
	QMAKE_CFLAGS += -gdwarf-2
	QMAKE_CXXFLAGS += -gdwarf-2
	QMAKE_INFO_PLIST= $${PWD}/Info.plist
	ICON = $${PWD}/icon.icns
	debug { DEFINES += _DEBUG }
}
DESTDIR = $$PWD/../bin/
QT += core gui widgets
INCLUDEPATH += .
HEADERS += $$PWD/filesSource.h $$PWD/repositorium.h
SOURCES += $$PWD/filesSource.cpp $$PWD/main.cpp $$PWD/repositorium.cpp
RESOURCES += $$PWD/repositorium.qrc
