#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringList>
#include <QDir>
QT_BEGIN_NAMESPACE
class QAction;
class QByteArray;
class QFile;
class QMenu;
class QLabel;
class QProcess;
class QSpinBox;
class QSplitter;
class QTextEdit;
class QTextStream;
class QToolBar;
class QTranslator;
class QPushButton;
class QLineEdit;
class QLibrary;
QT_END_NAMESPACE

class QFile;
class PanelMzML;
class WidgetFile;
class QVBoxLayout;
class FilesSource;
class Repositorium : public QMainWindow
{
	Q_OBJECT
	QVBoxLayout * layout_main_;

	QMenu * m_menuFile;
	QAction* actionOpenDirs_;
	QAction* action_makeInstaller_;
	QAction* action_clear_;
	QAction* action_make_all_;
	QAction* action_make_run_;
	void translateUI();
	QSplitter * m_splitterMain;
	QDir dir_main;
	QSpinBox*  m_spinboxShiftBytes;
	void createMenuBar();
	
	FilesSource * fs_destination;
	FilesSource * createDestinationFolder();
	FilesSource * createSourceFolder(const QString& destination);

public:
	Repositorium();
	public slots:
	void on_package_data_folders(QStringList dirs);
	void on_openDirs();
	void on_makeInstaller();
	void on_make_all();
	void on_make_run();
};

#endif//MAINWINDOW_H