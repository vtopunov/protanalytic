TEMPLATE = app
macx {
	CONFIG += c++11
	DEFINES += HAVE_PTHREAD
	QMAKE_CFLAGS += -gdwarf-2
	QMAKE_CXXFLAGS += -gdwarf-2
	QMAKE_INFO_PLIST= $${PWD}/Info.plist
	ICON = $${PWD}/icon.icns
	debug { DEFINES += _DEBUG }
}
DESTDIR = $$PWD/..$(Platform)/$(Configuration)/
QT += core gui network widgets
INCLUDEPATH += .
HEADERS += $$PWD/$$PWD/protoperator.h
SOURCES += $$PWD/$$PWD/main.cpp $$PWD/$$PWD/protoperator.cpp
FORMS += $$PWD/$$PWD/protoperator.ui
RESOURCES += $$PWD/$$PWD/protoperator.qrc
